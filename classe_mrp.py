# Databricks notebook source
# !pip install unidecode
# !pip install bigquery
# !pip install openpyxl
# !pip install fsspec
# !pip install gcsfs

# COMMAND ----------

import pandas as pd
import datetime
from datetime import timedelta
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

from google.cloud import bigquery as bq
client = bq.Client()

# COMMAND ----------

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.

# COMMAND ----------

# MAGIC %md
# MAGIC ### Tabela item

# COMMAND ----------

query = f"""
SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ITEM.ID_SITUACAO

FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
"""

TABELA_ITEM = client.query(query).to_dataframe()

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Dataset classe mrp

# COMMAND ----------

df_original = pd.read_excel('gs://classe_mrp/classe_mrp.xlsx')

df_original = df_original[['CD_ITEM','CLASSE_MRP']]

lista_itens_classificados = list(df_original['CD_ITEM'].unique())

df = df_original.copy()

# COMMAND ----------

df = df.merge(
  TABELA_ITEM[['CD_ITEM', 'NM_ITEM']],
  how = 'left',
  on = ['CD_ITEM']
)

# COMMAND ----------

# Remove numeros, parenteses e simbolo de porcentagem

def remove_numeros(texto):
  
 return ''.join([i for i in texto if not ((i.isdigit())|(i in ['(',')','%', ',','-','/','.','\n','º']))])

# COMMAND ----------

df['NM_ITEM'] = df['NM_ITEM'].map(remove_numeros)

# COMMAND ----------

df = pd.concat([df,df['NM_ITEM'].str.get_dummies(' ')],axis=1)

# COMMAND ----------

# df.shape

# COMMAND ----------

# df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Fitando o modelo usando um subconjunto de treino

# COMMAND ----------

x = df.drop(columns=['CD_ITEM','NM_ITEM','CLASSE_MRP'])

# COMMAND ----------

y = df['CLASSE_MRP']

# COMMAND ----------

x_training_data, x_test_data, y_training_data, y_test_data = train_test_split(x, y, test_size = 0.2)

# COMMAND ----------

model = DecisionTreeClassifier()
model.fit(x_training_data, y_training_data)
predictions = model.predict(x_test_data)

# COMMAND ----------

# print(classification_report(y_test_data, predictions))
# print(confusion_matrix(y_test_data, predictions))

# COMMAND ----------

report = classification_report(y_test_data, predictions, output_dict=True)

df_report = pd.DataFrame(report).transpose().reset_index()

df_report['DATA_CLASSIFICACAO'] = datetime.datetime.now()

df_report.rename(columns={
  'index': 'CLASSE_MRP',
  'f1-score': 'f1_score'
}, inplace = True)

carrega_tabela_bq("mrp-ia", "painel_mrp", "performance_classificacao_classe_mrp", df_report, 'WRITE_APPEND')

# df_report.to_excel(f'gs://classe_mrp/{str(datetime.datetime.today())[:19]}/performance_classe_mrp_automatica.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Fitando o modelo usando todo o dataset

# COMMAND ----------

model = DecisionTreeClassifier()
model.fit(x, y)

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Obtendo todos os itens ativos e que fazem parte do escopo da classe mrp

# COMMAND ----------

# df_original.merge(
#   TABELA_ITEM[['CD_ITEM','CD_GRUPO_ITEM','CD_SUBGRUPO_ITEM','NM_GRUPO','NM_SUBGRUPO']],
#   how = 'left',
#   on = ['CD_ITEM']
# ).groupby(['CD_GRUPO_ITEM','CD_SUBGRUPO_ITEM']).count()

# COMMAND ----------

df_itens_ativos = TABELA_ITEM[['CD_ITEM','NM_ITEM','CD_GRUPO_ITEM','CD_SUBGRUPO_ITEM','NM_GRUPO','NM_SUBGRUPO', 'ID_SITUACAO']]

df_itens_ativos = df_itens_ativos[
  (~df_itens_ativos['CD_ITEM'].isin(lista_itens_classificados))
  &(df_itens_ativos['ID_SITUACAO'] == 1)
  &(df_itens_ativos['CD_GRUPO_ITEM'] == 1)
  &(df_itens_ativos['CD_SUBGRUPO_ITEM'].isin([2,3,15,24]))
]

# df_itens_ativos.shape

# COMMAND ----------

if df_itens_ativos.shape[0] > 0:

  df_itens_ativos = df_itens_ativos[['CD_ITEM','NM_ITEM']]

  df_itens_ativos['NM_ITEM'] = df_itens_ativos['NM_ITEM'].map(remove_numeros)

  df_itens_ativos = pd.concat([df_itens_ativos, df_itens_ativos['NM_ITEM'].str.get_dummies(' ')],axis=1)

  x_novos_itens = df_itens_ativos.drop(columns=['CD_ITEM','NM_ITEM']).copy()

  lista_colunas_exemplos = list(x.columns)

  lista_colunas_novos_itens = list(x_novos_itens.columns)

  for coluna in lista_colunas_exemplos:

    if coluna not in lista_colunas_novos_itens:

      x_novos_itens[coluna] = 0

  x_novos_itens = x_novos_itens[lista_colunas_exemplos]

  df_itens_ativos['CLASSE_MRP'] = model.predict(x_novos_itens)

  df_itens_ativos = df_itens_ativos[['CD_ITEM','NM_ITEM','CLASSE_MRP']]
  
  df_itens_ativos['DATA_CLASSIFICACAO'] = datetime.datetime.now()
  
  carrega_tabela_bq("mrp-ia", "painel_mrp", "itens_classificados_classe_mrp", df_itens_ativos, 'WRITE_APPEND')
  
#   df_itens_ativos.to_excel(f'gs://classe_mrp/{str(datetime.datetime.today())[:19]}/itens_classificados.xlsx', index = False)

  df = pd.concat([df_original, df_itens_ativos], ignore_index = True)

  df.to_excel('gs://classe_mrp/classe_mrp.xlsx', index = False)

  carrega_tabela_bq("mrp-ia", "painel_mrp", "classe_mrp", df, 'WRITE_TRUNCATE')
  
#   table_id = 'classe_mrp'

#   dataset = bq.dataset.Dataset(f"mrp-ia.painel_mrp")

#   table_ref = dataset.table(table_id)

#   job_config = bq.LoadJobConfig(
#       write_disposition= 'WRITE_TRUNCATE',
#   )

#   job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

#   job.result()  # Waits for table load to complete.

#     df.to_gbq(
#       f'{nome_dataset}.{nome_tabela}',
#       project_id=id_projeto,
#       chunksize=None,
#       if_exists=modo
#     )
