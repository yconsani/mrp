# Databricks notebook source
# !pip install fsspec
# !pip install gcsfs
# !pip install openpyxl
# !pip install google-cloud-bigquery
# !pip install google-api-core==1.31.1

# COMMAND ----------

# Databricks notebook source
from pyspark.sql.functions import col, udf
from pyspark.sql.types import *
from pyspark.sql.functions import *

from google.cloud import bigquery as bq
from google.cloud import storage

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

# COMMAND ----------

def virgula(x):
  
    if x is None:
      
      return 0
    
    else:
      return float(str(x).replace('.','').replace(',','.'))
  
udf_virgula = udf(virgula, DoubleType())

# COMMAND ----------

# df1 = spark.read.format("csv").options(inferSchema='True',delimiter=';',header=True).load("gs://bkt-base-comercial/BASE_COMERCIAL_COMPLETA.csv")

df1 = spark.read.format("csv").options(inferSchema='True',delimiter=';',header=True).load("gs://base-comercial-pricing/BASE_COMERCIAL_COMPLETA.csv")

# COMMAND ----------

col_list = df1.columns

for c in col_list:
  if "VOLUME_" in c or "FATURAMENTO_" in c:
    df1 = df1.withColumn(c, udf_virgula(col(c)))

# COMMAND ----------

df1.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table",'pricing-seara.comercial.base_comercial').save()
