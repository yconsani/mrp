# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ### Importação de bibliotecas e criação das variáveis de data

# COMMAND ----------

# !pip install fsspec
# !pip install gcsfs
# !pip install openpyxl
# !pip install google-cloud-bigquery
# !pip install google-api-core==1.31.1

# COMMAND ----------

def retorna_inicio_mes_anterior(data, num_meses):
  
  for i in range(num_meses):
  
    inicio_do_mes = data.replace(day = 1)

    final_do_mes_anterior = inicio_do_mes - datetime.timedelta(days=1)

    inicio_do_mes_anterior = final_do_mes_anterior.replace(day = 1)

    data = inicio_do_mes_anterior
    
  return data

# COMMAND ----------

# import pandas as pd
from pyspark.sql.functions import *
from pyspark.sql.types import DoubleType, IntegerType, StringType, DateType, StructType, StructField
from pyspark.sql import Window
import datetime
from datetime import timedelta
import numpy as np
import pandas as pd
import scipy.stats as stats
import pytz

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession

spark = SparkSession.builder.appName("PysparkApp")\
.config ("spark.sql.shuffle.partitions", "200")\
.config("spark.driver.maxResultSize","5g")\
.config ("spark.sql.execution.arrow.pyspark.enabled", "true")\
.getOrCreate()

spark.conf.set("viewsEnabled","true")

spark.conf.set("spark.sql.debug.maxToStringFields", 1000)

from google.cloud import bigquery as bq
client = bq.Client()

# Cria método para mostrar o shape do df de maneira rápida
import pyspark
def sparkShape(dataFrame):
    return (dataFrame.count(), len(dataFrame.columns))
pyspark.sql.dataframe.DataFrame.shape = sparkShape

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

inicio_do_mes = hoje.replace(day = 1)

final_do_mes_anterior = inicio_do_mes - datetime.timedelta(days=1)

inicio_do_mes_anterior = final_do_mes_anterior.replace(day = 1)

trinta_dias_atras = hoje - datetime.timedelta(days=30)

inicio_do_mes_d_menos_3 = retorna_inicio_mes_anterior(hoje, 3)

inicio_do_mes_d_menos_5 = retorna_inicio_mes_anterior(hoje, 5)

inicio_do_mes_d_menos_6 = retorna_inicio_mes_anterior(hoje, 6)

data_ref = str(hoje)[:10]

# COMMAND ----------

def retorna_inicio_do_mes(x):
  
  return x.replace(day = 1)

retorna_inicio_do_mes_UDF = udf(lambda x: retorna_inicio_do_mes(x), DateType())

# COMMAND ----------

# MAGIC %md
# MAGIC ### Tabela Itens

# COMMAND ----------

query = """
   SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ORIGEM.ID_ORIGEM,
    ORIGEM.DS_ORIGEM
    
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
LEFT JOIN
(
SELECT
  RV_LOW_VALUE AS ID_ORIGEM,
  -- RV_DOMAIN,
  RV_MEANING AS DS_ORIGEM
FROM
  `seara-analytics-prod.erp_seara.bcmg_ref_codes`
WHERE
  RV_DOMAIN = 'ITEM.ID_ORIGEM'
) AS ORIGEM
ON
  CAST(ORIGEM.ID_ORIGEM AS INT) = ITEM.ID_ORIGEM

"""

ITEM = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input manual

# COMMAND ----------

df_inputs = pd.read_excel('gs://lead_time_mrp/Lead_time_fornecedores.xlsx')

df_inputs.rename(columns={
  'Lead_time': 'LEAD_TIME_MANUAL',
  'Origem': 'ORIGEM_MANUAL'
}, inplace = True)

df_inputs = df_inputs[['CD_FILIAL','CD_ITEM','LEAD_TIME_MANUAL','ORIGEM_MANUAL']]

df_inputs = df_inputs[
  ~((df_inputs['CD_ITEM'].isna())|(df_inputs['CD_FILIAL'].isna()))
]

df_inputs['CD_ITEM'] = df_inputs['CD_ITEM'].astype(int)

df_inputs['CD_FILIAL'] = df_inputs['CD_FILIAL'].astype(int)

# schema = StructType([ 
#     StructField("CD_ITEM", IntegerType(),True),
#     StructField("CD_FILIAL", IntegerType(),True),
#     StructField("LEAD_TIME_MANUAL", DoubleType(),True),
#     StructField("ORIGEM_MANUAL", StringType(),True)
# ])

# df_inputs = spark.createDataFrame(df_inputs, schema)

df_inputs = spark.createDataFrame(df_inputs)

df_inputs = df_inputs.withColumn('CD_FILIAL', col('CD_FILIAL').cast(IntegerType())).withColumn('CD_ITEM', col('CD_ITEM').cast(IntegerType()))

# df_inputs.printSchema()

# COMMAND ----------

df_lote_minimo = pd.read_excel('gs://lead_time_mrp/Lead_time_fornecedores.xlsx')

df_lote_minimo.rename(columns={
  'Lote_minimo': 'LOTE_MINIMO'
}, inplace = True)

df_lote_minimo = df_lote_minimo[['CD_FILIAL','CD_ITEM','LOTE_MINIMO']]

df_lote_minimo = spark.createDataFrame(df_lote_minimo)

# df_lote_minimo.display()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Importância dos itens componentes

# COMMAND ----------

query = """
SELECT
  CD_FILIAL,
  CD_ITEM_COMPONENTE AS CD_ITEM,
  COUNT(CD_ITEM) as NUM_ITENS_AFETADOS
FROM
  `mrp-ia.painel_mrp.formulas_utilizadas`
GROUP BY 
  CD_FILIAL,
  CD_ITEM_COMPONENTE
ORDER BY 
  CD_FILIAL,
  NUM_ITENS_AFETADOS DESC,
  CD_ITEM_COMPONENTE

"""

df_ranking_itens = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_ranking_itens = df_ranking_itens.join(
  df_inputs.select('CD_FILIAL','CD_ITEM','ORIGEM_MANUAL'),
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
).join(
  ITEM.select('CD_ITEM','ID_ORIGEM'),
  how = 'left',
  on = ['CD_ITEM']
)

# COMMAND ----------

df_ranking_itens = df_ranking_itens.withColumn(
  'SCORE_Z',
  when((col('NUM_ITENS_AFETADOS') >= 100)&((col('ORIGEM_MANUAL') == 'Importado')|(col('ID_ORIGEM').isin([3,6]))), lit(stats.norm.ppf(0.9999))). \
  when(col('NUM_ITENS_AFETADOS') >= 100, lit(stats.norm.ppf(0.999))). \
  when(col('NUM_ITENS_AFETADOS') >= 40, lit(stats.norm.ppf(0.98))). \
  otherwise(lit(stats.norm.ppf(0.95)))
).withColumn('SCORE_Z', round(col('SCORE_Z'),2))

# COMMAND ----------

df_ranking_itens = df_ranking_itens.select('CD_FILIAL','CD_ITEM','NUM_ITENS_AFETADOS','SCORE_Z')

# COMMAND ----------

# MAGIC %md
# MAGIC #### Lista filiais produtivas

# COMMAND ----------

query = f"""
WITH FILIAIS AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
    CD_EMPRESA,
    CD_FILIAL, 
    NM_FILIAL 
FROM 
    FILIAIS
WHERE
    CD_FILIAL IN (
        SELECT
            DISTINCT CD_FILIAL
        FROM
        `seara-analytics-prod.stg_erp_seara.scf_producao_filial`
        WHERE
            ID_SITUACAO = 1
        )
ORDER BY 
    CD_FILIAL
"""

df_filiais = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

lista_filiais = [x['CD_FILIAL'] for x in df_filiais.select(['CD_FILIAL']).collect()]

lista_filiais = lista_filiais + [405, 130, 120]

filiais = str(lista_filiais).replace('[', '').replace(']', '')

print('filiais: ', filiais)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Estoque virada de mês

# COMMAND ----------

query = f"""
WITH ITEM AS (
    SELECT
        ITEM.CD_ITEM,
        ITEM.NM_ITEM,
        ITEM.DS_ITEM,
        ITEM.CD_CLASSE_ITEM,
        CLASSE_ITEM.NM_CLASSE,
        ITEM.CD_GRUPO_ITEM,
        GRUPO_ITEM.NM_GRUPO,
        ITEM.CD_SUBGRUPO_ITEM,
        SUBGRUPO_ITEM.NM_SUBGRUPO,
        --ITEM.CD_FAMILIA_ITEM,
        ITEM.CD_FAMILIA_PRODUTO,
        FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
        FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
        FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
        FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
        FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO

    FROM
        `seara-analytics-prod.stg_erp_seara.item` AS ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
    ON
        ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
        AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
        AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
    ON
        ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
    ON
        SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
    ON
        FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
        --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
    ON
        FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
        AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
        AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
    ON
        FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
        AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
        AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
        AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
),

FILIAL AS (

  WITH A AS (
      SELECT
          CD_EMPRESA,
          CD_FILIAL,
          CD_FILIAL_CENTRALIZADORA,
          NM_FILIAL,
          DT_ATUALIZACAO,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
      FROM
          `seara-analytics-prod.stg_erp_seara.filial_cgc`
      )

  SELECT * FROM A WHERE A.rn = 1
)


SELECT
  ESTOQUE.CD_EMPRESA,
  FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  PARSE_DATE('%d/%m/%Y', LEFT(ESTOQUE.DT_MESANO_REFERENCIA,10)) AS DT_MESANO_REFERENCIA,
  ESTOQUE.CD_ITEM,
  CONCAT(ITEM.CD_ITEM, ' - ', ITEM.NM_ITEM) AS FILTRO_ITEM,
  ESTOQUE.CD_LOCAL_ESTOQUE,
  LOCAL_ESTOQUE.DS_LOCAL_ESTOQUE,
  CONCAT(ESTOQUE.CD_LOCAL_ESTOQUE, ' - ', LOCAL_ESTOQUE.DS_LOCAL_ESTOQUE) AS FILTRO_LOCAL_ESTOQUE,
  ESTOQUE.CD_TIPO_CUSTO_SUPRIMENTO,
  CAST(ESTOQUE.QT_SALDO_ATUAL AS FLOAT64) AS QT_SALDO_ATUAL,
  CAST(ESTOQUE.VL_CUSTO_UNITARIO AS FLOAT64) AS VL_CUSTO_UNITARIO,
  CAST(ESTOQUE.VL_SALDO_ATUAL AS FLOAT64) AS VL_SALDO_ATUAL,
  ITEM.NM_CLASSE,
  ITEM.CD_GRUPO_ITEM,
  ITEM.NM_GRUPO,
  -- CONCAT(ITEM.CD_GRUPO_ITEM, ' - ', ITEM.NM_GRUPO) AS FILTRO_GRUPO,
  ITEM.CD_SUBGRUPO_ITEM,
  ITEM.NM_SUBGRUPO,
  -- CONCAT(ITEM.CD_SUBGRUPO_ITEM, ' - ', ITEM.NM_SUBGRUPO) AS FILTRO_SUBGRUPO
FROM
  `seara-analytics-prod.stg_erp_seara.saldo_estoque_spm_mes` ESTOQUE
LEFT JOIN
  ITEM
ON
  ITEM.CD_ITEM = ESTOQUE.CD_ITEM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.local_estoque_suprimento` LOCAL_ESTOQUE
ON
  ESTOQUE.CD_EMPRESA = LOCAL_ESTOQUE.CD_EMPRESA
  AND ESTOQUE.CD_FILIAL = LOCAL_ESTOQUE.CD_FILIAL
  AND ESTOQUE.CD_LOCAL_ESTOQUE = LOCAL_ESTOQUE.CD_LOCAL_ESTOQUE
LEFT JOIN
  FILIAL
ON
  ESTOQUE.CD_EMPRESA = FILIAL.CD_EMPRESA
  AND ESTOQUE.CD_FILIAL = FILIAL.CD_FILIAL
WHERE
  ESTOQUE.CD_EMPRESA IN (30, 36)
  AND FILIAL.CD_FILIAL_CENTRALIZADORA IN  ({filiais})
  AND QT_SALDO_ATUAL > 0
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ESTOQUE.DT_MESANO_REFERENCIA) >= '{str(inicio_do_mes_d_menos_3)}'
"""

df_saldo_estoque_mes = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Estoque atual

# COMMAND ----------

query = f"""
WITH FILIAL AS (

  WITH A AS (
      SELECT
          CD_EMPRESA,
          CD_FILIAL,
          CD_FILIAL_CENTRALIZADORA,
          NM_FILIAL,
          DT_ATUALIZACAO,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
      FROM
          `seara-analytics-prod.stg_erp_seara.filial_cgc`
      )

  SELECT * FROM A WHERE A.rn = 1
 ),
 
ITEM AS (
SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO

FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
)

SELECT 
    SALDO.CD_EMPRESA,
    FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
    CONCAT(FILIAL.CD_FILIAL_CENTRALIZADORA,' - ', FILIAL_CENTRALIZADORA.NM_FILIAL) AS FILTRO_FILIAL,
    SALDO.CD_LOCAL_ESTOQUE,
    SALDO.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.CD_GRUPO_ITEM,
    ITEM.CD_SUBGRUPO_ITEM,
    ITEM.NM_GRUPO,
    ITEM.NM_SUBGRUPO,
    SALDO.QT_SALDO_ATUAL,
    SALDO.VL_CUSTO_UNITARIO_ATUAL,
    SALDO.VL_SALDO_ATUAL
    /*CD_TIPO_CUSTO_SUPRIMENTO*/
FROM `seara-analytics-prod.stg_erp_seara.saldo_estoque_suprimento` SALDO
LEFT JOIN 
    FILIAL 
ON
    FILIAL.CD_FILIAL = SALDO.CD_FILIAL
LEFT JOIN
    FILIAL AS FILIAL_CENTRALIZADORA
ON
    FILIAL_CENTRALIZADORA.CD_FILIAL = FILIAL.CD_FILIAL_CENTRALIZADORA
LEFT JOIN
    ITEM
ON
    ITEM.CD_ITEM = SALDO.CD_ITEM
WHERE
    SALDO.CD_EMPRESA IN (30,36)
    AND FILIAL.CD_FILIAL_CENTRALIZADORA IN ({filiais})
    AND CD_LOCAL_ESTOQUE NOT IN (150,296,500)
    AND CD_GRUPO_ITEM = 1
    AND CD_SUBGRUPO_ITEM IN (2,3,29)  -- (2,3,5,29) 5: MP para rações , 29: Oleos comestiveis mp
"""

df_saldo_estoque_atual = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Ordens de fornecimento

# COMMAND ----------

query = f"""
WITH FILIAL AS (

  WITH A AS (
      SELECT
          CD_EMPRESA,
          CD_FILIAL,
          CD_FILIAL_CENTRALIZADORA,
          NM_FILIAL,
          DT_ATUALIZACAO,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
      FROM
          `seara-analytics-prod.stg_erp_seara.filial_cgc`
      )

  SELECT * FROM A WHERE A.rn = 1
 ),

 A AS
(
   SELECT
    ORDEM_FORNECIMENTO_COMPRA.CD_EMPRESA,
    FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
    ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,
    ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ,
    -- FORNECEDOR.CD_BASE_FORNECEDOR,
    -- FORNECEDOR.CD_ESTAB_FORNECEDOR,
    -- FORNECEDOR.NM_RAZAO_SOCIAL,
    -- FORNECEDOR.NM_FANTASIA,
    -- ITEM_SOLICITACAO_COMPRA2.NUM_SEQ,
    ORDEM_FORNECIMENTO_COMPRA.CD_INCOTERM,
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA,
    ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM - ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE AS SALDO_ITEM_OFO,
    --SNAC9029.BUSCA_SALDO_ITEM_OFO(ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ),
    --USUARIO8.CD_USUARIO,
    -- USUARIO8.NM_USUARIO,
    -- USUARIO38.NM_USUARIO AS DS_USUARIO,
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN,
    SITUACAO_ORDEM_FORNECIMENTO.DS_SITUACAO_ORDEM_FRN,
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM,
    -- ITEM.DS_ITEM,
    ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO,
    -- RATEIO_ITEM_OFO_CMP.CD_CENTRO_CUSTO,
    ITEM.CD_GRUPO_ITEM,
    ITEM.CD_SUBGRUPO_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.VL_ITEM,
    COALESCE(ITEM_ORDEM_FORNECIMENTO_CMP.id_fechado, 'N') AS YN_ID_FECHADO,
    ITEM_ORDEM_FORNECIMENTO_CMP.ID_FECHADO
FROM
    `seara-analytics-prod.stg_erp_seara.item_ordem_fornecimento_cmp` AS ITEM_ORDEM_FORNECIMENTO_CMP

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_compra` AS ORDEM_FORNECIMENTO_COMPRA
ON
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO=ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO8
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_COMPRADOR=USUARIO8.CD_USUARIO

LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.situacao_ordem_fornecimento` AS SITUACAO_ORDEM_FORNECIMENTO
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN=SITUACAO_ORDEM_FORNECIMENTO.CD_SITUACAO_ORDEM_FRN

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.rateio_item_ofo_cmp` AS RATEIO_ITEM_OFO_CMP
ON
    RATEIO_ITEM_OFO_CMP.CD_ORDEM_FORNECIMENTO=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO
    AND RATEIO_ITEM_OFO_CMP.NR_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item_solicitacao_compra` AS ITEM_SOLICITACAO_COMPRA2
ON
    ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_ORDEM_FORNECIMENTO_CMP.CD_SOLICITACAO_COMPRA
    AND ITEM_SOLICITACAO_COMPRA2.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM
    AND ITEM_SOLICITACAO_COMPRA2.NUM_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NUM_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.solicitacao_compra` AS SOLICITACAO_COMPRA2
ON
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO38
ON
    USUARIO38.CD_USUARIO=SOLICITACAO_COMPRA2.CD_USUARIO_SOLICITANTE

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.fornecedor` AS FORNECEDOR  
ON
    FORNECEDOR.CD_BASE_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR
    AND FORNECEDOR.CD_ESTAB_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR
LEFT JOIN 
    FILIAL
ON
    FILIAL.CD_FILIAL = ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL
INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
ON
    ITEM.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM

WHERE
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA  IS NOT NULL
    AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO) >= '{str(inicio_do_mes_d_menos_3)}'
)

SELECT
    *
FROM
    A
WHERE
    CD_FILIAL IN ({filiais})
    AND CD_EMPRESA IN (30,36)
    -- AND SALDO_ITEM_OFO > 0
    -- AND ID_FECHADO = 'N'
    AND CD_SITUACAO_ORDEM_FRN IN (2,3) -- 2: Compra confirmada, 3: Fechada
ORDER BY
    CD_EMPRESA,
    CD_FILIAL,
    CD_ORDEM_FORNECIMENTO,
    NR_SEQ
"""

df_ofs = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

def converte_colunas_de_data(df, tipo='date'):
  col_names = df.schema.names
  
  for i in col_names:
    if i.startswith("DT_"):
      if tipo == 'date':
        df = df.withColumn(i, to_date(col(i), 'dd/MM/yyyy HH:mm:ss'))
      elif tipo == 'timestamp':
        df = df.withColumn(i, to_timestamp(col(i), 'dd/MM/yyyy HH:mm:ss'))
      else:
        return print("Use 'date' ou 'timestamp' como argumentos.")
  return df

# COMMAND ----------

df_ofs = converte_colunas_de_data(df_ofs, 'date')

# COMMAND ----------

df_ofs_bq = df_ofs.select(
 'CD_EMPRESA',
 'CD_FILIAL',
 'CD_ORDEM_FORNECIMENTO',
 'NR_SEQ',
 'CD_INCOTERM',
 'DT_ORDEM_FORNECIMENTO',
 'DT_ENTREGA',
 'QT_ITEM',
 'QT_ITEM_ENTREGUE',
 'SALDO_ITEM_OFO',
 'CD_ITEM'
).orderBy('DT_ENTREGA')

df_ofs_bq.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.ofs_estoque_ideal').save()

# COMMAND ----------

df_ofs = df_ofs.groupby([
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ORDEM_FORNECIMENTO',
#   'CD_BASE_FORNECEDOR',
#   'CD_ESTAB_FORNECEDOR',
#   'NM_RAZAO_SOCIAL',
#   'NM_FANTASIA',
#   'NUM_SEQ',
#   'NR_SEQ',
#   'CD_SOLICITACAO_COMPRA',
#   'QT_ITEM',
#   'QT_ITEM_ENTREGUE',
#   'SALDO_ITEM_OFO',
#   'NM_USUARIO',
#   'DS_USUARIO',
#   'CD_SITUACAO_ORDEM_FRN',
#   'DS_SITUACAO_ORDEM_FRN',
  'CD_ITEM',
#   'DS_ITEM',
  'DT_ORDEM_FORNECIMENTO',
#   'CD_CENTRO_CUSTO',
#   'CD_GRUPO_ITEM',
#   'CD_SUBGRUPO_ITEM',
#   'VL_ITEM',
#   'YN_ID_FECHADO',
#   'ID_FECHADO',
]).agg({
  'DT_ENTREGA': 'min'
}).withColumnRenamed('min(DT_ENTREGA)', 'DT_ENTREGA')

# COMMAND ----------

df_ofs = df_ofs.withColumn('LEAD_TIME', round(datediff(col("DT_ENTREGA"),col("DT_ORDEM_FORNECIMENTO")),2))

df_ofs = df_ofs.withColumn('MES', retorna_inicio_do_mes_UDF(col('DT_ORDEM_FORNECIMENTO')))

# COMMAND ----------

spark.catalog.dropTempView("df")

df_ofs.createOrReplaceTempView("df")

df_ofs = spark.sql(
  """
    SELECT 
        *,
       percentile_approx(LEAD_TIME, 0.25) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_PCT25, 
        percentile_approx(LEAD_TIME, 0.5) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_PCT50,
       percentile_approx(LEAD_TIME, 0.75) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_PCT75
       -- percentile_approx(LEAD_TIME, 0.90) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_PCT90,
       -- MAX(LEAD_TIME) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_MAX
--        STDDEV(LEAD_TIME) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES) AS LEAD_TIME_DESVPAD
     FROM 
       df
   """
)

# COMMAND ----------

df_ofs = df_ofs.groupBy('CD_EMPRESA','CD_FILIAL','CD_ITEM', 'MES').agg(
#   avg('LEAD_TIME').alias('LEAD_TIME_MEDIO'),
#   avg('LEAD_TIME_PCT25').alias('LEAD_TIME_PCT25'),
  avg('LEAD_TIME_PCT50').alias('LEAD_TIME_PCT50'),
#   avg('LEAD_TIME_PCT50').alias('LEAD_TIME_PCT50'),
#   avg('LEAD_TIME_PCT75').alias('LEAD_TIME_PCT75')
#   avg('LEAD_TIME_PCT90').alias('LEAD_TIME_PCT90'),
#   avg('LEAD_TIME_MAX').alias('LEAD_TIME_MAX'),
#   avg('LEAD_TIME_DESVPAD').alias('LEAD_TIME_DESVPAD')
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_ofs.createOrReplaceTempView("df")

df_ofs = spark.sql(
  """
    SELECT 
        *,
       percentile_approx(LEAD_TIME_PCT50, 0.25) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q1, 
        percentile_approx(LEAD_TIME_PCT50, 0.75) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q3
     FROM 
       df
   """
)

# COMMAND ----------

df_ofs = df_ofs.withColumn(
  'IQR', col('Q3') - col('Q1')
).withColumn(
  'VAL_MAXIMO', col('Q3') + 1.5*col('IQR')
).withColumn(
  'VAL_MINIMO', when(col('Q1') - 1.5*col('IQR') < 0, 0).otherwise(col('Q1') - 1.5*col('IQR'))
)
    

# COMMAND ----------

df_ofs = df_ofs.filter(
  (col('LEAD_TIME_PCT50') >= col('VAL_MINIMO'))
  &(col('LEAD_TIME_PCT50') <= col('VAL_MAXIMO'))
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_ofs.createOrReplaceTempView("df")

df_ofs = spark.sql(
  """
    SELECT 
        *,
       STDDEV(LEAD_TIME_PCT50) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS LEAD_TIME_DESVPAD
     FROM 
       df
   """
).withColumn('LEAD_TIME_DESVPAD', round(col('LEAD_TIME_DESVPAD'),2))

# COMMAND ----------

df_ofs = df_ofs.withColumn('LEAD_TIME_DESVPAD_MESES', round(col('LEAD_TIME_DESVPAD')/30,3))

# COMMAND ----------

df_ofs = df_ofs.groupBy(
  'CD_EMPRESA','CD_FILIAL','CD_ITEM'
).agg(
  avg('LEAD_TIME_PCT50').alias('LEAD_TIME_PCT50'),
  avg('LEAD_TIME_DESVPAD').alias('LEAD_TIME_DESVPAD'),
  avg('LEAD_TIME_DESVPAD_MESES').alias('LEAD_TIME_DESVPAD_MESES')  
)

# COMMAND ----------

df_ofs = df_ofs.withColumn('LEAD_TIME_PCT50_MESES', round(col('LEAD_TIME_PCT50')/30,3))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Desvios na data de entrega

# COMMAND ----------

query = f"""
SELECT
  ORDEM_FORNECIMENTO_COMPRA.CD_EMPRESA,
  ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL,
  ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,
  ITEM_ORDEM_FORNECIMENTO_CMP.CD_SOLICITACAO_COMPRA,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO,
  ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR,
  ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR AS CD_ESTAB_FORNECEDOR_DOC_RCB,
  ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ,
  ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM,
  ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM,
  ITEM_ORDEM_FORNECIMENTO_CMP.VL_ITEM,
  ITEM_ORDEM_FORNECIMENTO_CMP.VL_TOTAL_ITEM,
  ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE,
  -- ITEM_ORDEM_FORNECIMENTO_CMP.NR_PRAZO_ENTREGA,
  ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO,
  -- ORDEM_FORNECIMENTO_COMPRA.DT_ENTREGA_CNT,
  -- ITEM_SOLICITACAO_COMPRA.DT_ENTREGA AS SC_DT_ENTREGA,
  -- ORDEM_FORNECIMENTO_COMPRA.DT_ENVIO_FORNECEDOR,
  ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA,
  DOCUMENTO_RECEBIMENTO.DT_ENTRADA,
  -- DOCUMENTO_RECEBIMENTO.DT_SAIDA,
  -- DOCUMENTO_RECEBIMENTO.DT_INTEGRACAO,
  ABS(DATE_DIFF(
    DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DOCUMENTO_RECEBIMENTO.DT_ENTRADA)),
    DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA)),
    DAY
    )) AS DESVIO_ENTREGA
FROM
  `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_compra` ORDEM_FORNECIMENTO_COMPRA
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.item_ordem_fornecimento_cmp` ITEM_ORDEM_FORNECIMENTO_CMP
ON 
  ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO = ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO
LEFT JOIN 
( 
  SELECT
    CD_ORDEM_FORNECIMENTO,
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_EMPRESA_ETQ,
    CD_FILIAL_ETQ,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO,
    CD_ITEM,
    NR_SEQ_ITEM_OFO,
    SUM(QT_CALCULO_OFO) AS QT_CALCULO_OFO
  FROM
    `seara-analytics-prod.stg_erp_seara.item_documento_recebimento`
  GROUP BY
    CD_ORDEM_FORNECIMENTO,
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_EMPRESA_ETQ,
    CD_FILIAL_ETQ,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO,
    CD_ITEM,
    NR_SEQ_ITEM_OFO
 ) ITEM_DOCUMENTO_RECEBIMENTO
ON
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ORDEM_FORNECIMENTO = ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_ETQ = ORDEM_FORNECIMENTO_COMPRA.CD_EMPRESA
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_ETQ = ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_ITEM = ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM
  AND ITEM_DOCUMENTO_RECEBIMENTO.NR_SEQ_ITEM_OFO = ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.documento_recebimento` DOCUMENTO_RECEBIMENTO
ON
  DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR
  AND DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR
  AND DOCUMENTO_RECEBIMENTO.CD_EMPRESA_FISICA = ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_ETQ
  AND DOCUMENTO_RECEBIMENTO.CD_FILIAL_FISICA = ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_ETQ
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_solicitacao_compra` ITEM_SOLICITACAO_COMPRA 
ON
  ITEM_SOLICITACAO_COMPRA.CD_SOLICITACAO_COMPRA = ITEM_ORDEM_FORNECIMENTO_CMP.CD_SOLICITACAO_COMPRA
  AND ITEM_SOLICITACAO_COMPRA.CD_ITEM = ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM
  AND ITEM_SOLICITACAO_COMPRA.CD_BASE_CLIENTE = ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR
  AND ITEM_SOLICITACAO_COMPRA.CD_ESTAB_CLIENTE = ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR
WHERE
  PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA) between '{str(inicio_do_mes_d_menos_3)}' and '{str(hoje)}'
  -- AND ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE > 0
  -- AND ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL = 4001
  -- AND ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM = 40478
  -- AND ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO = 4072632
ORDER BY 
  CD_EMPRESA,
  CD_FILIAL,
  CD_ORDEM_FORNECIMENTO,
  NR_SEQ

"""

df_desvio_entrega = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

spark.catalog.dropTempView("df")

df_desvio_entrega.createOrReplaceTempView("df")

df_desvio_entrega = spark.sql(
  """
    SELECT 
        *,
        ROUND(AVG(DESVIO_ENTREGA) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM),2) AS DESVIO_ENTREGA_MEDIO
     FROM 
       df
   """
)

# COMMAND ----------

df_desvio_entrega.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.desvio_entrega_estoque_ideal').save()

# COMMAND ----------

df_desvio_entrega = df_desvio_entrega.groupBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM'
).agg(
 round(avg(col('DESVIO_ENTREGA_MEDIO')),2).alias('DESVIO_ENTREGA_MEDIO')
)

# COMMAND ----------

# df_desvio_entrega.filter(
#   (col('CD_FILIAL') == 4001)
#   &(col('CD_ITEM') == 40478)
# ).display()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Tempo de abastecimento

# COMMAND ----------

query = f"""
WITH FILIAL AS (

  WITH A AS (
      SELECT
          CD_EMPRESA,
          CD_FILIAL,
          CD_FILIAL_CENTRALIZADORA,
          NM_FILIAL,
          DT_ATUALIZACAO,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
      FROM
          `seara-analytics-prod.stg_erp_seara.filial_cgc`
      )

  SELECT * FROM A WHERE A.rn = 1
 ),

 A AS
(
   SELECT
    ORDEM_FORNECIMENTO_COMPRA.CD_EMPRESA,
    FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
    ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,
    ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ,
    -- FORNECEDOR.CD_BASE_FORNECEDOR,
    -- FORNECEDOR.CD_ESTAB_FORNECEDOR,
    -- FORNECEDOR.NM_RAZAO_SOCIAL,
    -- FORNECEDOR.NM_FANTASIA,
    -- ITEM_SOLICITACAO_COMPRA2.NUM_SEQ,
    ORDEM_FORNECIMENTO_COMPRA.CD_INCOTERM,
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA,
    ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM - ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE AS SALDO_ITEM_OFO,
    --SNAC9029.BUSCA_SALDO_ITEM_OFO(ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ),
    --USUARIO8.CD_USUARIO,
    -- USUARIO8.NM_USUARIO,
    -- USUARIO38.NM_USUARIO AS DS_USUARIO,
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN,
    SITUACAO_ORDEM_FORNECIMENTO.DS_SITUACAO_ORDEM_FRN,
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM,
    -- ITEM.DS_ITEM,
    ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO,
    -- RATEIO_ITEM_OFO_CMP.CD_CENTRO_CUSTO,
    ITEM.CD_GRUPO_ITEM,
    ITEM.CD_SUBGRUPO_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.VL_ITEM,
    COALESCE(ITEM_ORDEM_FORNECIMENTO_CMP.id_fechado, 'N') AS YN_ID_FECHADO,
    ITEM_ORDEM_FORNECIMENTO_CMP.ID_FECHADO
FROM
    `seara-analytics-prod.stg_erp_seara.item_ordem_fornecimento_cmp` AS ITEM_ORDEM_FORNECIMENTO_CMP

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_compra` AS ORDEM_FORNECIMENTO_COMPRA
ON
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO=ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO8
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_COMPRADOR=USUARIO8.CD_USUARIO

LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.situacao_ordem_fornecimento` AS SITUACAO_ORDEM_FORNECIMENTO
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN=SITUACAO_ORDEM_FORNECIMENTO.CD_SITUACAO_ORDEM_FRN

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.rateio_item_ofo_cmp` AS RATEIO_ITEM_OFO_CMP
ON
    RATEIO_ITEM_OFO_CMP.CD_ORDEM_FORNECIMENTO=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO
    AND RATEIO_ITEM_OFO_CMP.NR_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item_solicitacao_compra` AS ITEM_SOLICITACAO_COMPRA2
ON
    ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_ORDEM_FORNECIMENTO_CMP.CD_SOLICITACAO_COMPRA
    AND ITEM_SOLICITACAO_COMPRA2.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM
    AND ITEM_SOLICITACAO_COMPRA2.NUM_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NUM_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.solicitacao_compra` AS SOLICITACAO_COMPRA2
ON
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO38
ON
    USUARIO38.CD_USUARIO=SOLICITACAO_COMPRA2.CD_USUARIO_SOLICITANTE

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.fornecedor` AS FORNECEDOR  
ON
    FORNECEDOR.CD_BASE_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR
    AND FORNECEDOR.CD_ESTAB_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR
LEFT JOIN 
    FILIAL
ON
    FILIAL.CD_FILIAL = ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL
INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
ON
    ITEM.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM

WHERE
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA  IS NOT NULL
    AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO) >= '{str(inicio_do_mes_d_menos_3)}'
)

SELECT
    *
FROM
    A
WHERE
    CD_FILIAL IN ({filiais})
    AND CD_EMPRESA IN (30,36)
    -- AND SALDO_ITEM_OFO > 0
    -- AND ID_FECHADO = 'N'
    AND CD_SITUACAO_ORDEM_FRN IN (2,3) -- 2: Compra confirmada, 3: Fechada
ORDER BY
    CD_EMPRESA,
    CD_FILIAL,
    CD_ORDEM_FORNECIMENTO,
    NR_SEQ
"""

df_abastecimento = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_abastecimento = converte_colunas_de_data(df_abastecimento, 'date')

# COMMAND ----------

df_abastecimento = df_abastecimento.groupBy(
 'CD_EMPRESA',
 'CD_FILIAL',
 'CD_ITEM',
#  'CD_ORDEM_FORNECIMENTO',
#  'NR_SEQ',
#  'CD_INCOTERM',
#  'CD_SOLICITACAO_COMPRA',
 'DT_ENTREGA',
#  'CD_SITUACAO_ORDEM_FRN',
#  'DS_SITUACAO_ORDEM_FRN',
#  'DT_ORDEM_FORNECIMENTO',
#  'CD_GRUPO_ITEM',
#  'CD_SUBGRUPO_ITEM',
#  'VL_ITEM',
#  'YN_ID_FECHADO',
#  'ID_FECHADO'
).agg(
  sum('QT_ITEM').alias('QT_ITEM')
).orderBy(
 'CD_EMPRESA',
 'CD_FILIAL',
 'CD_ITEM',
 'DT_ENTREGA'
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_abastecimento.createOrReplaceTempView("df")

df_abastecimento = spark.sql(
  """
    SELECT 
        *,
       DATEDIFF(DT_ENTREGA, lag(DT_ENTREGA,1) over (partition by CD_EMPRESA, CD_FILIAL, CD_ITEM order by DT_ENTREGA asc)) AS TEMPO_ABASTECIMENTO
     FROM 
       df
   """
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_abastecimento.createOrReplaceTempView("df")

df_abastecimento = spark.sql(
  """
    SELECT 
        *,
        percentile_approx(TEMPO_ABASTECIMENTO, 0.5) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS TEMPO_ABASTECIMENTO_PCT50,
        percentile_approx(QT_ITEM, 0.5) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS QT_ITEM_PCT50
     FROM 
       df
   """
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_abastecimento.createOrReplaceTempView("df")

df_abastecimento = spark.sql(
  """
    SELECT 
        *,
       percentile_approx(TEMPO_ABASTECIMENTO, 0.25) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q1, 
       percentile_approx(TEMPO_ABASTECIMENTO, 0.75) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q3
     FROM 
       df
   """
)

# COMMAND ----------

df_abastecimento = df_abastecimento.withColumn(
  'IQR', col('Q3') - col('Q1')
).withColumn(
  'VAL_MAXIMO', col('Q3') + 1.5*col('IQR')
).withColumn(
  'VAL_MINIMO', when(col('Q1') - 1.5*col('IQR') < 0, 0).otherwise(col('Q1') - 1.5*col('IQR'))
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_abastecimento.createOrReplaceTempView("df")

df_abastecimento = spark.sql(
  """
    SELECT 
        *,
       ROUND(STDDEV(TEMPO_ABASTECIMENTO) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM),2) AS TEMPO_ABASTECIMENTO_DESVPAD
     FROM 
       df
     WHERE
       TEMPO_ABASTECIMENTO >= VAL_MINIMO
       AND TEMPO_ABASTECIMENTO <= VAL_MAXIMO
   """
)

# COMMAND ----------

df_abastecimento.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.abastecimento_estoque_ideal').save()

# COMMAND ----------

df_abastecimento = df_abastecimento.filter(
  (col('TEMPO_ABASTECIMENTO') >= col('VAL_MINIMO'))
  &(col('TEMPO_ABASTECIMENTO') <= col('VAL_MAXIMO'))
)

# COMMAND ----------

df_abastecimento = df_abastecimento.groupBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM'
).agg(
  avg('QT_ITEM_PCT50').alias('QT_ITEM_ENTREGA_PCT50'),
  avg('TEMPO_ABASTECIMENTO_PCT50').alias('TEMPO_ABASTECIMENTO_PCT50'),
  avg('TEMPO_ABASTECIMENTO_DESVPAD').alias('TEMPO_ABASTECIMENTO_DESVPAD')
).withColumn(
  'TEMPO_ABASTECIMENTO_DESVPAD', round(col('TEMPO_ABASTECIMENTO_DESVPAD'),2)
)

# COMMAND ----------

df_abastecimento = df_abastecimento.withColumn('QT_ITEM_ENTREGA_PCT50', col('QT_ITEM_ENTREGA_PCT50').cast(DoubleType()))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Frete médio por Filial - Item - OF

# COMMAND ----------

query = f"""
WITH FILIAIS AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_EMPRESA, CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  OFS.CD_EMPRESA,
  FILIAIS.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  OFS.CD_INCOTERM,
  OFS.CD_ORDEM_FORNECIMENTO,
  OFS.DT_ORDEM_FORNECIMENTO,
  -- OFS.DT_FECHAMENTO,
  -- OFS.DS_OBSERVACAO,
  ITEM_OFS.NR_SEQ AS NR_SEQ_ITEM_OFO,
  ITEM_OFS.CD_ITEM,
  ITEM_OFS.QT_ITEM_ENTREGUE,
  ITEM.NM_ITEM,
  -- ITEM_OFS.DS_MATERIAL,
  ITEM_OFS.ID_FECHADO,
  ITEM_OFS.QT_ITEM,
  ITEM_OFS.VL_ITEM,
  ITEM_OFS.VL_TOTAL_ITEM,
  ITEM_OFS.DT_ENTREGA,
  -- ITEM_OFS.VL_ITEM_ENTREGUE,
  ITEM_REC.CD_DOCUMENTO,
  ITEM_REC.CD_SERIE_DOCUMENTO,
  -- ITEM_REC.QT_CALCULO_OFO,
  -- ITEM_REC.VL_CALCULO_CUSTO,
  -- ITEM_REC.VL_UNITARIO_FSC_OFO,
  ITEM_REC.CD_BASE_FORNECEDOR,
  ITEM_REC.CD_ESTAB_FORNECEDOR,
  ITEM_REC.NR_SEQ_ITEM AS NR_SEQ_ITEM_REC,
  ITEM_REC_FRETE.NR_SEQ_ITEM AS NR_SEQ_ITEM_REC_FRETE,
  FRETE_RECEBIMENTO.CD_DOCUMENTO AS CD_DOCUMENTO_FRETE,
  FRETE_RECEBIMENTO.CD_SERIE_DOCUMENTO AS CD_SERIE_DOCUMENTO_FRETE,
  FRETE_RECEBIMENTO.CD_BASE_FORNECEDOR AS CD_BASE_FORNECEDOR_FRETE,
  FRETE_RECEBIMENTO.CD_ESTAB_FORNECEDOR AS CD_ESTAB_FORNECEDOR_FRETE,
  -- ITEM_REC_FRETE.VL_UNITARIO_FISCAL_AJUSTADO AS VL_UNITARIO_FISCAL_AJUSTADO,
  RATEIO_ITEM_NFI_SNAC.VL_RATEIO_AJUSTADO

FROM
  `seara-analytics-prod.stg_erp_seara.item_ordem_fornecimento_cmp` ITEM_OFS
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_compra` AS OFS 
ON
  ITEM_OFS.CD_ORDEM_FORNECIMENTO = OFS.CD_ORDEM_FORNECIMENTO
LEFT JOIN 
  FILIAIS 
ON
  FILIAIS.CD_EMPRESA = OFS.CD_EMPRESA
  AND FILIAIS.CD_FILIAL = OFS.CD_FILIAL
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item` AS ITEM 
ON
  ITEM.CD_ITEM = ITEM_OFS.CD_ITEM
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` AS ITEM_REC  
ON
  ITEM_OFS.CD_ORDEM_FORNECIMENTO = ITEM_REC.CD_ORDEM_FORNECIMENTO
  AND ITEM_OFS.CD_ITEM = ITEM_REC.CD_ITEM
  AND ITEM_OFS.NR_SEQ = ITEM_REC.NR_SEQ_ITEM_OFO
INNER JOIN 
  `seara-analytics-prod.stg_erp_seara.frete_recebimento` AS FRETE_RECEBIMENTO  
ON
  ITEM_REC.CD_DOCUMENTO = FRETE_RECEBIMENTO.CD_DOCUMENTO_RCB
  AND ITEM_REC.CD_SERIE_DOCUMENTO = FRETE_RECEBIMENTO.CD_SERIE_DOCUMENTO_RCB
  AND ITEM_REC.CD_BASE_FORNECEDOR = FRETE_RECEBIMENTO.CD_BASE_FORNECEDOR_RCB
  AND ITEM_REC.CD_ESTAB_FORNECEDOR = FRETE_RECEBIMENTO.CD_ESTAB_FORNECEDOR_RCB
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` AS ITEM_REC_FRETE
ON
  FRETE_RECEBIMENTO.CD_DOCUMENTO = ITEM_REC_FRETE.CD_DOCUMENTO
  AND FRETE_RECEBIMENTO.CD_SERIE_DOCUMENTO = ITEM_REC_FRETE.CD_SERIE_DOCUMENTO
  AND FRETE_RECEBIMENTO.CD_BASE_FORNECEDOR = ITEM_REC_FRETE.CD_BASE_FORNECEDOR
  AND FRETE_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ITEM_REC_FRETE.CD_ESTAB_FORNECEDOR
  AND ITEM_OFS.CD_ITEM = ITEM_REC_FRETE.CD_ITEM
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.rateio_item_nfi_snac` AS RATEIO_ITEM_NFI_SNAC 
ON
  ITEM_REC_FRETE.CD_DOCUMENTO = RATEIO_ITEM_NFI_SNAC.CD_DOCUMENTO
  AND ITEM_REC_FRETE.CD_SERIE_DOCUMENTO = RATEIO_ITEM_NFI_SNAC.CD_SERIE_DOCUMENTO
  AND ITEM_REC_FRETE.CD_BASE_FORNECEDOR = RATEIO_ITEM_NFI_SNAC.CD_BASE_FORNECEDOR
  AND ITEM_REC_FRETE.CD_ESTAB_FORNECEDOR = RATEIO_ITEM_NFI_SNAC.CD_ESTAB_FORNECEDOR
  AND ITEM_REC_FRETE.NR_SEQ_ITEM = RATEIO_ITEM_NFI_SNAC.NR_SEQ_ITEM
WHERE
  FILIAIS.CD_FILIAL_CENTRALIZADORA IN ({filiais})
  AND FILIAIS.CD_EMPRESA IN (30,36)
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', OFS.DT_ORDEM_FORNECIMENTO) >= '{str(inicio_do_mes_d_menos_3)}'
  AND OFS.CD_INCOTERM = 'FOB'
  AND ITEM_OFS.QT_ITEM_ENTREGUE > 0
ORDER BY 
  ITEM_OFS.NR_SEQ

"""

df_fretes = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_fretes = df_fretes.withColumn(
  'VL_RATEIO_AJUSTADO', round(col('VL_RATEIO_AJUSTADO'),2)
).withColumn(
  'PR_FRETE_VL_TOTAL_ITEM', round(col('VL_RATEIO_AJUSTADO')/col('VL_TOTAL_ITEM'),2)
).withColumn('VL_FRETE_POR_KG', col('VL_RATEIO_AJUSTADO')/col('QT_ITEM'))

# COMMAND ----------

df_fretes.createOrReplaceTempView("df")

df_fretes = spark.sql(
  """
    SELECT 
        *, 
       percentile_approx(VL_RATEIO_AJUSTADO, 0.5) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS VL_RATERIO_AJUSTADO_MEDIANA,
      AVG(VL_RATEIO_AJUSTADO) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS VL_RATERIO_AJUSTADO_MEDIA
     FROM 
       df
   """
)

# COMMAND ----------

df_fretes = df_fretes.withColumn(
  'VL_RATERIO_AJUSTADO_MEDIA', round(col('VL_RATERIO_AJUSTADO_MEDIA'),2)
)

# COMMAND ----------

df_custo_cycle = df_fretes.groupBy('CD_EMPRESA','CD_FILIAL','CD_ITEM').agg(avg('VL_RATERIO_AJUSTADO_MEDIA').alias('CUSTO_MEDIO_FRETE_POR_OF')).withColumn('CUSTO_MEDIO_FRETE_POR_OF', round(col('CUSTO_MEDIO_FRETE_POR_OF'),2).cast('double'))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Forecast do consumo diário dos insumos

# COMMAND ----------

query = f"""
SELECT
  *
FROM
  `mrp-ia.painel_mrp.tabela_mrp`
"""

df_demanda = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_demanda = df_demanda.withColumn('DATA', to_date(col('DATA')))

#df_demanda = df_demanda.withColumn('DT_MESANO_REFERENCIA', retorna_inicio_do_mes_UDF(col('DATA')))

# COMMAND ----------

df_demanda = df_demanda.groupBy(['CD_EMPRESA','CD_FILIAL','CD_ITEM_COMPONENTE','DATA']).agg(
  sum('CONSUMO_PLANEJADO').alias('CONSUMO_PLANEJADO')
).withColumnRenamed('CD_ITEM_COMPONENTE', 'CD_ITEM')

# COMMAND ----------

df_demanda = df_demanda.join(
  df_ofs,
  how = 'outer',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
).join(
  df_abastecimento,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
).join(
  ITEM.select('CD_ITEM', 'CD_GRUPO_ITEM', 'CD_SUBGRUPO_ITEM'),
  how = 'left',
  on = ['CD_ITEM']
)

# COMMAND ----------

df_demanda = df_demanda.withColumn(
  'DATA',
  when(col('DATA').isNull(), hoje).otherwise(col('DATA'))
).withColumn(
  'CONSUMO_PLANEJADO',
  when(col('CONSUMO_PLANEJADO').isNull(), 0).otherwise(col('CONSUMO_PLANEJADO'))
)

# COMMAND ----------

df_demanda = df_demanda.na.fill(value=0,subset=[
  'LEAD_TIME_PCT50',
  'LEAD_TIME_DESVPAD',
  'LEAD_TIME_DESVPAD_MESES',
  'LEAD_TIME_PCT50_MESES',
  'QT_ITEM_ENTREGA_PCT50',
  'TEMPO_ABASTECIMENTO_PCT50',
  'TEMPO_ABASTECIMENTO_DESVPAD'
])

# COMMAND ----------

df_calendario = df_demanda.select(
  'CD_EMPRESA', 
  'CD_FILIAL', 
  'CD_ITEM',
  'LEAD_TIME_PCT50',
  'LEAD_TIME_DESVPAD',
  'LEAD_TIME_DESVPAD_MESES',
  'LEAD_TIME_PCT50_MESES',
  'QT_ITEM_ENTREGA_PCT50',
  'TEMPO_ABASTECIMENTO_PCT50',
  'TEMPO_ABASTECIMENTO_DESVPAD',
  'CD_GRUPO_ITEM',
  'CD_SUBGRUPO_ITEM'
).distinct().withColumn('DATA', lit(hoje))

# COMMAND ----------

# Join usado para adicionar datas iguais ao dia de hoje, caso não existam para um determinado sku/filial/empresa

df_demanda = df_demanda.join(
  df_calendario,
  how = 'outer',
  on = ['CD_EMPRESA', 
  'CD_FILIAL', 
  'CD_ITEM',
  'LEAD_TIME_PCT50',
  'LEAD_TIME_DESVPAD',
  'LEAD_TIME_DESVPAD_MESES',
  'LEAD_TIME_PCT50_MESES',
  'QT_ITEM_ENTREGA_PCT50',
  'TEMPO_ABASTECIMENTO_PCT50',
  'TEMPO_ABASTECIMENTO_DESVPAD',
  'CD_GRUPO_ITEM',
  'CD_SUBGRUPO_ITEM',
  'DATA']
)

# COMMAND ----------

df_demanda.registerTempTable("df")

df_demanda = spark.sql(
    """
      SELECT 
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_GRUPO_ITEM,
        CD_SUBGRUPO_ITEM,
        DATA,
        QT_ITEM_ENTREGA_PCT50,
        CONSUMO_PLANEJADO,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_DESVPAD,
        CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_DESVPAD
      FROM
       df
    """
)

df_demanda.registerTempTable("df")

df_demanda = spark.sql(
    """
      SELECT 
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_GRUPO_ITEM,
        CD_SUBGRUPO_ITEM,
        DATA,
        QT_ITEM_ENTREGA_PCT50,
        CONSUMO_PLANEJADO,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_DESVPAD,
        CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_DESVPAD
      FROM
       df
    """
)

df_demanda.registerTempTable("df")


df_demanda = spark.sql(
    """
      SELECT 
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_GRUPO_ITEM,
        CD_SUBGRUPO_ITEM,
        DATA,
        QT_ITEM_ENTREGA_PCT50,
        CONSUMO_PLANEJADO,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS LEAD_TIME_DESVPAD,
         CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_DESVPAD
      FROM
       df
    """
)

df_demanda.registerTempTable("df")

df_demanda = spark.sql(
    """
      SELECT 
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_GRUPO_ITEM,
        CD_SUBGRUPO_ITEM,
        DATA,
        QT_ITEM_ENTREGA_PCT50,
        CONSUMO_PLANEJADO,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS LEAD_TIME_DESVPAD,
         CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS TEMPO_ABASTECIMENTO_DESVPAD
      FROM
       df
    """
)

df_demanda.registerTempTable("df")

df_demanda = spark.sql(
    """
      SELECT 
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_GRUPO_ITEM,
        CD_SUBGRUPO_ITEM,
        DATA,
        QT_ITEM_ENTREGA_PCT50,
        CONSUMO_PLANEJADO,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA )
          END AS LEAD_TIME_DESVPAD,
         CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA )
          END AS TEMPO_ABASTECIMENTO_DESVPAD
      FROM
       df
    """
)

# COMMAND ----------

df_demanda = df_demanda.withColumn('LEAD_TIME_PCT50', when(col('LEAD_TIME_PCT50') >= 0, col('LEAD_TIME_PCT50').cast('Integer')).otherwise(lit(0)))

# COMMAND ----------

# df_demanda= df_demanda.withColumn(
#   'LEAD_TIME_PCT50_MESES', round(col('LEAD_TIME_PCT50')/30,3)
# ).withColumn(
#   'LEAD_TIME_DESVPAD_MESES', round(col('LEAD_TIME_DESVPAD')/30,3)
# ).withColumn(
#   'TEMPO_ABASTECIMENTO_PCT50_MESES', round(col('TEMPO_ABASTECIMENTO_PCT50')/30,3)
# ).withColumn(
#   'TEMPO_ABASTECIMENTO_DESVPAD_MESES', round(col('TEMPO_ABASTECIMENTO_DESVPAD')/30,3)
# )

# # COMMAND ----------

df_demanda = df_demanda.join(
  df_inputs.select('CD_FILIAL','CD_ITEM','LEAD_TIME_MANUAL'),
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
)

# # COMMAND ----------

string_cases_sql_lead_time = ""
string_cases_sql_dias_lead_time = ""

string_cases_sql_lead_time_manual = ""
string_cases_sql_dias_lead_time_manual = ""

string_cases_sql_tempo_abastecimento = ""
string_cases_sql_dias_tempo_abastecimento = ""

inicio = 1
fim = 180
step = 1

for valor in range(inicio, fim + step, step):
  
  string_cases_sql_lead_time += f"WHEN (LEAD_TIME_PCT50 <= {valor}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {valor} FOLLOWING ) \n"
  
  string_cases_sql_dias_lead_time += f"WHEN (LEAD_TIME_PCT50 <= {valor}) THEN {valor} \n"
  
for valor in range(inicio, fim + step, step):
  
  string_cases_sql_lead_time_manual += f"WHEN (LEAD_TIME_MANUAL <= {valor}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {valor} FOLLOWING ) \n"
  
  string_cases_sql_dias_lead_time_manual += f"WHEN (LEAD_TIME_MANUAL <= {valor}) THEN {valor} \n"
  
for valor in range(inicio, fim + step, step):
  
  string_cases_sql_tempo_abastecimento += f"WHEN (TEMPO_ABASTECIMENTO_PCT50 <= {valor}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {valor} FOLLOWING ) \n"
  
  string_cases_sql_dias_tempo_abastecimento += f"WHEN (TEMPO_ABASTECIMENTO_PCT50 <= {valor}) THEN {valor} \n"

# COMMAND ----------

df_demanda.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.demanda').save()

query = f"""

SELECT 
*,
CASE 
  {string_cases_sql_lead_time}
  WHEN (LEAD_TIME_PCT50 > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {fim} FOLLOWING )
  ELSE NULL
END AS CONSUMO_CICLO_LEAD_TIME,
CASE 
  {string_cases_sql_dias_lead_time}
  WHEN (LEAD_TIME_PCT50 > {fim}) THEN {fim}
  ELSE NULL
END AS DIAS_CONSIDERADOS_LEAD_TIME,
CASE 
  {string_cases_sql_lead_time_manual}
  WHEN (LEAD_TIME_MANUAL > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {fim} FOLLOWING )
  ELSE NULL
END AS CONSUMO_CICLO_LEAD_TIME_MANUAL,
CASE 
  {string_cases_sql_dias_lead_time_manual}
  WHEN (LEAD_TIME_MANUAL > {fim}) THEN {fim}
  ELSE NULL
END AS DIAS_CONSIDERADOS_LEAD_TIME_MANUAL,
CASE 
  {string_cases_sql_tempo_abastecimento}
  WHEN (TEMPO_ABASTECIMENTO_PCT50 > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY UNIX_DATE(DATA) RANGE BETWEEN CURRENT ROW AND {fim} FOLLOWING )
  ELSE NULL
END AS CONSUMO_CICLO_ABASTECIMENTO,
CASE 
  {string_cases_sql_dias_tempo_abastecimento}
  WHEN (TEMPO_ABASTECIMENTO_PCT50 > {fim}) THEN {fim}
  ELSE NULL
END AS DIAS_CONSIDERADOS_ABASTECIMENTO,
SUM(CONSUMO_PLANEJADO) OVER (
  PARTITION BY 
  CD_EMPRESA, CD_FILIAL, CD_ITEM 
  ORDER BY 
    UNIX_DATE(DATA)
  RANGE 
    BETWEEN CURRENT ROW AND 30 FOLLOWING
) AS CONSUMO_PROX_30_DIAS
FROM
  `mrp-ia.mrp_estoque.demanda`
"""

df_demanda = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# df_demanda = spark.sql(
#     f"""
#       SELECT 
#         *,
#         CASE 
#           {string_cases_sql_lead_time}
#           WHEN (LEAD_TIME_PCT50 > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp) RANGE BETWEEN CURRENT ROW AND INTERVAL {fim} DAYS FOLLOWING )
#           ELSE NULL
#         END AS CONSUMO_CICLO_LEAD_TIME,
#         CASE 
#           {string_cases_sql_dias_lead_time}
#           WHEN (LEAD_TIME_PCT50 > {fim}) THEN {fim}
#           ELSE NULL
#         END AS DIAS_CONSIDERADOS_LEAD_TIME,
#         CASE 
#           {string_cases_sql_lead_time_manual}
#           WHEN (LEAD_TIME_MANUAL > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp) RANGE BETWEEN CURRENT ROW AND INTERVAL {fim} DAYS FOLLOWING )
#           ELSE NULL
#         END AS CONSUMO_CICLO_LEAD_TIME_MANUAL,
#         CASE 
#           {string_cases_sql_dias_lead_time_manual}
#           WHEN (LEAD_TIME_MANUAL > {fim}) THEN {fim}
#           ELSE NULL
#         END AS DIAS_CONSIDERADOS_LEAD_TIME_MANUAL,
#         CASE 
#           {string_cases_sql_tempo_abastecimento}
#           WHEN (TEMPO_ABASTECIMENTO_PCT50 > {fim}) THEN SUM(CONSUMO_PLANEJADO) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp) RANGE BETWEEN CURRENT ROW AND INTERVAL {fim} DAYS FOLLOWING )
#           ELSE NULL
#         END AS CONSUMO_CICLO_ABASTECIMENTO,
#         CASE 
#           {string_cases_sql_dias_tempo_abastecimento}
#           WHEN (TEMPO_ABASTECIMENTO_PCT50 > {fim}) THEN {fim}
#           ELSE NULL
#         END AS DIAS_CONSIDERADOS_ABASTECIMENTO,
#         SUM(CONSUMO_PLANEJADO) OVER (
#          PARTITION BY 
#           CD_EMPRESA, CD_FILIAL, CD_ITEM 
#          ORDER BY 
#            CAST(DATA AS timestamp) 
#          RANGE 
#            BETWEEN CURRENT ROW AND INTERVAL 30 DAYS FOLLOWING
#         ) AS CONSUMO_PROX_30_DIAS
#       FROM
#        df
#     """
# )

# # COMMAND ----------

for coluna in ['CONSUMO_CICLO_LEAD_TIME','CONSUMO_CICLO_LEAD_TIME_MANUAL','CONSUMO_CICLO_ABASTECIMENTO','CONSUMO_PROX_30_DIAS']: #,'CONSUMO_PROX_60_DIAS', 'CONSUMO_PROX_90_DIAS']:
  
  df_demanda = df_demanda.withColumn(coluna, round(col(coluna),2))

# # COMMAND ----------

df_demanda = df_demanda.filter(col('DATA') == hoje)

# # COMMAND ----------

df_demanda = df_demanda.drop('CD_GRUPO_ITEM','CD_SUBGRUPO_ITEM')

# df_demanda.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.demanda').save()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Consumo Histórico

# COMMAND ----------

query = f"""
WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  MOV.CD_EMPRESA,
  FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  MOV.CD_ITEM,
  SUM(-MOV.QT_MOVIMENTO) AS QT_MOVIMENTO,
  SUM(-MOV.VL_MOVIMENTO) AS VL_MOVIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(MOV.DT_MOVIMENTO, 10)) AS DT_MOVIMENTO
FROM    
  `seara-analytics-prod.stg_erp_seara.movimento_etq_suprimento` MOV
LEFT JOIN
  FILIAL
ON
  MOV.CD_FILIAL = FILIAL.CD_FILIAL
  AND MOV.CD_EMPRESA = FILIAL.CD_EMPRESA
WHERE 
  MOV.CD_OPERACAO_SUPRIMENTO IN (900, 901, 906, 907, 910, 911, 916, 917, 918, 919, 920, 928, 929, 932, 933, 934, 935, 936, 937, 938, 939, 970, 971, 972, 973)
  AND MOV.CD_LOCAL_ESTOQUE != 299
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', MOV.DT_MOVIMENTO) between '{str(inicio_do_mes_d_menos_3)}' and '{str(final_do_mes_anterior)}'
  AND FILIAL.CD_FILIAL_CENTRALIZADORA IN ({filiais})
  AND MOV.CD_ITEM NOT IN  (103179, 248606, 255505, 331988, 373818, 376388, 405033, 470643, 508977)
GROUP BY
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  DT_MOVIMENTO
ORDER BY
  DT_MOVIMENTO  
"""

# df_consumo_historico = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

query = f"""
WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  FILIAL_CGC10.CD_EMPRESA,
  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  -- REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL,
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM,
  ITEM3.NM_ITEM,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.QT_ITEM),2) AS QT_MOVIMENTO,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.VL_TOTAL_ITEM),2) AS VL_MOVIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) AS DT_MOVIMENTO
FROM
  `seara-analytics-prod.stg_erp_seara.requisicao_suprimento_snac`AS REQUISICAO_SUPRIMENTO_SNAC
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_requisicao_spm_snac` AS ITEM_REQUISICAO_SPM_SNAC
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_REQUISICAO_SUPRIMENTO=ITEM_REQUISICAO_SPM_SNAC.CD_REQUISICAO_SUPRIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item` AS ITEM3 
ON
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM=ITEM3.CD_ITEM
LEFT JOIN 
  FILIAL AS FILIAL_CGC10
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL=FILIAL_CGC10.CD_FILIAL
  AND REQUISICAO_SUPRIMENTO_SNAC.CD_EMPRESA=FILIAL_CGC10.CD_EMPRESA

WHERE
  ITEM_REQUISICAO_SPM_SNAC.ID_INTEGRACAO_ESTOQUE  =  'S'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_SITUACAO  IN  (4, 5, 6)
  AND  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) between '{str(inicio_do_mes_d_menos_3)}' and '{str(final_do_mes_anterior)}'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_OPERACAO_SUPRIMENTO  IN  (900, 901, 906, 907, 910, 911, 916, 917, 918, 919, 920, 928, 929, 932, 933, 934, 935, 936, 937, 938, 939, 970, 971, 972, 973, 981, 983, 984)
  -- AND  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM  NOT IN  (103179, 248606, 255505, 331988, 373818, 376388, 405033, 470643, 508977)
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_LOCAL_ESTOQUE_ATI  !=  299
  AND  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA  IN  ({filiais})
    
GROUP BY
  CD_EMPRESA,
  CD_FILIAL_CENTRALIZADORA,
  CD_FILIAL,
  CD_ITEM,
  NM_ITEM,
  DT_MOVIMENTO

ORDER BY 
  CD_EMPRESA,CD_FILIAL_CENTRALIZADORA,CD_ITEM,DT_MOVIMENTO
"""

df_consumo_historico = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# df_consumo_historico.filter(
#   (col('CD_FILIAL') == 4001)
#   &(col('CD_ITEM') == 40478)
# ).display()

# COMMAND ----------

for coluna in ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM']:

  df_consumo_historico = df_consumo_historico.withColumn(
    coluna, col(coluna).cast(IntegerType())
  )
  
df_consumo_historico = df_consumo_historico.withColumn(
    'QT_MOVIMENTO', col('QT_MOVIMENTO').cast(DoubleType())
  )

df_consumo_historico = df_consumo_historico.withColumn('MES', retorna_inicio_do_mes_UDF(col('DT_MOVIMENTO')))

# COMMAND ----------

df_consumo_historico.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.consumo_historico_diario_estoque_ideal').save()

# COMMAND ----------

df_consumo_historico = df_consumo_historico.groupBy('CD_EMPRESA','CD_FILIAL','CD_ITEM','MES').agg(sum('QT_MOVIMENTO').alias('QT_MOVIMENTO'))

# COMMAND ----------

spark.catalog.dropTempView("df")

df_consumo_historico.createOrReplaceTempView("df")

df_consumo_historico = spark.sql(
  """
    SELECT 
        *,
        percentile_approx(QT_MOVIMENTO, 0.5) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS CONSUMO_MENSAL_HISTORICO_PCT50,
        percentile_approx(QT_MOVIMENTO, 0.25) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q1,
        percentile_approx(QT_MOVIMENTO, 0.75) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q3
     FROM 
       df
   """
)

# COMMAND ----------

df_consumo_historico = df_consumo_historico.withColumn(
  'IQR', col('Q3') - col('Q1')
).withColumn(
  'VAL_MAXIMO', col('Q3') + 1.5*col('IQR')
).withColumn(
  'VAL_MINIMO', when(col('Q1') - 1.5*col('IQR') < 0, 0).otherwise(col('Q1') - 1.5*col('IQR'))
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_consumo_historico.createOrReplaceTempView("df")

df_consumo_historico = spark.sql(
  """
    SELECT 
        *,
       STDDEV(QT_MOVIMENTO) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS QT_MOVIMENTO_DESVPAD
     FROM 
       df
     WHERE
       QT_MOVIMENTO >= VAL_MINIMO
       AND QT_MOVIMENTO <= VAL_MAXIMO
   """
).withColumn('QT_MOVIMENTO_DESVPAD', round(col('QT_MOVIMENTO_DESVPAD'),2))

# COMMAND ----------

df_consumo_historico_bq = df_consumo_historico.alias('df_consumo_historico_bq')

for coluna in ['QT_MOVIMENTO','CONSUMO_MENSAL_HISTORICO_PCT50','Q1','Q3','IQR','VAL_MAXIMO','VAL_MINIMO','QT_MOVIMENTO_DESVPAD']:

  df_consumo_historico_bq = df_consumo_historico_bq.withColumn(coluna, round(col(coluna),2))

df_consumo_historico_bq.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.consumo_historico_estoque_ideal').save()

# COMMAND ----------

df_consumo_historico = df_consumo_historico.filter(
  (col('QT_MOVIMENTO') >= col('VAL_MINIMO'))
  &(col('QT_MOVIMENTO') <= col('VAL_MAXIMO'))
)

# COMMAND ----------

df_consumo_historico = df_consumo_historico.groupBy('CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM').agg(
  avg('QT_MOVIMENTO').alias('CONSUMO_MENSAL_HISTORICO'),
  avg('QT_MOVIMENTO_DESVPAD').alias('DESVPAD_CONSUMO_MENSAL_HISTORICO')
).withColumn('CONSUMO_MENSAL_HISTORICO', round(col('CONSUMO_MENSAL_HISTORICO'),2)).withColumn('DESVPAD_CONSUMO_MENSAL_HISTORICO', round(col('DESVPAD_CONSUMO_MENSAL_HISTORICO'),2))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Consumo dos últimos 30 dias

# COMMAND ----------

# Utilizaremos o consumo do último mês fechado devido à baixa confiabilidade dos dados para meses correntes

query = f"""
WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  FILIAL_CGC10.CD_EMPRESA,
  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  -- REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL,
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM,
  ITEM3.NM_ITEM,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.QT_ITEM),2) AS QT_MOVIMENTO,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.VL_TOTAL_ITEM),2) AS VL_MOVIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) AS DT_MOVIMENTO
FROM
  `seara-analytics-prod.stg_erp_seara.requisicao_suprimento_snac`AS REQUISICAO_SUPRIMENTO_SNAC
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_requisicao_spm_snac` AS ITEM_REQUISICAO_SPM_SNAC
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_REQUISICAO_SUPRIMENTO=ITEM_REQUISICAO_SPM_SNAC.CD_REQUISICAO_SUPRIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item` AS ITEM3 
ON
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM=ITEM3.CD_ITEM
LEFT JOIN 
  FILIAL AS FILIAL_CGC10
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL=FILIAL_CGC10.CD_FILIAL
  AND REQUISICAO_SUPRIMENTO_SNAC.CD_EMPRESA=FILIAL_CGC10.CD_EMPRESA

WHERE
  ITEM_REQUISICAO_SPM_SNAC.ID_INTEGRACAO_ESTOQUE  =  'S'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_SITUACAO  IN  (4, 5, 6)
  -- AND  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) between '{str(trinta_dias_atras)}' and '{str(hoje)}'
  AND  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) between '{str(inicio_do_mes_anterior)}' and '{str(final_do_mes_anterior)}'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_OPERACAO_SUPRIMENTO  IN  (900, 901, 906, 907, 910, 911, 916, 917, 918, 919, 920, 928, 929, 932, 933, 934, 935, 936, 937, 938, 939, 970, 971, 972, 973, 981, 983, 984)
  -- AND  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM  NOT IN  (103179, 248606, 255505, 331988, 373818, 376388, 405033, 470643, 508977)
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_LOCAL_ESTOQUE_ATI  !=  299
  AND  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA  IN  ({filiais})
  
GROUP BY
  CD_EMPRESA,
  CD_FILIAL_CENTRALIZADORA,
  CD_FILIAL,
  CD_ITEM,
  NM_ITEM,
  DT_MOVIMENTO

ORDER BY 
  CD_EMPRESA,
  CD_FILIAL_CENTRALIZADORA,
  CD_ITEM,
  DT_MOVIMENTO
"""

df_30_dias = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

for coluna in ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM']:
  
  df_30_dias = df_30_dias.withColumn(coluna, col(coluna).cast(IntegerType()))
  
for coluna in ['QT_MOVIMENTO', 'VL_MOVIMENTO']:
  
  df_30_dias = df_30_dias.withColumn(coluna, col(coluna).cast(DoubleType()))

# COMMAND ----------

df_30_dias.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.consumo_ultimos_30_dias_estoque_ideal').save()

# COMMAND ----------

df_30_dias = df_30_dias.groupBy('CD_EMPRESA','CD_FILIAL','CD_ITEM').agg(
  sum('QT_MOVIMENTO').alias('QT_MOVIMENTO'),
  sum('VL_MOVIMENTO').alias('VL_MOVIMENTO')
).withColumn('QT_MOVIMENTO', round(col('QT_MOVIMENTO'),2)).withColumn('VL_MOVIMENTO', round(col('VL_MOVIMENTO'),2))

# COMMAND ----------

df_30_dias = df_30_dias.withColumnRenamed('QT_MOVIMENTO', 'QT_CONSUMO_ULTIMOS_30_DIAS').withColumnRenamed('VL_MOVIMENTO', 'VL_CONSUMO_ULTIMOS_30_DIAS')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Consolidação e cálculo do Estoque Ideal

# COMMAND ----------

df_cycle = df_saldo_estoque_atual.select('CD_EMPRESA','CD_FILIAL','FILTRO_FILIAL', 'CD_ITEM', 'NM_ITEM', 'CD_GRUPO_ITEM', 'CD_SUBGRUPO_ITEM', 'NM_GRUPO', 'NM_SUBGRUPO', 'CD_LOCAL_ESTOQUE','QT_SALDO_ATUAL','VL_SALDO_ATUAL','VL_CUSTO_UNITARIO_ATUAL').alias('df_cycle')

for coluna in ['QT_SALDO_ATUAL','VL_SALDO_ATUAL','VL_CUSTO_UNITARIO_ATUAL']:
  
  df_cycle = df_cycle.withColumn(coluna, round(col(coluna),2).cast('double'))

# COMMAND ----------

df_cycle = df_cycle.groupBy(['CD_EMPRESA','CD_FILIAL', 'FILTRO_FILIAL','CD_ITEM', 'NM_ITEM', 'CD_GRUPO_ITEM', 'CD_SUBGRUPO_ITEM', 'NM_GRUPO', 'NM_SUBGRUPO']).agg(
  sum('QT_SALDO_ATUAL').alias('QT_SALDO_ATUAL'),
  sum('VL_SALDO_ATUAL').alias('VL_SALDO_ATUAL')
)

# # COMMAND ----------

df_cycle = df_cycle.filter(
  (col('QT_SALDO_ATUAL') > 0)
  &(col('VL_SALDO_ATUAL') > 0)
)

# # COMMAND ----------

df_cycle = df_cycle.withColumn('VL_CUSTO_UNITARIO_ATUAL', col('VL_SALDO_ATUAL')/col('QT_SALDO_ATUAL'))

# # COMMAND ----------

df_cycle = df_cycle.join(
  df_consumo_historico,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
)

print('Join com o consumo histórico finalizado.')

df_cycle = df_cycle.join(
  df_30_dias,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
)

print('Join com o consumo dos últimos 30 dias finalizado.')

df_cycle = df_cycle.join(
  df_demanda,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
)

print('Join com a demanda histórica finalizado.')

df_cycle = df_cycle.join(
  df_custo_cycle,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
)

print('Join com o custo finalizado.')

# COMMAND ----------

# PREENCHE PROGRESSIVAMENTE OS VALORES VAZIOS DAS COLUNAS 

spark.catalog.dropTempView("df")

df_cycle.createOrReplaceTempView("df")

df_cycle = spark.sql(
    f"""
      SELECT 
       CD_EMPRESA,
       CD_FILIAL,
       CD_ITEM,
       FILTRO_FILIAL,
       NM_ITEM,
       CD_GRUPO_ITEM,
       CD_SUBGRUPO_ITEM,
       NM_GRUPO,
       NM_SUBGRUPO,
       QT_SALDO_ATUAL,
       VL_SALDO_ATUAL,
       VL_CUSTO_UNITARIO_ATUAL,
       CASE
          WHEN DATA IS NOT NULL THEN DATA
          ELSE DATE('{str(hoje)}')
          END AS DATA,
       QT_ITEM_ENTREGA_PCT50,
       CONSUMO_MENSAL_HISTORICO,
       DESVPAD_CONSUMO_MENSAL_HISTORICO,
       QT_CONSUMO_ULTIMOS_30_DIAS,
       VL_CONSUMO_ULTIMOS_30_DIAS,
       CONSUMO_CICLO_LEAD_TIME,
       DIAS_CONSIDERADOS_LEAD_TIME,
       CONSUMO_CICLO_ABASTECIMENTO,
       DIAS_CONSIDERADOS_ABASTECIMENTO,
       CONSUMO_CICLO_LEAD_TIME_MANUAL,
       DIAS_CONSIDERADOS_LEAD_TIME_MANUAL,
       CONSUMO_PROX_30_DIAS,
       LEAD_TIME_MANUAL,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS LEAD_TIME_DESVPAD,
        CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_DESVPAD,
        CASE
          WHEN CUSTO_MEDIO_FRETE_POR_OF IS NOT NULL THEN CUSTO_MEDIO_FRETE_POR_OF
          ELSE AVG(CUSTO_MEDIO_FRETE_POR_OF) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM, CD_SUBGRUPO_ITEM )
          END AS CUSTO_MEDIO_FRETE_POR_OF
      FROM
       df
    """
)

print('Preenchimento de lead time utilizando média até o nível subgrupo de item concluído.')

spark.catalog.dropTempView("df")

df_cycle.createOrReplaceTempView("df")

df_cycle = spark.sql(
    f"""
      SELECT 
       CD_EMPRESA,
       CD_FILIAL,
       CD_ITEM,
       FILTRO_FILIAL,
       NM_ITEM,
       CD_GRUPO_ITEM,
       CD_SUBGRUPO_ITEM,
       NM_GRUPO,
       NM_SUBGRUPO,
       QT_SALDO_ATUAL,
       VL_SALDO_ATUAL,
       VL_CUSTO_UNITARIO_ATUAL,
       DATA,
       QT_ITEM_ENTREGA_PCT50,
       CONSUMO_MENSAL_HISTORICO,
       DESVPAD_CONSUMO_MENSAL_HISTORICO,
       QT_CONSUMO_ULTIMOS_30_DIAS,
       VL_CONSUMO_ULTIMOS_30_DIAS,
       CONSUMO_CICLO_LEAD_TIME,
       DIAS_CONSIDERADOS_LEAD_TIME,
       CONSUMO_CICLO_ABASTECIMENTO,
       DIAS_CONSIDERADOS_ABASTECIMENTO,
       CONSUMO_CICLO_LEAD_TIME_MANUAL,
       DIAS_CONSIDERADOS_LEAD_TIME_MANUAL,
       CONSUMO_PROX_30_DIAS,
       LEAD_TIME_MANUAL,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS LEAD_TIME_DESVPAD,
        CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS TEMPO_ABASTECIMENTO_DESVPAD,
        CASE
          WHEN CUSTO_MEDIO_FRETE_POR_OF IS NOT NULL THEN CUSTO_MEDIO_FRETE_POR_OF
          ELSE AVG(CUSTO_MEDIO_FRETE_POR_OF) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_GRUPO_ITEM )
          END AS CUSTO_MEDIO_FRETE_POR_OF
      FROM
       df
    """
)

print('Preenchimento de lead time utilizando média até o nível grupo de item concluído.')

spark.catalog.dropTempView("df")

df_cycle.createOrReplaceTempView("df")

df_cycle = spark.sql(
    f"""
      SELECT 
       CD_EMPRESA,
       CD_FILIAL,
       CD_ITEM,
       FILTRO_FILIAL,
       NM_ITEM,
       CD_GRUPO_ITEM,
       CD_SUBGRUPO_ITEM,
       NM_GRUPO,
       NM_SUBGRUPO,
       QT_SALDO_ATUAL,
       VL_SALDO_ATUAL,
       VL_CUSTO_UNITARIO_ATUAL,
       DATA,
       QT_ITEM_ENTREGA_PCT50,
       CONSUMO_MENSAL_HISTORICO,
       DESVPAD_CONSUMO_MENSAL_HISTORICO,
       QT_CONSUMO_ULTIMOS_30_DIAS,
       VL_CONSUMO_ULTIMOS_30_DIAS,
       CONSUMO_CICLO_LEAD_TIME,
       DIAS_CONSIDERADOS_LEAD_TIME,
       CONSUMO_CICLO_ABASTECIMENTO,
       DIAS_CONSIDERADOS_ABASTECIMENTO,
       CONSUMO_CICLO_LEAD_TIME_MANUAL,
       DIAS_CONSIDERADOS_LEAD_TIME_MANUAL,
       CONSUMO_PROX_30_DIAS,
       LEAD_TIME_MANUAL,
        CASE
          WHEN LEAD_TIME_PCT50 IS NOT NULL THEN LEAD_TIME_PCT50
          ELSE AVG(LEAD_TIME_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS LEAD_TIME_PCT50,
        CASE
          WHEN LEAD_TIME_DESVPAD IS NOT NULL THEN LEAD_TIME_DESVPAD
          ELSE AVG(LEAD_TIME_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS LEAD_TIME_DESVPAD,
        CASE
          WHEN TEMPO_ABASTECIMENTO_PCT50 IS NOT NULL THEN TEMPO_ABASTECIMENTO_PCT50
          ELSE AVG(TEMPO_ABASTECIMENTO_PCT50) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS TEMPO_ABASTECIMENTO_PCT50,
        CASE
          WHEN TEMPO_ABASTECIMENTO_DESVPAD IS NOT NULL THEN TEMPO_ABASTECIMENTO_DESVPAD
          ELSE AVG(TEMPO_ABASTECIMENTO_DESVPAD) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS TEMPO_ABASTECIMENTO_DESVPAD,
        CASE
          WHEN CUSTO_MEDIO_FRETE_POR_OF IS NOT NULL THEN CUSTO_MEDIO_FRETE_POR_OF
          ELSE AVG(CUSTO_MEDIO_FRETE_POR_OF) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL )
          END AS CUSTO_MEDIO_FRETE_POR_OF
      FROM
       df
    """
)

print('Preenchimento de lead time utilizando média até o nível filial concluído.')

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'LEAD_TIME_PCT50_MESES', round(col('LEAD_TIME_PCT50')/30,3)
).withColumn(
  'LEAD_TIME_MANUAL_MESES', round(col('LEAD_TIME_MANUAL')/30,3)
).withColumn(
  'LEAD_TIME_DESVPAD_MESES', round(col('LEAD_TIME_DESVPAD')/30,3)
).withColumn(
  'TEMPO_ABASTECIMENTO_PCT50_MESES', round(col('TEMPO_ABASTECIMENTO_PCT50')/30,3)
).withColumn(
  'TEMPO_ABASTECIMENTO_DESVPAD_MESES', round(col('TEMPO_ABASTECIMENTO_DESVPAD')/30,3)
)

# COMMAND ----------

# HOLDING COSTS

taxa_selic = 0.0775

taxa_selic_mensal = (1 + taxa_selic)**(1/12) - 1

outros_custos = 0.1

# COMMAND ----------

df_cycle = df_cycle.join(
  ITEM.select('CD_ITEM','ID_ORIGEM','DS_ORIGEM'),
  how = 'left',
  on = ['CD_ITEM']
).join(
  df_inputs.select('CD_FILIAL','CD_ITEM','ORIGEM_MANUAL'),
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
)

# COMMAND ----------

df_cycle = df_cycle.join(
  df_ranking_itens,
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
)

# COMMAND ----------

df_cycle = df_cycle.withColumn('ESTOQUE_CICLO', 
  round(col('CONSUMO_CICLO_ABASTECIMENTO'),2)
).withColumn('TEMPO_DE_CICLO', 
  round(col('DIAS_CONSIDERADOS_ABASTECIMENTO'),2)
)

# COMMAND ----------

df_cycle = df_cycle.join(
  df_lote_minimo,
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
).join(
  df_desvio_entrega,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'ESTOQUE_CICLO',
  when((col('ESTOQUE_CICLO') > 0)&(col('LOTE_MINIMO') > col('ESTOQUE_CICLO')), col('LOTE_MINIMO')).otherwise(col('ESTOQUE_CICLO'))
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'DESVPAD_CONSUMO_DIARIO_HISTORICO', round(col('DESVPAD_CONSUMO_MENSAL_HISTORICO')/lit(30),2)
).withColumn(
  'CONSUMO_DIARIO_HISTORICO', round(col('CONSUMO_MENSAL_HISTORICO')/lit(30),2)
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'ESTOQUE_SEGURANCA_CALCULADO', when(
  col('CONSUMO_CICLO_LEAD_TIME_MANUAL').isNull(),
  round(col('SCORE_Z')*sqrt( col('LEAD_TIME_PCT50') * (col('DESVPAD_CONSUMO_DIARIO_HISTORICO'))**2 + ( col('DESVIO_ENTREGA_MEDIO')*col('CONSUMO_DIARIO_HISTORICO') )**2 ), 2)
  ).otherwise(
    round(col('SCORE_Z')*sqrt( col('LEAD_TIME_MANUAL') * (col('DESVPAD_CONSUMO_DIARIO_HISTORICO'))**2 + ( col('DESVIO_ENTREGA_MEDIO')*col('CONSUMO_DIARIO_HISTORICO') )**2 ), 2)
  )
).withColumn(
  'ESTOQUE_SEGURANCA',
  when(col('ESTOQUE_SEGURANCA_CALCULADO') <= 5*col('ESTOQUE_CICLO'), col('ESTOQUE_SEGURANCA_CALCULADO')).otherwise(5*col('ESTOQUE_CICLO'))
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'ESTOQUE_IDEAL',
  col('ESTOQUE_CICLO') + col('ESTOQUE_SEGURANCA')
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'FILTRO_ITEM',
  concat(col('CD_ITEM').cast(StringType()),lit(' - '),col('NM_ITEM'))
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'ESTOQUE_MIN',
  col('ESTOQUE_SEGURANCA')
).withColumn(
  'ESTOQUE_MAX',
  col('ESTOQUE_IDEAL')
)

# COMMAND ----------

df_cycle = df_cycle.withColumn(
  'GIRO_ETQ_ATUAL',
  round(col('QT_SALDO_ATUAL')/col('QT_CONSUMO_ULTIMOS_30_DIAS')*30,1)
).withColumn(
  'GIRO_ETQ_MIN',
  round(col('ESTOQUE_MIN')/col('QT_CONSUMO_ULTIMOS_30_DIAS')*30,1)
).withColumn(
  'GIRO_ETQ_MAX',
  round(col('ESTOQUE_MAX')/col('QT_CONSUMO_ULTIMOS_30_DIAS')*30,1)
).withColumn(
  'QT_SALDO_ATUAL',
  round(col('QT_SALDO_ATUAL'),2)
).withColumn(
  'VL_SALDO_ATUAL',
  round(col('VL_SALDO_ATUAL'),2)
).withColumn(
  'VL_CUSTO_UNITARIO_ATUAL',
  round(col('VL_CUSTO_UNITARIO_ATUAL'),2)
)

# COMMAND ----------

df_cycle = df_cycle.select(
 'CD_EMPRESA',
 'CD_FILIAL',
 'FILTRO_FILIAL',
 'CD_ITEM',
 'NM_ITEM',
 'FILTRO_ITEM',
 'CD_GRUPO_ITEM',
 'CD_SUBGRUPO_ITEM',
 'NM_GRUPO',
 'NM_SUBGRUPO',
 'QT_SALDO_ATUAL',
 'VL_SALDO_ATUAL',
 'VL_CUSTO_UNITARIO_ATUAL',
 'DATA',
 'QT_ITEM_ENTREGA_PCT50',
 'DESVPAD_CONSUMO_MENSAL_HISTORICO',
 'DESVPAD_CONSUMO_DIARIO_HISTORICO',
 'VL_CONSUMO_ULTIMOS_30_DIAS',
 'DIAS_CONSIDERADOS_LEAD_TIME',
 'DIAS_CONSIDERADOS_ABASTECIMENTO',
 'DIAS_CONSIDERADOS_LEAD_TIME_MANUAL',
 'CONSUMO_MENSAL_HISTORICO',
 'CONSUMO_DIARIO_HISTORICO',
 'QT_CONSUMO_ULTIMOS_30_DIAS',
 'CONSUMO_PROX_30_DIAS',
 'CONSUMO_CICLO_ABASTECIMENTO', 
 'CONSUMO_CICLO_LEAD_TIME',
 'CONSUMO_CICLO_LEAD_TIME_MANUAL', 
 'LEAD_TIME_MANUAL',
 'LEAD_TIME_PCT50',
 'LEAD_TIME_DESVPAD',
 'TEMPO_ABASTECIMENTO_PCT50',
 'TEMPO_ABASTECIMENTO_DESVPAD',
 'DESVIO_ENTREGA_MEDIO',
 'CUSTO_MEDIO_FRETE_POR_OF',
 'LEAD_TIME_PCT50_MESES',
 'LEAD_TIME_MANUAL_MESES',
 'LEAD_TIME_DESVPAD_MESES',
 'TEMPO_ABASTECIMENTO_PCT50_MESES',
 'TEMPO_ABASTECIMENTO_DESVPAD_MESES',
 'ID_ORIGEM',
 'DS_ORIGEM',
 'ORIGEM_MANUAL',
 'NUM_ITENS_AFETADOS',
 'LOTE_MINIMO',
 'ESTOQUE_CICLO',
 'TEMPO_DE_CICLO',
 'SCORE_Z',
 'ESTOQUE_SEGURANCA_CALCULADO',
 'ESTOQUE_SEGURANCA',
 'ESTOQUE_IDEAL',
 'ESTOQUE_MIN',
 'ESTOQUE_MAX',
 'GIRO_ETQ_ATUAL',
 'GIRO_ETQ_MIN',
 'GIRO_ETQ_MAX'
)

# COMMAND ----------

df_cycle.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.estoque_ideal_plantas').save()

# COMMAND ----------

df_cycle_his = df_cycle.alias('df_cycle_his')

df_cycle_his = df_cycle_his.withColumn('DT_REFERENCIA', lit(hoje))

delete_statement = f"""

DELETE mrp-ia.mrp_estoque.his_estoque_ideal_plantas
WHERE
  DT_REFERENCIA = '{hoje}'
"""

query_job = client.query(delete_statement)

query_job.result()

df_cycle_his.write.format("bigquery").mode('append').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.mrp_estoque.his_estoque_ideal_plantas').save()
