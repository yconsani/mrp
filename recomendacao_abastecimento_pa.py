# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ### Importando biliotecas e definindo variáveis

# COMMAND ----------

# !pip install fsspec
# !pip install gcsfs
# !pip install openpyxl
# !pip install google-cloud-bigquery
# !pip install google-api-core==1.31.1
# !pip install db-dtypes
# !pip install pandasql

# COMMAND ----------

# import pandas as pd
import datetime
import numpy as np
import pandas as pd
import pytz

from pandasql import sqldf

pysqldf = lambda q: sqldf(q, globals())

from google.cloud import bigquery as bq
client = bq.Client()

# COMMAND ----------

# função que para facilitar o carregamento de tabelas no bigquery

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.
  
#   df.to_gbq(
#     f'{nome_dataset}.{nome_tabela}',
#     project_id=id_projeto,
#     chunksize=None,
#     if_exists=modo
#   )
  
#   return print(f'tabela {nome_tabela} carregada no local {id_projeto}.{nome_dataset}.{nome_tabela}')  #print(f"Loaded dataframe to {table_ref.path}")

# COMMAND ----------

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Projeção dos Estoques

# COMMAND ----------

query = """

SELECT
  CD_FILIAL,
  CD_ITEM,
  VOLUME_META_DIARIZADO,
  DATA
FROM
  mrp-ia.estoque_ideal_produto_acabado.volume_meta_projetado
"""

df_projecao_volume_meta = client.query(query).to_dataframe()

# COMMAND ----------

query = """

SELECT
  CD_FILIAL,
  CD_ITEM,
  DATA_REFERENCIA_ESTOQUE,
  QT_PESO_ATUAL,
  ESTOQUE_MINIMO,
  ESTOQUE_MAXIMO,
  DIAS_CONSIDERADOS_CICLO_ABASTECIMENTO
FROM
  mrp-ia.estoque_ideal_produto_acabado.resultados
"""

df = client.query(query).to_dataframe()

# COMMAND ----------

df_projecao = df.merge(
  df_projecao_volume_meta,
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
)

df_projecao['DATA'] = df_projecao['DATA'].dt.date

df_projecao['DATA_REFERENCIA_ESTOQUE'] = pd.to_datetime(df_projecao['DATA_REFERENCIA_ESTOQUE']).dt.date

df_projecao.sort_values(by=['CD_FILIAL', 'CD_ITEM', 'DATA'], inplace = True)

df_projecao.rename(columns={'QT_PESO_ATUAL': 'QT_PESO_INICIAL'}, inplace = True)

# COMMAND ----------

q = """
SELECT 
  *,
  sum(VOLUME_META_DIARIZADO) OVER (PARTITION BY CD_FILIAL, CD_ITEM ORDER BY DATA ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS VOLUME_META_DIARIZADO_ACUM
FROM 
  df_projecao ; 
"""

df_projecao = pysqldf(q)

# COMMAND ----------

df_projecao['DATA'] = pd.to_datetime(df_projecao['DATA']).dt.date

df_projecao['DATA_REFERENCIA_ESTOQUE'] = pd.to_datetime(df_projecao['DATA_REFERENCIA_ESTOQUE']).dt.date

# COMMAND ----------

df_projecao_pandas = df_projecao.copy()

# COMMAND ----------

df_projecao_pandas['QT_PESO_PROJETADO'] = np.nan

df_projecao_pandas['QT_ABASTECIMENTO_SUGERIDA'] = np.nan

# COMMAND ----------

lista_filiais = list(df_projecao_pandas['CD_FILIAL'].unique())

# COMMAND ----------

data_maxima_projecao = hoje + datetime.timedelta(days=30)

# COMMAND ----------

# Deleta todas as linhas da tabela antes de começar a populá-la novamente

delete_statement = f"""

DELETE mrp-ia.estoque_ideal_produto_acabado.recomendacao_abastecimento
WHERE
  1 = 1
"""

query_job = client.query(delete_statement)

query_job.result()

# COMMAND ----------

for cd_filial in lista_filiais:
  
  df_projecao_pandas_filial = df_projecao_pandas[
      (df_projecao_pandas ['CD_FILIAL'] == cd_filial)
      &(df_projecao_pandas['DATA'] <= data_maxima_projecao)
    ].copy()

  lista_itens = list(df_projecao_pandas.loc[
    (df_projecao_pandas ['CD_FILIAL'] == cd_filial)
  , 'CD_ITEM'].unique())
  
#   print(f'Filial: {cd_filial}')
  
  print(f'Horário: {datetime.datetime.now().hour}:{datetime.datetime.now().minute}:{datetime.datetime.now().second}.   Progresso filiais: {round((lista_filiais.index(cd_filial)+1)/len(lista_filiais),2)}. Filial em execução: {cd_filial}.')
  
  for cd_item in lista_itens:
    
#     print(f'Horário: {datetime.datetime.now().hour}:{datetime.datetime.now().minute}:{datetime.datetime.now().second}.   Progresso itens: {round((lista_itens.index(cd_item)+1)/len(lista_itens),2)}')

    df_aux = df_projecao_pandas_filial[
      (df_projecao_pandas_filial['CD_ITEM'] == cd_item)
    ].copy()

    lista_datas = list(df_aux.loc[df_aux['DATA'] <= data_maxima_projecao, 'DATA'])
    
    if len(lista_datas) == 0:
      
      continue
      
    else:
      
      data_minima = min(lista_datas)

    estoque_inicial = df_aux.loc[
      (df_aux['DATA'] == data_minima)
    , 'QT_PESO_INICIAL'].item()

    volume_meta_diarizado = df_aux.loc[
      (df_aux['DATA'] == data_minima)
    , 'VOLUME_META_DIARIZADO'].item()

    projecao_estoque = estoque_inicial - volume_meta_diarizado

    estoque_minimo = df_aux.loc[
      (df_aux['DATA'] == data_minima)
    , 'ESTOQUE_MINIMO'].item()

    estoque_maximo = df_aux.loc[
      (df_aux['DATA'] == data_minima)
    , 'ESTOQUE_MAXIMO'].item()

    lista_datas.remove(data_minima)

    lista_datas.sort()

    for data in lista_datas:

      estoque_inicial = projecao_estoque

      volume_meta_diarizado = df_aux.loc[
        (df_aux['DATA'] == data)
      , 'VOLUME_META_DIARIZADO'].item()

      if estoque_inicial - volume_meta_diarizado <= estoque_minimo:

        reabastecimento = estoque_maximo - (estoque_inicial - volume_meta_diarizado)

      else:

        reabastecimento = 0

      projecao_estoque = estoque_inicial - volume_meta_diarizado + reabastecimento

      df_projecao_pandas_filial.loc[
        (df_projecao_pandas_filial['CD_FILIAL'] == cd_filial)
        &(df_projecao_pandas_filial['CD_ITEM'] == cd_item)
        &(df_projecao_pandas_filial['DATA'] == data)
      , 'QT_PESO_PROJETADO'] = projecao_estoque

      df_projecao_pandas_filial.loc[
        (df_projecao_pandas_filial['CD_FILIAL'] == cd_filial)
        &(df_projecao_pandas_filial['CD_ITEM'] == cd_item)
        &(df_projecao_pandas_filial['DATA'] == data)
      , 'QT_ABASTECIMENTO_SUGERIDA'] = reabastecimento

  carrega_tabela_bq('mrp-ia', 'estoque_ideal_produto_acabado', 'recomendacao_abastecimento', df_projecao_pandas_filial, modo = 'WRITE_APPEND')
