# Databricks notebook source
# !pip install unidecode
# !pip install bigquery
# !pip install pandas-gbq

# COMMAND ----------

import pandas as pd
import datetime
from datetime import timedelta
import unidecode
import pytz
import multiprocessing
from google.cloud import bigquery as bq

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.header import Header
import email

# import pydata_google_auth
# credentials = pydata_google_auth.get_user_credentials( ['https://www.googleapis.com/auth/bigquery'] )
# # %reload_ext google.cloud.bigquery
# from google.cloud.bigquery import magics
# magics.context.credentials = credentials
# from google.cloud import bigquery as bq
# client = bq.Client(project="seara-analytics-dev", credentials=credentials)

client = bq.Client()

tz = pytz.timezone('Brazil/East')

# COMMAND ----------

# função que para facilitar o carregamento de tabelas no bigquery

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.
  
#   df.to_gbq(
#     f'{nome_dataset}.{nome_tabela}',
#     project_id=id_projeto,
#     chunksize=None,
#     if_exists=modo
#   )
  
  return print(f'tabela {nome_tabela} carregada no local {id_projeto}.{nome_dataset}.{nome_tabela}')  #print(f"Loaded dataframe to {table_ref.path}")

# COMMAND ----------

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

trinta_dias_atras = hoje - datetime.timedelta(days=30)

data_ref = str(hoje)[:10]

# COMMAND ----------

query = f"""
WITH FILIAIS AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
    CD_EMPRESA,
    CD_FILIAL, 
    NM_FILIAL 
FROM 
    FILIAIS
WHERE
    CD_FILIAL IN (
        SELECT
            DISTINCT CD_FILIAL
        FROM
        `seara-analytics-prod.stg_erp_seara.scf_producao_filial`
        WHERE
            ID_SITUACAO = 1
        )
ORDER BY 
    CD_FILIAL
"""

df_filiais = client.query(query).to_dataframe()

lista_filiais = list(df_filiais['CD_FILIAL'].unique())

lista_filiais = lista_filiais + [405, 130, 120]

filiais = str(lista_filiais).replace('[', '').replace(']', '')

print('filiais: ', filiais)

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando e tratando as tabelas com as ordens de fornecimento, saldo dos estoques e a tabela item.')
print('-----------------------------------------------------------')

# COMMAND ----------

# #### OFS_ANDAMENTO_UNID

query = f"""

WITH A AS
(
   SELECT
    ORDEM_FORNECIMENTO_COMPRA.CD_EMPRESA,
    FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
    ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,
    FORNECEDOR.CD_BASE_FORNECEDOR,
    FORNECEDOR.CD_ESTAB_FORNECEDOR,
    FORNECEDOR.NM_RAZAO_SOCIAL,
    FORNECEDOR.NM_FANTASIA,
    ITEM_SOLICITACAO_COMPRA2.NUM_SEQ,
    ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ,
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA,
    ITEM_ORDEM_FORNECIMENTO_CMP.DT_ENTREGA,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE,
    ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM - ITEM_ORDEM_FORNECIMENTO_CMP.QT_ITEM_ENTREGUE AS SALDO_ITEM_OFO,
    --SNAC9029.BUSCA_SALDO_ITEM_OFO(ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO,ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ),
    --USUARIO8.CD_USUARIO,
    USUARIO8.NM_USUARIO,
    USUARIO38.NM_USUARIO AS DS_USUARIO,
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN,
    SITUACAO_ORDEM_FORNECIMENTO.DS_SITUACAO_ORDEM_FRN,
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM,
    ITEM.DS_ITEM,
    ORDEM_FORNECIMENTO_COMPRA.DT_ORDEM_FORNECIMENTO,
    RATEIO_ITEM_OFO_CMP.CD_CENTRO_CUSTO,
    ITEM.CD_GRUPO_ITEM,
    ITEM.CD_SUBGRUPO_ITEM,
    ITEM_ORDEM_FORNECIMENTO_CMP.VL_ITEM,
    COALESCE(ITEM_ORDEM_FORNECIMENTO_CMP.id_fechado, 'N') AS YN_ID_FECHADO,
    ITEM_ORDEM_FORNECIMENTO_CMP.ID_FECHADO
FROM
    `seara-analytics-prod.stg_erp_seara.item_ordem_fornecimento_cmp` AS ITEM_ORDEM_FORNECIMENTO_CMP

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_compra` AS ORDEM_FORNECIMENTO_COMPRA
ON
    ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO=ORDEM_FORNECIMENTO_COMPRA.CD_ORDEM_FORNECIMENTO

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO8
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_COMPRADOR=USUARIO8.CD_USUARIO

LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.situacao_ordem_fornecimento` AS SITUACAO_ORDEM_FORNECIMENTO
ON
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN=SITUACAO_ORDEM_FORNECIMENTO.CD_SITUACAO_ORDEM_FRN

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.rateio_item_ofo_cmp` AS RATEIO_ITEM_OFO_CMP
ON
    RATEIO_ITEM_OFO_CMP.CD_ORDEM_FORNECIMENTO=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ORDEM_FORNECIMENTO
    AND RATEIO_ITEM_OFO_CMP.NR_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NR_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item_solicitacao_compra` AS ITEM_SOLICITACAO_COMPRA2
ON
    ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_ORDEM_FORNECIMENTO_CMP.CD_SOLICITACAO_COMPRA
    AND ITEM_SOLICITACAO_COMPRA2.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM
    AND ITEM_SOLICITACAO_COMPRA2.NUM_SEQ=ITEM_ORDEM_FORNECIMENTO_CMP.NUM_SEQ

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.solicitacao_compra` AS SOLICITACAO_COMPRA2
ON
    SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA=ITEM_SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.usuario` AS USUARIO38
ON
    USUARIO38.CD_USUARIO=SOLICITACAO_COMPRA2.CD_USUARIO_SOLICITANTE

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.fornecedor` AS FORNECEDOR  
ON
    FORNECEDOR.CD_BASE_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_BASE_FORNECEDOR
    AND FORNECEDOR.CD_ESTAB_FORNECEDOR = ORDEM_FORNECIMENTO_COMPRA.CD_ESTAB_FORNECEDOR
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.filial_cgc` AS FILIAL
ON
    FILIAL.CD_FILIAL = ORDEM_FORNECIMENTO_COMPRA.CD_FILIAL
INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
ON
    ITEM.CD_ITEM=ITEM_ORDEM_FORNECIMENTO_CMP.CD_ITEM

WHERE
    ORDEM_FORNECIMENTO_COMPRA.CD_SITUACAO_ORDEM_FRN  NOT IN  (3, 4)
    AND  SOLICITACAO_COMPRA2.CD_SOLICITACAO_COMPRA  IS NOT NULL
)

SELECT
    *
FROM
    A
WHERE
    CD_FILIAL IN ({filiais})
    AND CD_EMPRESA IN (30,36)
    AND SALDO_ITEM_OFO > 0
    AND ID_FECHADO = 'N'
    AND CD_SITUACAO_ORDEM_FRN = 2
ORDER BY 
    CD_ORDEM_FORNECIMENTO                                      

"""

OFS_ANDAMENTO_UNID = client.query(query).to_dataframe()

# OFS_ANDAMENTO_UNID['FILTRO_NM_RAZAO_SOCIAL_CD_OF'] = OFS_ANDAMENTO_UNID['NM_RAZAO_SOCIAL'] + ' - ' + OFS_ANDAMENTO_UNID['CD_ORDEM_FORNECIMENTO'].astype(str)

OFS_ANDAMENTO_UNID.drop_duplicates(inplace=True)

# Arredondando a quantitade de item solicitado, entregue e o saldo para duas casas decimais
OFS_ANDAMENTO_UNID['QT_ITEM'] = [round(float(x), 3) for x in OFS_ANDAMENTO_UNID['QT_ITEM']]
OFS_ANDAMENTO_UNID['QT_ITEM_ENTREGUE'] = OFS_ANDAMENTO_UNID['QT_ITEM_ENTREGUE'].apply(
    lambda x: round(float(x), 3) if not pd.isnull(x) else x)
OFS_ANDAMENTO_UNID['SALDO_ITEM_OFO'] = [round(float(x), 3) for x in OFS_ANDAMENTO_UNID['SALDO_ITEM_OFO']]

OFS_ANDAMENTO_UNID['CD_ORDEM_FORNECIMENTO'] = OFS_ANDAMENTO_UNID['CD_ORDEM_FORNECIMENTO'].astype(str)

# A data de entrega não possui quebra por horas e está com o tipo string, não é necessário manter essa informação e precisamos mudar para o tipo date
OFS_ANDAMENTO_UNID['DT_ENTREGA'] = OFS_ANDAMENTO_UNID['DT_ENTREGA'].apply(
    lambda x: datetime.datetime.strptime(x, '%d/%m/%Y %H:%M:%S').date() if not pd.isnull(x) else x)
OFS_ANDAMENTO_UNID['DT_ORDEM_FORNECIMENTO'] = [datetime.datetime.strptime(x, '%d/%m/%Y %H:%M:%S') for x in
                                               OFS_ANDAMENTO_UNID['DT_ORDEM_FORNECIMENTO']]

OFS_ANDAMENTO_UNID = OFS_ANDAMENTO_UNID.groupby(
    by=['CD_FILIAL', 'CD_ITEM', 'DS_ITEM', 'DT_ENTREGA', 'NM_RAZAO_SOCIAL']).agg({
    'SALDO_ITEM_OFO': 'sum',
    'CD_ORDEM_FORNECIMENTO': ', '.join,
    'VL_ITEM': 'sum'
}).reset_index()

OFS_ANDAMENTO_UNID['VL_ITEM'] = OFS_ANDAMENTO_UNID['VL_ITEM'].apply(lambda x: float(x))

OFS_ANDAMENTO_UNID.rename(columns={'SALDO_ITEM_OFO': 'SUM_SALDO_ITEM_OFO'}, inplace=True)

# print(OFS_ANDAMENTO_UNID.shape)

# OFS_ANDAMENTO_UNID.head()

# COMMAND ----------

# #### SALDO_ESTOQUE_SUPRIMENTO

query = f"""

    WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
        )

    SELECT * FROM A WHERE A.rn = 1
    )

    SELECT 
        SALDO.CD_EMPRESA,
        FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
        SALDO.CD_LOCAL_ESTOQUE,
        SALDO.CD_ITEM,
        SALDO.QT_SALDO_ATUAL,
        SALDO.VL_CUSTO_UNITARIO_ATUAL
        /*CD_TIPO_CUSTO_SUPRIMENTO*/
    FROM `seara-analytics-prod.stg_erp_seara.saldo_estoque_suprimento` SALDO
    LEFT JOIN 
        FILIAL 
    ON
        FILIAL.CD_FILIAL = SALDO.CD_FILIAL
    WHERE FILIAL.CD_FILIAL_CENTRALIZADORA IN ({filiais})
"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

SALDO_ESTOQUE_SUPRIMENTO = query_job.to_dataframe()

# print(SALDO_ESTOQUE_SUPRIMENTO.shape)

# Arredondando a quantitade de item solicitado, entregue e o saldo para duas casas decimais
SALDO_ESTOQUE_SUPRIMENTO['QT_SALDO_ATUAL'] = SALDO_ESTOQUE_SUPRIMENTO['QT_SALDO_ATUAL'].apply(
    lambda x: round(float(x), 3) if not pd.isnull(x) else x)

SALDO_ESTOQUE_SUPRIMENTO['VL_CUSTO_UNITARIO_ATUAL'] = SALDO_ESTOQUE_SUPRIMENTO['VL_CUSTO_UNITARIO_ATUAL'].astype(float)

VALOR_UNITARIO_ESTOQUE = SALDO_ESTOQUE_SUPRIMENTO[
    ['CD_FILIAL', 'CD_LOCAL_ESTOQUE', 'CD_ITEM', 'QT_SALDO_ATUAL', 'VL_CUSTO_UNITARIO_ATUAL']].copy()

# VALOR_UNITARIO_ESTOQUE['VL_CUSTO_UNITARIO_PONDERADO'] = VALOR_UNITARIO_ESTOQUE['VL_CUSTO_UNITARIO_ATUAL'].astype(float)*VALOR_UNITARIO_ESTOQUE['QT_SALDO_ATUAL']

VALOR_UNITARIO_ESTOQUE = VALOR_UNITARIO_ESTOQUE.groupby(['CD_FILIAL', 'CD_ITEM', 'CD_LOCAL_ESTOQUE']).agg({
    'VL_CUSTO_UNITARIO_ATUAL': 'mean'
}).reset_index()

# VALOR_UNITARIO_ESTOQUE['VL_CUSTO_UNITARIO'] = VALOR_UNITARIO_ESTOQUE['VL_CUSTO_UNITARIO_PONDERADO']/VALOR_UNITARIO_ESTOQUE['QT_SALDO_ATUAL']

VALOR_UNITARIO_ESTOQUE = VALOR_UNITARIO_ESTOQUE[['CD_FILIAL', 'CD_ITEM', 'CD_LOCAL_ESTOQUE', 'VL_CUSTO_UNITARIO_ATUAL']]

SALDO_ESTOQUE_SUPRIMENTO.sort_values('CD_ITEM', inplace=True)

SALDO_ESTOQUE_SUPRIMENTO['DATA'] = hoje

# SALDO_ESTOQUE_SUPRIMENTO.head()

SALDO_ESTOQUE_SUPRIMENTO = SALDO_ESTOQUE_SUPRIMENTO.groupby(
    by=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'CD_LOCAL_ESTOQUE', 'DATA']).agg(
    ETQ_SALDO_ATUAL=pd.NamedAgg(column="QT_SALDO_ATUAL", aggfunc="sum")
).reset_index()

# print(SALDO_ESTOQUE_SUPRIMENTO.shape)

# SALDO_ESTOQUE_SUPRIMENTO.head()

# As filiais 126 e 136 não possuem estoque. Os estoques dessas filiais fica nas filiais contábeis 120 e 130, respectivamente.

# SALDO_ESTOQUE_SUPRIMENTO = SALDO_ESTOQUE_SUPRIMENTO[
#     ~(SALDO_ESTOQUE_SUPRIMENTO['CD_FILIAL'].isin([136,126]))
# ]

# SALDO_ESTOQUE_SUPRIMENTO['CD_FILIAL'].replace({120: 126, 130: 136}, inplace=True)

# COMMAND ----------

# #### ITEM

query = """
   SELECT CD_ITEM,
        DS_ITEM
   FROM `seara-analytics-prod.stg_erp_seara.item`
"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

ITEM = query_job.to_dataframe()

ITEM.dropna(subset=['CD_ITEM'], inplace=True)

# print(ITEM.shape)

# ITEM.head()

# COMMAND ----------

query = """
   SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO

FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO

"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

ITEM_FULL = query_job.to_dataframe()

ITEM_FULL.dropna(subset=['CD_ITEM'],inplace=True)

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando a tabela explosao_formula_produto.')
print('-----------------------------------------------------------')

# COMMAND ----------

# #### EXPLOSAO_FORMULA_PRODUTO

query = f"""

WITH 

ULTIMA_VIGENTE_CADA_FILIAL AS(
SELECT 
    CD_FILIAL,
    MAX(CD_CODIGO_EXPLOSAO) AS ULTIMO_CODIGO_EXPLOSAO_VIGENTE
FROM 
    `seara-analytics-prod.stg_erp_seara.explosao_formula_produto_log`
WHERE
    DS_FORMULA_UTILIZADA = 'VIGENTE'
    AND CD_FILIAL IS NOT NULL
GROUP BY 
    CD_FILIAL
),

EXPLOSAO_FORMULA_PRODUTO AS (
SELECT
    EXPLOSAO_FORMULA_PRODUTO.*,
    ULTIMA_VIGENTE_CADA_FILIAL.ULTIMO_CODIGO_EXPLOSAO_VIGENTE,
    (SELECT 
        MAX(CD_CODIGO_EXPLOSAO)
    FROM 
        `seara-analytics-prod.stg_erp_seara.explosao_formula_produto_log` 
    WHERE 
        CD_FILIAL IS NULL
        AND DS_FORMULA_UTILIZADA = 'VIGENTE') AS ULTIMA_VIGENTE_TODAS_FILIAIS
FROM 
    `seara-analytics-prod.stg_erp_seara.explosao_formula_produto` EXPLOSAO_FORMULA_PRODUTO
LEFT JOIN 
    ULTIMA_VIGENTE_CADA_FILIAL
ON 
    EXPLOSAO_FORMULA_PRODUTO.CD_FILIAL = ULTIMA_VIGENTE_CADA_FILIAL.CD_FILIAL
),

TABELA_COM_MARCACAO_DE_FORMULA_VIGENTE_EM_VIGOR AS(
SELECT 
    *,
    CASE 
        WHEN ULTIMA_VIGENTE_TODAS_FILIAIS >= COALESCE(ULTIMO_CODIGO_EXPLOSAO_VIGENTE,0) THEN ULTIMA_VIGENTE_TODAS_FILIAIS
        ELSE ULTIMO_CODIGO_EXPLOSAO_VIGENTE
        END AS FORMULA_VIGENTE_EM_VIGOR
FROM
    EXPLOSAO_FORMULA_PRODUTO
)

SELECT 
    *
FROM 
    TABELA_COM_MARCACAO_DE_FORMULA_VIGENTE_EM_VIGOR
WHERE
    CD_CODIGO_EXPLOSAO = FORMULA_VIGENTE_EM_VIGOR
    AND CD_FILIAL IN ({filiais})
    AND CD_EMPRESA IN (30,36)
ORDER BY 
    CD_CODIGO_EXPLOSAO DESC,
    CD_EMPRESA,
    CD_FILIAL,
    CD_ITEM,
    CD_ITEM_COMPONENTE

"""

EXPLOSAO_FORMULA_PRODUTO = client.query(query).to_dataframe()

# COMMAND ----------

servidor = 'correio.seara.com.br'
user = 'report.horas.extras@seara.com.br'
password = 'c*8SW9f&=g'

def send_mail_erro(to_mail):

    from_mail =  'report.horas.extras@seara.com.br' #'bruno.nascimento@seara.com.br'
    message = MIMEMultipart()
    message['From'] = Header("report.horas.extras@seara.com.br")
    message['To'] =  Header(to_mail, 'utf-8')
    subject = 'Código de explosao não encontrado na explosao_formula_produto'
    message['Subject'] = Header(subject, 'utf-8')

    message.attach(MIMEText( 'Esse é um email automático criado para avisar que existem códigos de explosao na explosao_formula_produto_log que não foram encontrados na explosao_formula_produto.', 'plain', 'utf-8'))


    debug = False
    if debug:
        print(msg.as_string())
    else:
        server = smtplib.SMTP(servidor,587)
        server.starttls()
        server.login(user, password)
        text = message.as_string()
        server.sendmail(from_mail, to_mail, text)
        server.quit()

# if EXPLOSAO_FORMULA_PRODUTO.shape[0] == 0: 
    
#     send_mail_erro('carlos.proto@seara.com.br')
    
#     send_mail_erro('yohan.consani@seara.com.br')

dias_anteriores = 1

while EXPLOSAO_FORMULA_PRODUTO.shape[0] == 0: 

  query = f"""

    WITH 

    ULTIMA_VIGENTE_CADA_FILIAL AS(
    SELECT 
        CD_FILIAL,
        MAX(CD_CODIGO_EXPLOSAO) AS ULTIMO_CODIGO_EXPLOSAO_VIGENTE
    FROM 
        `seara-analytics-prod.stg_erp_seara.explosao_formula_produto_log`
    WHERE
        DS_FORMULA_UTILIZADA = 'VIGENTE'
        AND CD_FILIAL IS NOT NULL
        AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_ATUALIZACAO) < '{hoje - timedelta(days = dias_anteriores)}'
    GROUP BY 
        CD_FILIAL
    ),

    EXPLOSAO_FORMULA_PRODUTO AS (
    SELECT
        EXPLOSAO_FORMULA_PRODUTO.*,
        ULTIMA_VIGENTE_CADA_FILIAL.ULTIMO_CODIGO_EXPLOSAO_VIGENTE,
        (SELECT 
            MAX(CD_CODIGO_EXPLOSAO)
        FROM 
            `seara-analytics-prod.stg_erp_seara.explosao_formula_produto_log` 
        WHERE 
            CD_FILIAL IS NULL
            AND DS_FORMULA_UTILIZADA = 'VIGENTE'
            AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_ATUALIZACAO) < '{hoje - timedelta(days = dias_anteriores)}'
        ) AS ULTIMA_VIGENTE_TODAS_FILIAIS
    FROM 
        `seara-analytics-prod.stg_erp_seara.explosao_formula_produto` EXPLOSAO_FORMULA_PRODUTO
    LEFT JOIN 
        ULTIMA_VIGENTE_CADA_FILIAL
    ON 
        EXPLOSAO_FORMULA_PRODUTO.CD_FILIAL = ULTIMA_VIGENTE_CADA_FILIAL.CD_FILIAL
    ),

    TABELA_COM_MARCACAO_DE_FORMULA_VIGENTE_EM_VIGOR AS(
    SELECT 
        *,
        CASE 
            WHEN ULTIMA_VIGENTE_TODAS_FILIAIS >= COALESCE(ULTIMO_CODIGO_EXPLOSAO_VIGENTE,0) THEN ULTIMA_VIGENTE_TODAS_FILIAIS
            ELSE ULTIMO_CODIGO_EXPLOSAO_VIGENTE
            END AS FORMULA_VIGENTE_EM_VIGOR
    FROM
        EXPLOSAO_FORMULA_PRODUTO
    )

    SELECT 
        *
    FROM 
        TABELA_COM_MARCACAO_DE_FORMULA_VIGENTE_EM_VIGOR
    WHERE
        CD_CODIGO_EXPLOSAO = FORMULA_VIGENTE_EM_VIGOR
        AND CD_FILIAL IN ({filiais})
        AND CD_EMPRESA IN (30,36)
    ORDER BY 
        CD_CODIGO_EXPLOSAO DESC,
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        CD_ITEM_COMPONENTE
  """
  
  EXPLOSAO_FORMULA_PRODUTO = client.query(query).to_dataframe()
  
  dias_anteriores += 1

# COMMAND ----------

EXPLOSAO_FORMULA_PRODUTO.drop_duplicates(inplace=True)

EXPLOSAO_FORMULA_PRODUTO['DT_ATUALIZACAO'] = pd.to_datetime(EXPLOSAO_FORMULA_PRODUTO['DT_ATUALIZACAO'],
                                                            format='%d/%m/%Y %H:%M:%S', errors='coerce')

EXPLOSAO_FORMULA_PRODUTO['DS_TIPO_REGISTRO'] = EXPLOSAO_FORMULA_PRODUTO['DS_TIPO_REGISTRO'].apply(
    lambda x: 'Matéria Prima' if x == 'Matéria-Prima' else x)

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO.merge(ITEM, how='left', left_on='CD_ITEM', right_on='CD_ITEM')

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO.sort_values(by=['CD_ITEM', 'CD_ITEM_COMPONENTE', 'DT_ATUALIZACAO'],
                                                                ascending=False)

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO.merge(ITEM, how='left', left_on='CD_ITEM_COMPONENTE',
                                                          right_on='CD_ITEM', suffixes=("", "_COMPONENTE"))

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO.loc[:, ~EXPLOSAO_FORMULA_PRODUTO.columns.duplicated()]

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO[
    ['CD_EMPRESA', 'CD_FILIAL', 'CD_CODIGO_EXPLOSAO', 'CD_FORMULA_PRODUTO_CNE', 'CD_FORMULA_PRODUTO_CNE_COMP',
     'DS_ARVORE', 'CD_ITEM', 'DS_ITEM', 'CD_ITEM_COMPONENTE', 'DS_ITEM_COMPONENTE',
     'DS_TIPO_REGISTRO', 'DS_FORMULA_UTILIZADA',
     'CONSUMO_NECESSARIO', 'DT_ATUALIZACAO']]

# COMMAND ----------

# Removendo manualemnte a estrutura de embalagem das farinhas

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO[
  ~(
    (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 822)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'].isin([226339, 795305, 954044, 984038]))
    &(EXPLOSAO_FORMULA_PRODUTO['DS_TIPO_REGISTRO'] == 'Embalagens')
  )
]

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Adicionando os tempos de cura corretos na tabela explosao_formula_produto.')
print('-----------------------------------------------------------')

# COMMAND ----------

# #### Tempo de cura

query = f"""

SELECT
    *
FROM
    `seara-analytics-prod.stg_erp_seara.tempo_cura_produto`     
"""

TEMPO_CURA = client.query(query).to_dataframe()

TEMPO_CURA.rename(columns={'CD_ITEM': 'CD_ITEM_COMPONENTE', 'QT_TEMPO': 'QT_TEMPO_CURA'}, inplace=True)

# print(TEMPO_CURA.shape)

# TEMPO_CURA.head()

TEMPO_CURA = TEMPO_CURA[[
    'CD_EMPRESA',
    'CD_FILIAL',
    'CD_ITEM_COMPONENTE',
    'QT_TEMPO_CURA'
]]

# COMMAND ----------

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO.merge(TEMPO_CURA, how='left',
                                                          on=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM_COMPONENTE'])

EXPLOSAO_FORMULA_PRODUTO['QT_TEMPO_CURA'].fillna(0, inplace=True)

# EXPLOSAO_FORMULA_PRODUTO_painel = EXPLOSAO_FORMULA_PRODUTO.copy()

lista_itens_compostos = list(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'].unique())
lista_itens_componentes = list(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'].unique())

EXPLOSAO_FORMULA_PRODUTO['ITEM COMPONENTE É ITEM COMPOSTO'] = 'Não'

for filial in EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'].unique():
    lista_itens_compostos_filial = EXPLOSAO_FORMULA_PRODUTO.loc[
        (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial), 'CD_ITEM'].unique()

    EXPLOSAO_FORMULA_PRODUTO.loc[
        (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial)
        & (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'].isin(
            lista_itens_compostos_filial
        )
        ), 'ITEM COMPONENTE É ITEM COMPOSTO'] = 'Sim'

# COMMAND ----------

# Codigo comentado abaixo estava sendo desenvolvido para tratar os casos excepcionais que apareciam quando tentamos desfazer a explosao de consumo.

comentario = """

EXPLOSAO_FORMULA_PRODUTO['PROXIMO_ITEM_HIERARQUIA'] = EXPLOSAO_FORMULA_PRODUTO['DS_ARVORE'].apply(lambda x: int(x.split('/')[-1]) if not pd.isnull(x) else x)

EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == EXPLOSAO_FORMULA_PRODUTO['PROXIMO_ITEM_HIERARQUIA'])
    |(EXPLOSAO_FORMULA_PRODUTO['PROXIMO_ITEM_HIERARQUIA'].isna())
]

### Validações

#Exemplo de item componente repetido na explosao de um mesmo item composto
EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 861372)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 451970)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 228)
].sort_values(by=['CD_ITEM_COMPONENTE'])

#Lista técnica de outro exemplo
EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 846678) #846678 928410
    #&(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO']].sort_values(by=['CD_ITEM_COMPONENTE'])

#Investigando o exemplo acima
EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 846678) #846678 928410
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO','CD_FORMULA_PRODUTO_CNE_COMP']].sort_values(by=['CD_ITEM_COMPONENTE'])

#Verificando se os conumos acima correspondem a um item componente que também é composto
EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 928410) #846678 928410
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO']].sort_values(by=['CD_ITEM_COMPONENTE'])

EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 928429) #846678 928410
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO']].sort_values(by=['CD_ITEM_COMPONENTE'])

EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 928437) #846678 928410
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO']].sort_values(by=['CD_ITEM_COMPONENTE'])

# A estrutura de embalagens é repetida para alguns itens. É necessário remover embalagens repetidas.

df_verificar = EXPLOSAO_FORMULA_PRODUTO.groupby(['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'CD_ITEM_COMPONENTE']).agg(
    contagem = pd.NamedAgg(column = 'CD_ITEM_COMPONENTE', aggfunc = 'count')
).reset_index().sort_values(by=['contagem', 'CD_FILIAL', 'CD_ITEM'], ascending = False)

df_verificar = df_verificar[df_verificar['contagem'] > 1]

lista_combinacoes_a_verificar = list(zip(df_verificar['CD_EMPRESA'], df_verificar['CD_FILIAL'], df_verificar['CD_ITEM'], df_verificar['CD_ITEM_COMPONENTE']))

#Lógica para remover esses casos. Precisa desenvolver completamente!
for cd_empresa, cd_filial, cd_item, cd_item_componente in [lista_combinacoes_a_verificar[0]]:

    itens_componentes_e_compostos = EXPLOSAO_FORMULA_PRODUTO.loc[
        (EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == cd_empresa)
        &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == cd_filial)
        &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == cd_item)
        &(EXPLOSAO_FORMULA_PRODUTO['ITEM COMPONENTE É ITEM COMPOSTO'] == 'Sim')
    , 'CD_ITEM_COMPONENTE'].unique()

    for item_componente in [itens_componentes_e_compostos[0]]:

        df_consumo_necessario_item_componente_em_item_intermediario = EXPLOSAO_FORMULA_PRODUTO.loc[
            (EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == cd_empresa)
            &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == cd_filial)
            &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == item_componente)
            &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == cd_item_componente)
        ]

        if df_consumo_necessario_item_componente_em_item_intermediario.shape[0] > 0:

            consumo_necessario = df_consumo_necessario_item_componente_em_item_intermediario['CONSUMO_NECESSARIO'].max()

            # Obtem uma linha com o consumo igual. Elimina essa linha da explosao_formula_produto, pois diz respeito à explosao de consumo de um item componente

            indice_a_remover = EXPLOSAO_FORMULA_PRODUTO[
                (EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == cd_empresa)
                &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == cd_filial)
                &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == cd_item)
                &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == cd_item_componente)
                &(EXPLOSAO_FORMULA_PRODUTO['CONSUMO_NECESSARIO'] == consumo_necessario)
            ].index[0]

            EXPLOSAO_FORMULA_PRODUTO.drop(index= indice_a_remover, inplace = True)

#Verifica se funcionou. 1 linha deve ter sido removida.
EXPLOSAO_FORMULA_PRODUTO[
    (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == 846678) #846678 928410
    &(EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == 377533)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == 907)
    &(EXPLOSAO_FORMULA_PRODUTO['CD_EMPRESA'] == 30)
][['CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_ITEM_COMPONENTE','CONSUMO_NECESSARIO','CD_FORMULA_PRODUTO_CNE_COMP']].sort_values(by=['CD_ITEM_COMPONENTE'])

"""

# COMMAND ----------

# Adicionando os tempos de cura nos itens componentes de itens componentes com tempo de cura

for filial in EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'].unique():

    for sku in EXPLOSAO_FORMULA_PRODUTO.loc[(EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial) & (
            EXPLOSAO_FORMULA_PRODUTO['ITEM COMPONENTE É ITEM COMPOSTO'] == 'Sim'), 'CD_ITEM'].unique():

        aux = EXPLOSAO_FORMULA_PRODUTO[
            (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial) & (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == sku)].copy()

        EXPLOSAO_FORMULA_PRODUTO = EXPLOSAO_FORMULA_PRODUTO[
            ~((EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial) & (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM'] == sku))]

        for item in aux.loc[(aux['ITEM COMPONENTE É ITEM COMPOSTO'] == 'Sim') & (
                aux['QT_TEMPO_CURA'] > 0), 'CD_ITEM_COMPONENTE'].unique():

            itens_devido_item_componente_ser_composto = list(EXPLOSAO_FORMULA_PRODUTO.loc[
                                                                 (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial) & (
                                                                             EXPLOSAO_FORMULA_PRODUTO[
                                                                                 'CD_ITEM'] == item), 'CD_ITEM_COMPONENTE'])

            for item_componente in itens_devido_item_componente_ser_composto:

                if aux[aux['CD_ITEM_COMPONENTE'] == item_componente].shape[0] > 0:

                    tempo_cura = aux.loc[aux['CD_ITEM_COMPONENTE'] == item, 'QT_TEMPO_CURA'].item()

                    for indice in aux[aux['CD_ITEM_COMPONENTE'] == item_componente].index:

                        if pd.isnull(aux.loc[indice, 'DS_ARVORE']):

                            aux.loc[indice, 'QT_TEMPO_CURA'] = tempo_cura

                        elif str(item) in aux.loc[indice, 'DS_ARVORE']:

                            aux.loc[indice, 'QT_TEMPO_CURA'] = tempo_cura

        EXPLOSAO_FORMULA_PRODUTO = pd.concat([EXPLOSAO_FORMULA_PRODUTO, aux], ignore_index=True)

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando e tratando a tabela do plano de produção.')
print('-----------------------------------------------------------')

# COMMAND ----------

query = """
   SELECT DISTINCT CD_ITEM
   FROM `seara-analytics-prod.stg_erp_seara.item`
   WHERE CD_GRUPO_ITEM = 3 AND CD_SUBGRUPO_ITEM IN (4,11,35,63)
"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

ITENS_RETIRAR_PLANO = query_job.to_dataframe()

ITENS_RETIRAR_PLANO.dropna(inplace=True)

itens_retirar_plano = list(ITENS_RETIRAR_PLANO['CD_ITEM'])

# COMMAND ----------

query = f"""
    SELECT 
        *
    FROM 
        `seara-analytics-prod.stg_erp_seara.plano_diario_pdc_item_cne`
    WHERE 
        CD_FILIAL IN ({filiais})
        AND CD_EMPRESA IN (30,36)
        AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_PRODUCAO) >= '{str(hoje)}'
"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

PLANO_DIARIO_PDC_ITEM_CNE = query_job.to_dataframe()

PLANO_DIARIO_PDC_ITEM_CNE['DT_TRANSMISSAO'] = pd.to_datetime(PLANO_DIARIO_PDC_ITEM_CNE['DT_TRANSMISSAO'],
                                                             format='%d/%m/%Y %H:%M:%S', errors='coerce').dt.date

PLANO_DIARIO_PDC_ITEM_CNE['DT_PRODUCAO'] = pd.to_datetime(PLANO_DIARIO_PDC_ITEM_CNE['DT_PRODUCAO'],
                                                          format='%d/%m/%Y %H:%M:%S', errors='coerce').dt.date

PLANO_DIARIO_PDC_ITEM_CNE.loc[pd.isnull(PLANO_DIARIO_PDC_ITEM_CNE['QT_PESO_ALR']), 'QT_PESO_ALR'] = \
PLANO_DIARIO_PDC_ITEM_CNE.loc[pd.isnull(PLANO_DIARIO_PDC_ITEM_CNE['QT_PESO_ALR']), 'QT_PESO']

# PLANO_DIARIO_PDC_ITEM_CNE = PLANO_DIARIO_PDC_ITEM_CNE[PLANO_DIARIO_PDC_ITEM_CNE['DT_PRODUCAO'] >= hoje]

PLANO_DIARIO_PDC_ITEM_CNE = PLANO_DIARIO_PDC_ITEM_CNE[
    ['CD_EMPRESA', 'CD_FILIAL', 'ID_USUARIO', 'CD_ITEM', 'QT_PESO_ALR', 'DT_PRODUCAO']]

PLANO_DIARIO_PDC_ITEM_CNE = PLANO_DIARIO_PDC_ITEM_CNE[PLANO_DIARIO_PDC_ITEM_CNE['QT_PESO_ALR'] > 0]

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Adicionando e diarizando plano mensal de producao.')
print('-----------------------------------------------------------')

# COMMAND ----------

query = f"""
SELECT
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  QT_PRD_ALTERADA,
  ID_SITUACAO,
  PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_MES_ANO_PRODUCAO) AS DT_MES_ANO_PRODUCAO,
  --DT_ATUALIZACAO
FROM
  `seara-analytics-prod.stg_erp_seara.plano_mensal_pdc_item_cne`
WHERE 
    PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_MES_ANO_PRODUCAO) >= '{str(hoje)}'
    AND CD_FILIAL IN ({filiais})
    AND CD_EMPRESA IN (30,36)
ORDER BY 
  CD_FILIAL,
  CD_ITEM,
  DT_MES_ANO_PRODUCAO

"""

query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

PLANO_MENSAL_PDC_ITEM_CNE = query_job.to_dataframe()

PLANO_MENSAL_PDC_ITEM_CNE['DT_MES_ANO_PRODUCAO'] = pd.to_datetime(PLANO_MENSAL_PDC_ITEM_CNE['DT_MES_ANO_PRODUCAO'],
                                                                  format='%d/%m/%Y %H:%M:%S', errors='coerce').dt.date

# COMMAND ----------

for filial in PLANO_MENSAL_PDC_ITEM_CNE['CD_FILIAL'].unique():
    lista_itens_componentes_filial = EXPLOSAO_FORMULA_PRODUTO.loc[
        EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial, 'CD_ITEM_COMPONENTE'].unique()

    PLANO_MENSAL_PDC_ITEM_CNE.drop(
        PLANO_MENSAL_PDC_ITEM_CNE[
            (PLANO_MENSAL_PDC_ITEM_CNE['CD_FILIAL'] == filial)
            & (PLANO_MENSAL_PDC_ITEM_CNE['CD_ITEM'].isin(lista_itens_componentes_filial))
            & (PLANO_MENSAL_PDC_ITEM_CNE['CD_ITEM'].isin(itens_retirar_plano))
            ].index
        , inplace=True)

meses_no_plano_producao_mensal = PLANO_MENSAL_PDC_ITEM_CNE['DT_MES_ANO_PRODUCAO'].apply(lambda x: x.month).unique()

anos_no_plano_producao_mensal = PLANO_MENSAL_PDC_ITEM_CNE['DT_MES_ANO_PRODUCAO'].apply(lambda x: x.year).unique()

df_calendario = pd.DataFrame(columns=['MES_REF', 'DATA', 'DIA_DA_SEMANA'])

for ano in anos_no_plano_producao_mensal:

    for mes in meses_no_plano_producao_mensal:

        data_inicial = datetime.date(ano, mes, 1)

        if mes == 12:

            data_final = datetime.date(ano, 12, 31)
        else:

            data_final = datetime.date(ano, mes + 1, 1) - datetime.timedelta(days=1)

        data = data_inicial

        while data <= data_final:
            df_calendario.loc[len(df_calendario)] = [str(mes)+str(ano), data, data.weekday()]

            data += datetime.timedelta(days=1)

df_calendario = df_calendario[df_calendario['DIA_DA_SEMANA'].isin([0, 1, 2, 3, 4])]

df_calendario_agg = df_calendario.groupby(['MES_REF']).agg({'DIA_DA_SEMANA': 'count'}).reset_index()

df_calendario_agg.rename(columns={'DIA_DA_SEMANA': 'QT_DIAS_UTEIS_MES'}, inplace=True)

PLANO_MENSAL_PDC_ITEM_CNE['MES_REF'] = PLANO_MENSAL_PDC_ITEM_CNE['DT_MES_ANO_PRODUCAO'].apply(lambda x: str(x.month)+str(x.year))

PLANO_MENSAL_PDC_ITEM_CNE = PLANO_MENSAL_PDC_ITEM_CNE.groupby(
    ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'ID_SITUACAO', 'MES_REF']).agg({'QT_PRD_ALTERADA': 'sum'}).reset_index()

PLANO_MENSAL_PDC_ITEM_CNE = PLANO_MENSAL_PDC_ITEM_CNE.merge(df_calendario_agg, how='left', on=['MES_REF'])

PLANO_MENSAL_PDC_ITEM_CNE['QT_PRD_ALTERADA_DIARIZADA'] = PLANO_MENSAL_PDC_ITEM_CNE['QT_PRD_ALTERADA'] / \
                                                         PLANO_MENSAL_PDC_ITEM_CNE['QT_DIAS_UTEIS_MES']

PLANO_MENSAL_PDC_ITEM_CNE = PLANO_MENSAL_PDC_ITEM_CNE.merge(df_calendario[['MES_REF', 'DATA']], how='left',
                                                            on=['MES_REF'])

PLANO_MENSAL_PDC_ITEM_CNE.drop(columns=['MES_REF', 'QT_DIAS_UTEIS_MES', 'QT_PRD_ALTERADA'], inplace=True)

PLANO_MENSAL_PDC_ITEM_CNE.rename(columns={
    'QT_PRD_ALTERADA_DIARIZADA': 'QT_PRD_ALTERADA',
    'DATA': 'DT_MES_ANO_PRODUCAO'
}, inplace=True)

PLANO_MENSAL_PDC_ITEM_CNE = PLANO_MENSAL_PDC_ITEM_CNE[
    ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'QT_PRD_ALTERADA', 'ID_SITUACAO', 'DT_MES_ANO_PRODUCAO']]

# COMMAND ----------

# Encontra as maiores datas, para cada filial e item, no plano de produção diário

PLANO_DIARIO_PDC_ITEM_CNE_MAX_DATES = PLANO_DIARIO_PDC_ITEM_CNE.groupby(['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM']).agg(
    {'DT_PRODUCAO': 'max'}).reset_index()

PLANO_DIARIO_PDC_ITEM_CNE_MAX_DATES.rename(columns={'DT_PRODUCAO': 'MAX_DT_PRODUCAO'}, inplace=True)

def ultimo_dia_do_mes(data):
    if data.month == 12:

        return datetime.date(data.year, 12, 31)

    else:

        inicio_mes_seguinte = datetime.date(data.year, data.month + 1, 1)

        final_do_mes = inicio_mes_seguinte - datetime.timedelta(days=1)

        return final_do_mes

PLANO_DIARIO_PDC_ITEM_CNE_MAX_DATES['MAX_DT_PRODUCAO'] = PLANO_DIARIO_PDC_ITEM_CNE_MAX_DATES['MAX_DT_PRODUCAO'].apply(
    lambda x: ultimo_dia_do_mes(x))

PLANO_MENSAL_PDC_ITEM_CNE_aux = PLANO_MENSAL_PDC_ITEM_CNE.merge(PLANO_DIARIO_PDC_ITEM_CNE_MAX_DATES, how='left',
                                                                on=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM'])
PLANO_MENSAL_PDC_ITEM_CNE_aux = PLANO_MENSAL_PDC_ITEM_CNE_aux[
    PLANO_MENSAL_PDC_ITEM_CNE_aux['DT_MES_ANO_PRODUCAO'] > PLANO_MENSAL_PDC_ITEM_CNE_aux['MAX_DT_PRODUCAO']]

PLANO_MENSAL_PDC_ITEM_CNE_aux.rename(columns={
    'QT_PRD_ALTERADA': 'QT_PESO_ALR',
    'DT_MES_ANO_PRODUCAO': 'DT_PRODUCAO'
}, inplace=True)

PLANO_MENSAL_PDC_ITEM_CNE_aux.drop(columns=['MAX_DT_PRODUCAO'], inplace=True)

PLANO_MENSAL_PDC_ITEM_CNE_aux['ID_USUARIO'] = 'PLANO_MENSAL'

PLANO_DIARIO_PDC_ITEM_CNE = pd.concat([PLANO_DIARIO_PDC_ITEM_CNE, PLANO_MENSAL_PDC_ITEM_CNE_aux], ignore_index=True)


# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando uma tabela de exceções (itens componentes no plano de produção que não devem ser removidos).')
print('-----------------------------------------------------------')

# COMMAND ----------

def verifica_excecao(indice):
    dt_producao = PLANO_DIARIO_PDC_ITEM_CNE.loc[indice, 'DT_PRODUCAO']

    filial = PLANO_DIARIO_PDC_ITEM_CNE.loc[indice, 'CD_FILIAL']

    item = PLANO_DIARIO_PDC_ITEM_CNE.loc[indice, 'CD_ITEM']

    lista_itens_componentes_filial = EXPLOSAO_FORMULA_PRODUTO.loc[
        EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial, 'CD_ITEM_COMPONENTE'].unique()

    if item in lista_itens_componentes_filial:

        itens_compostos_formulas = list(EXPLOSAO_FORMULA_PRODUTO.loc[
                                            (EXPLOSAO_FORMULA_PRODUTO['CD_ITEM_COMPONENTE'] == item)
                                            & (EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial)
                                            , 'CD_ITEM'])

        itens_compostos_plano = list(PLANO_DIARIO_PDC_ITEM_CNE.loc[
                                         (PLANO_DIARIO_PDC_ITEM_CNE['CD_FILIAL'] == filial)
                                         & (PLANO_DIARIO_PDC_ITEM_CNE['DT_PRODUCAO'] == dt_producao),
                                         'CD_ITEM'
                                     ])

        # Se o item componente possui o item composto no plano de produção, no mesmo dia, quer dizer que não é uma exceção

        if len(set(itens_compostos_formulas).intersection(set(itens_compostos_plano))) > 0:

            return

        else:

            return (PLANO_DIARIO_PDC_ITEM_CNE.loc[indice])

    else:

        return


#### Exceções: itens componentes e compostos, mas que não há plano de produção do sku final

a_pool = multiprocessing.Pool()

result = a_pool.map(verifica_excecao, PLANO_DIARIO_PDC_ITEM_CNE.index)

result = list(filter(None.__ne__, result))

PLANO_DIARIO_PDC_ITEM_CNE_EXCECOES = pd.DataFrame(result)

if PLANO_DIARIO_PDC_ITEM_CNE_EXCECOES.shape[0] > 0:
    PLANO_DIARIO_PDC_ITEM_CNE_EXCECOES = PLANO_DIARIO_PDC_ITEM_CNE_EXCECOES[
        ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DT_PRODUCAO', 'ID_USUARIO', 'QT_PESO_ALR']]

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Removendo itens componentes do plano de produção.')
print('-----------------------------------------------------------')

# COMMAND ----------

for filial in PLANO_DIARIO_PDC_ITEM_CNE['CD_FILIAL'].unique():
    lista_itens_componentes_filial = EXPLOSAO_FORMULA_PRODUTO.loc[
        EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial, 'CD_ITEM_COMPONENTE'].unique()

    PLANO_DIARIO_PDC_ITEM_CNE.drop(
        PLANO_DIARIO_PDC_ITEM_CNE[
            (PLANO_DIARIO_PDC_ITEM_CNE['CD_FILIAL'] == filial)
            & (PLANO_DIARIO_PDC_ITEM_CNE['CD_ITEM'].isin(lista_itens_componentes_filial))
            & (PLANO_DIARIO_PDC_ITEM_CNE['CD_ITEM'].isin(itens_retirar_plano))
            ].index
        , inplace=True)

PLANO_DIARIO_PDC_ITEM_CNE = pd.concat([PLANO_DIARIO_PDC_ITEM_CNE, PLANO_DIARIO_PDC_ITEM_CNE_EXCECOES],
                                      ignore_index=True)

PLANO_DIARIO_PDC_ITEM_CNE.sort_values(by=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DT_PRODUCAO'], inplace=True)

# COMMAND ----------

# #### CALENDARIO_PRODUCAO_CNE

query = f"""
    SELECT * 
    FROM `seara-analytics-prod.stg_erp_seara.calendario_producao_cne`
    WHERE CD_FILIAL IN ({filiais})
"""
query_job = client.query(
    query,
    # Location must match that of the dataset(s) referenced in the query.
    # location="US",
)  # API request - starts the query

CALENDARIO_PRODUCAO_CNE = query_job.to_dataframe()

CALENDARIO_PRODUCAO_CNE['DT_PRODUCAO'] = pd.to_datetime(CALENDARIO_PRODUCAO_CNE['DT_PRODUCAO'],
                                                        format='%d/%m/%Y %H:%M:%S', errors='coerce').dt.date

CALENDARIO_PRODUCAO_CNE = CALENDARIO_PRODUCAO_CNE[
    CALENDARIO_PRODUCAO_CNE['DT_PRODUCAO'] >= hoje - datetime.timedelta(60)]

CALENDARIO_PRODUCAO_CNE.sort_values(by=['CD_EMPRESA', 'CD_FILIAL', 'CD_PLANTA_FABRIL', 'DT_PRODUCAO'], inplace=True)

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando a tabela integrada.')
print('-----------------------------------------------------------')

# COMMAND ----------

# #### Criação da tabela integrada

df = PLANO_DIARIO_PDC_ITEM_CNE.merge(EXPLOSAO_FORMULA_PRODUTO, how='left',
                                     left_on=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM'],
                                     right_on=['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM'])

df = df.rename(columns={'CONSUMO_NECESSARIO': 'CONSUMO_POR_KG'})

df['CONSUMO TOTAL'] = df['CONSUMO_POR_KG'] * df['QT_PESO_ALR']

df['QT_TEMPO_CURA'].fillna(0, inplace=True)

df['DT_CONSUMO'] = df['DT_PRODUCAO'] - df['QT_TEMPO_CURA'].apply(
    lambda x: timedelta(days=x) if x > 0 else timedelta(days=0.0))

# COMMAND ----------

PLANO_DIARIO_PDC_ITEM_CNE = PLANO_DIARIO_PDC_ITEM_CNE.groupby(
    ['CD_EMPRESA', 'CD_FILIAL', 'ID_USUARIO', 'CD_ITEM', 'DT_PRODUCAO']).agg({'QT_PESO_ALR': 'sum'}).reset_index()

PLANO_DIARIO_PDC_ITEM_CNE.sort_values(by=['CD_EMPRESA', 'CD_FILIAL', 'DT_PRODUCAO'], ascending=True, inplace=True)

# COMMAND ----------

# ### Tabela MRP

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Criando a tabela MRP.')
print('-----------------------------------------------------------')

df_plano_diario = df.groupby(
    ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DS_ITEM', 'CD_ITEM_COMPONENTE', 'DS_ITEM_COMPONENTE', 'DS_TIPO_REGISTRO',
     'DT_CONSUMO']).agg(
    CONSUMO_PLANEJADO=pd.NamedAgg(column='CONSUMO TOTAL', aggfunc='sum'),
).reset_index()

df_plano_diario['DT_CONSUMO'] = pd.to_datetime(df_plano_diario['DT_CONSUMO']).dt.date

df_plano_diario['CONSUMO_PLANEJADO'] = round(df_plano_diario['CONSUMO_PLANEJADO'], 3)

df_sistema = df_plano_diario  # .merge(OFS_ANDAMENTO_UNID[['CD_FILIAL','CD_ITEM','DT_ENTREGA','CD_ORDEM_FORNECIMENTO','SUM_SALDO_ITEM_OFO']], how = 'outer', left_on = ['CD_FILIAL','CD_ITEM_COMPONENTE','DT_CONSUMO'], right_on = ['CD_FILIAL','CD_ITEM','DT_ENTREGA'], suffixes=('','_delete'))

df_sistema.rename(columns={'DT_CONSUMO': 'DATA'}, inplace=True)

df_sistema = df_sistema[
    ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DS_ITEM', 'CD_ITEM_COMPONENTE', 'DS_ITEM_COMPONENTE', 'DS_TIPO_REGISTRO',
     'DATA', 'CONSUMO_PLANEJADO',
     ]].sort_values(by=['CD_FILIAL', 'CD_ITEM_COMPONENTE', 'DATA'], ascending=True).reset_index(drop=True)

df_sistema.loc[~df_sistema['CD_ITEM'].isna(), 'CD_ITEM'] = df_sistema.loc[
    ~df_sistema['CD_ITEM'].isna(), 'CD_ITEM'].astype(int)
df_sistema.loc[~df_sistema['CD_ITEM_COMPONENTE'].isna(), 'CD_ITEM_COMPONENTE'] = df_sistema.loc[
    ~df_sistema['CD_ITEM_COMPONENTE'].isna(), 'CD_ITEM_COMPONENTE'].astype(int)

# Mantém somente os itens consumidos a partir de hoje
df_sistema_final = df_sistema[(df_sistema['DATA'] >= hoje - datetime.timedelta(30))].copy()

# COMMAND ----------

SALDO_ESTOQUE_SUPRIMENTO = SALDO_ESTOQUE_SUPRIMENTO.merge(VALOR_UNITARIO_ESTOQUE, how='left',
                                                          on=['CD_FILIAL', 'CD_ITEM', 'CD_LOCAL_ESTOQUE'])

# #### Alertas

df_alertas = df_sistema_final[
    ['CD_FILIAL', 'CD_ITEM_COMPONENTE', 'DS_TIPO_REGISTRO', 'DATA', 'CONSUMO_PLANEJADO']].copy()

df_alertas.rename(columns={
    'CD_ITEM_COMPONENTE': 'CD_ITEM'
}, inplace=True)

df_alertas = df_alertas.groupby(['CD_FILIAL', 'CD_ITEM', 'DS_TIPO_REGISTRO', 'DATA']).sum().reset_index()

df_alertas = df_alertas.merge(
    OFS_ANDAMENTO_UNID.loc[
        OFS_ANDAMENTO_UNID['DT_ENTREGA'] >= hoje,
       ['CD_FILIAL', 'CD_ITEM', 'DT_ENTREGA', 'SUM_SALDO_ITEM_OFO']],
    how='outer',
    left_on=['CD_FILIAL', 'CD_ITEM', 'DATA'],
    right_on=['CD_FILIAL', 'CD_ITEM', 'DT_ENTREGA'],
)

df_alertas.loc[df_alertas['DATA'].isna(), 'DATA'] = df_alertas.loc[df_alertas['DATA'].isna(), 'DT_ENTREGA']

df_alertas.drop(columns=['DT_ENTREGA', 'DS_TIPO_REGISTRO'], inplace=True)

df_alertas['SUM_SALDO_ITEM_OFO'].fillna(0, inplace=True)

df_alertas['CONSUMO_PLANEJADO'].fillna(0, inplace=True)

formulas_aux = EXPLOSAO_FORMULA_PRODUTO[['CD_ITEM_COMPONENTE', 'DS_TIPO_REGISTRO']].copy().drop_duplicates()

formulas_aux.rename(columns={'CD_ITEM_COMPONENTE': 'CD_ITEM'}, inplace=True)

df_alertas = df_alertas.merge(formulas_aux, how='left', on=['CD_ITEM'])

df_alertas.sort_values(by=['DATA'], inplace=True)

df_alertas_aux = df_alertas.groupby(['CD_FILIAL', 'CD_ITEM', 'DATA']).sum().groupby(level=[0, 1]).cumsum().reset_index()

df_alertas_aux.rename(columns={
    'CONSUMO_PLANEJADO': 'CONSUMO_PLANEJADO_ACUMULADO',
    'SUM_SALDO_ITEM_OFO': 'SUM_SALDO_ITEM_OFO_ACUMULADO'
}, inplace=True)

df_alertas = df_alertas.merge(df_alertas_aux, how='left', on=['CD_FILIAL', 'CD_ITEM', 'DATA'])

SALDO_ESTOQUE_SUPRIMENTO_AUX = \
SALDO_ESTOQUE_SUPRIMENTO[~(SALDO_ESTOQUE_SUPRIMENTO['CD_LOCAL_ESTOQUE'].isin([150, 296, 500]))][
    ['CD_FILIAL', 'CD_ITEM', 'DATA', 'ETQ_SALDO_ATUAL']]

SALDO_ESTOQUE_SUPRIMENTO_AUX = SALDO_ESTOQUE_SUPRIMENTO_AUX.groupby(['CD_FILIAL', 'CD_ITEM']).sum().reset_index()

df_alertas = df_alertas.merge(SALDO_ESTOQUE_SUPRIMENTO_AUX, how='left', on=['CD_FILIAL', 'CD_ITEM'])

df_alertas['ETQ_SALDO_ATUAL'].fillna(0, inplace=True)

# query = f"""
#     SELECT * FROM `mrp-ia.painel_mrp.lead_time_fornecedores`
# """

# LEAD_TIME = client.query(query).to_dataframe()

LEAD_TIME = pd.read_excel('gs://lead_time_mrp/Lead_time_fornecedores.xlsx')

LEAD_TIME = LEAD_TIME.groupby(['CD_FILIAL', 'CD_ITEM']).agg({'Lead_time': 'mean'}).reset_index()

df_alertas = df_alertas.merge(LEAD_TIME, how='left', on=['CD_FILIAL', 'CD_ITEM'])

df_alertas['Lead_time'].fillna(0, inplace=True)

df_alertas = df_alertas[df_alertas['DS_TIPO_REGISTRO'].isin(['Embalagens', 'Ingredientes'])]

df_alertas['ETQ_FIM_DO_DIA'] = df_alertas['ETQ_SALDO_ATUAL'] + df_alertas['SUM_SALDO_ITEM_OFO_ACUMULADO'] - df_alertas[
    'CONSUMO_PLANEJADO_ACUMULADO']

df_alertas = df_alertas[df_alertas['ETQ_FIM_DO_DIA'] <= 0]

df_alertas = df_alertas.groupby(['CD_FILIAL', 'CD_ITEM']).agg({'DATA': 'min', 'Lead_time': 'mean'}).reset_index()

df_alertas.rename(columns={'DATA': 'DATA_CORTE_GIRO_ETQ'}, inplace=True)

df_alertas['DT_LIMITE_ALERTA'] = df_alertas['DATA_CORTE_GIRO_ETQ'] - df_alertas['Lead_time'].apply(
    lambda x: datetime.timedelta(days=x)) - datetime.timedelta(days=20)

df_alertas['DT_LIMITE_CRITICO'] = df_alertas['DATA_CORTE_GIRO_ETQ'] - df_alertas['Lead_time'].apply(
    lambda x: datetime.timedelta(days=x))

df_alertas.sort_values(by=['CD_FILIAL', 'CD_ITEM', 'DT_LIMITE_ALERTA'], inplace=True)

df_alertas.loc[df_alertas['DT_LIMITE_ALERTA'] <= hoje, 'FLAG_ALERTA'] = 'ALERTA'

df_alertas.loc[df_alertas['DT_LIMITE_CRITICO'] <= hoje, 'FLAG_ALERTA'] = 'CRITICO'

# COMMAND ----------

#### Consumo últimos 30 dias

query_antiga = f"""

WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  REQ.CD_EMPRESA,
  FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  MOV.CD_ITEM,
  MOV.QT_MOVIMENTO,
  MOV.VL_MOVIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(MOV.DT_MOVIMENTO, 10)) AS DT_MOVIMENTO
FROM    
  `seara-analytics-prod.stg_erp_seara.movimento_etq_suprimento` MOV
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.requisicao_suprimento_snac` REQ
ON
  MOV.CD_REQUISICAO_SUPRIMENTO = REQ.CD_REQUISICAO_SUPRIMENTO
LEFT JOIN
  FILIAL
ON
  REQ.CD_FILIAL = FILIAL.CD_FILIAL
WHERE 
  REQ.CD_OPERACAO_SUPRIMENTO IN (900, 901, 906, 907, 910, 911, 916, 917, 918, 919, 920, 928, 929, 932, 933, 934, 935, 936, 937, 938, 939, 970, 971, 972, 973)
  AND REQ.CD_LOCAL_ESTOQUE_ATI != 299
  AND REQ.CD_SITUACAO  IN  (4, 5, 6)
  -- AND MOV.QT_MOVIMENTO < 0
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', MOV.DT_MOVIMENTO) between '{str(trinta_dias_atras)}' and '{str(hoje)}'
  AND FILIAL.CD_FILIAL_CENTRALIZADORA IN ({filiais})
  AND MOV.CD_ITEM NOT IN  (103179, 248606, 255505, 331988, 373818, 376388, 405033, 470643, 508977)
"""

# COMMAND ----------

#### Consumo últimos 30 dias

query = f"""
WITH FILIAL AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            CD_FILIAL_CENTRALIZADORA,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
  FILIAL_CGC10.CD_EMPRESA,
  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  -- REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL,
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM,
  ITEM3.NM_ITEM,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.QT_ITEM),2) AS QT_MOVIMENTO,
  ROUND(SUM(-ITEM_REQUISICAO_SPM_SNAC.VL_TOTAL_ITEM),2) AS VL_MOVIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) AS DT_MOVIMENTO
FROM
  `seara-analytics-prod.stg_erp_seara.requisicao_suprimento_snac`AS REQUISICAO_SUPRIMENTO_SNAC
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_requisicao_spm_snac` AS ITEM_REQUISICAO_SPM_SNAC
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_REQUISICAO_SUPRIMENTO=ITEM_REQUISICAO_SPM_SNAC.CD_REQUISICAO_SUPRIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item` AS ITEM3 
ON
  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM=ITEM3.CD_ITEM
LEFT JOIN 
  FILIAL AS FILIAL_CGC10
ON
  REQUISICAO_SUPRIMENTO_SNAC.CD_FILIAL=FILIAL_CGC10.CD_FILIAL
  AND REQUISICAO_SUPRIMENTO_SNAC.CD_EMPRESA=FILIAL_CGC10.CD_EMPRESA

WHERE
  ITEM_REQUISICAO_SPM_SNAC.ID_INTEGRACAO_ESTOQUE  =  'S'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_SITUACAO  IN  (4, 5, 6)
  AND  PARSE_DATE('%d/%m/%Y', LEFT(REQUISICAO_SUPRIMENTO_SNAC.DT_EMISSAO,10)) between '{str(trinta_dias_atras)}' and '{str(hoje)}'
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_OPERACAO_SUPRIMENTO  IN  (900, 901, 906, 907, 910, 911, 916, 917, 918, 919, 920, 928, 929, 932, 933, 934, 935, 936, 937, 938, 939, 970, 971, 972, 973)
  AND  ITEM_REQUISICAO_SPM_SNAC.CD_ITEM  NOT IN  (103179, 248606, 255505, 331988, 373818, 376388, 405033, 470643, 508977)
  AND  REQUISICAO_SUPRIMENTO_SNAC.CD_LOCAL_ESTOQUE_ATI  !=  299
  AND  FILIAL_CGC10.CD_FILIAL_CENTRALIZADORA  IN  ({filiais})
  
GROUP BY
  CD_EMPRESA,
  CD_FILIAL_CENTRALIZADORA,
  CD_FILIAL,
  CD_ITEM,
  NM_ITEM,
  DT_MOVIMENTO

ORDER BY 
  CD_EMPRESA,
  CD_FILIAL_CENTRALIZADORA,
  CD_ITEM,
  DT_MOVIMENTO
"""

df_consumo_30_dias = client.query(query).to_dataframe()

# df_consumo_30_dias['QT_MOVIMENTO'] = - df_consumo_30_dias['QT_MOVIMENTO']

# df_consumo_30_dias['VL_MOVIMENTO'] = - df_consumo_30_dias['VL_MOVIMENTO']

df_consumo_30_dias = df_consumo_30_dias.groupby(['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DT_MOVIMENTO']).agg(
    {'QT_MOVIMENTO': 'sum', 'VL_MOVIMENTO': 'sum'}).reset_index()

df_consumo_30_dias['QT_MOVIMENTO'] = round(df_consumo_30_dias['QT_MOVIMENTO'].astype(float), 2)

df_consumo_30_dias['VL_MOVIMENTO'] = round(df_consumo_30_dias['VL_MOVIMENTO'].astype(float), 2)

# COMMAND ----------

print('-----------------------------------------------------------')
print(datetime.datetime.now())
print('Fazendo os tratamentos finais e carregando as tabelas no BigQuery.')
print('-----------------------------------------------------------')

# COMMAND ----------

# #### consumo 30 dias

# carrega_tabela_bq("mrp-ia.painel_mrp", "consumo_30_dias", df_consumo_30_dias, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "consumo_30_dias", df_consumo_30_dias, 'WRITE_TRUNCATE')

# COMMAND ----------

# #### tabela_today

today = pd.DataFrame(columns={'Data'})

today['Data'] = [hoje]

# carrega_tabela_bq("mrp-ia.painel_mrp", "data_atual", today, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "data_atual", today, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### dim_itens_componentes

dm_itens_componentes = EXPLOSAO_FORMULA_PRODUTO[['CD_ITEM_COMPONENTE', 'DS_ITEM_COMPONENTE',
                                                 'DS_TIPO_REGISTRO']].drop_duplicates()  # EXPLOSAO_FORMULA_PRODUTO_painel[['CD_ITEM_COMPONENTE','DS_ITEM_COMPONENTE']].drop_duplicates()

dm_itens_componentes = dm_itens_componentes.merge(ITEM_FULL[['CD_ITEM', 'CD_SUBGRUPO_ITEM', 'NM_SUBGRUPO']], how='left',
                                                  left_on=['CD_ITEM_COMPONENTE'], right_on=['CD_ITEM'])

dm_itens_componentes.drop(columns=['CD_ITEM'], inplace=True)

dm_itens_componentes['FILTRO_SUBGRUPO'] = dm_itens_componentes['CD_SUBGRUPO_ITEM'].fillna(0).astype(int).astype(
    str) + ' - ' + dm_itens_componentes['NM_SUBGRUPO']

dm_itens_componentes.columns = [x.replace(" ", "_") for x in dm_itens_componentes.columns]

dm_itens_componentes['FILTRO_ITEM_COMPONENTE'] = (
            dm_itens_componentes['CD_ITEM_COMPONENTE'].astype(str) + ' - ' + dm_itens_componentes[
        'DS_ITEM_COMPONENTE']).apply(lambda x: x[:40] if not pd.isnull(x) else 'SEM DESCRICÃO')

# carrega_tabela_bq("mrp-ia.painel_mrp", "dim_itens_componentes", dm_itens_componentes, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "dim_itens_componentes", dm_itens_componentes, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### dm_itens_compostos

dm_itens_compostos = EXPLOSAO_FORMULA_PRODUTO[['CD_ITEM',
                                               'DS_ITEM']].drop_duplicates()  # EXPLOSAO_FORMULA_PRODUTO_painel[['CD_ITEM','DS_ITEM']].drop_duplicates()

dm_itens_compostos = dm_itens_compostos.merge(ITEM_FULL[['CD_ITEM', 'CD_SUBGRUPO_ITEM', 'NM_SUBGRUPO']], how='left',
                                              on=['CD_ITEM'])

dm_itens_compostos['FILTRO_SUBGRUPO'] = dm_itens_compostos['CD_SUBGRUPO_ITEM'].fillna(0).astype(int).astype(
    str) + ' - ' + dm_itens_compostos['NM_SUBGRUPO']

dm_itens_compostos.columns = [x.replace(" ", "_") for x in dm_itens_compostos.columns]

dm_itens_compostos['FILTRO_ITEM'] = (
            dm_itens_compostos['CD_ITEM'].astype(str) + ' - ' + dm_itens_compostos['DS_ITEM'].apply(
        lambda x: x[:40] if not pd.isnull(x) else 'SEM DESCRICÃO'))

# carrega_tabela_bq("mrp-ia.painel_mrp", "dim_itens_compostos", dm_itens_compostos, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "dim_itens_compostos", dm_itens_compostos, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### dim_filial

query = f"""
    WITH FILIAIS AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

    SELECT CD_EMPRESA, CD_FILIAL, NM_FILIAL FROM A WHERE A.rn = 1
)

SELECT
    *
FROM 
    FILIAIS
WHERE
    CD_FILIAL IN ({filiais})
"""

dim_filial = client.query(query).to_dataframe()

dim_filial['FILTRO_FILIAL'] = dim_filial['CD_FILIAL'].astype(str) + ' - ' + dim_filial['NM_FILIAL']

# carrega_tabela_bq("mrp-ia.painel_mrp", "dim_filial", dim_filial, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "dim_filial", dim_filial, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### formulas_utilizadas

formulas_utilizadas = EXPLOSAO_FORMULA_PRODUTO

formulas_utilizadas = formulas_utilizadas[[
    'CD_EMPRESA',
    'CD_FILIAL',
    'CD_ITEM',
    'CD_ITEM_COMPONENTE',
    'CONSUMO_NECESSARIO',
    'DS_ARVORE',
    'QT_TEMPO_CURA'
]]

formulas_utilizadas.columns = [unidecode.unidecode(x.replace(" ", "_")) for x in formulas_utilizadas.columns]

# carrega_tabela_bq("mrp-ia.painel_mrp", "formulas_utilizadas", formulas_utilizadas, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "formulas_utilizadas", formulas_utilizadas, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### plano_producao

plano_producao = PLANO_DIARIO_PDC_ITEM_CNE.merge(ITEM, how='left', on=['CD_ITEM']).round(0)

for filial in plano_producao['CD_FILIAL'].unique():
    itens_compostos_filial = EXPLOSAO_FORMULA_PRODUTO.loc[
        EXPLOSAO_FORMULA_PRODUTO['CD_FILIAL'] == filial, 'CD_ITEM'].unique()

    aux = df_sistema_final.loc[
        (df_sistema_final['CD_FILIAL'] == filial)
        & (df_sistema_final['CD_ITEM_COMPONENTE'].isin(itens_compostos_filial))
        ][['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM_COMPONENTE', 'DATA', 'CONSUMO_PLANEJADO']]

    aux.rename(columns={
        'CD_ITEM_COMPONENTE': 'CD_ITEM',
        'DATA': 'DT_PRODUCAO',
        'CONSUMO_PLANEJADO': 'QT_PESO_ALR'
    }, inplace=True)

    aux['ID_USUARIO'] = 'TABELAMRP'

    aux = aux.merge(ITEM, how='left', on=['CD_ITEM'])

    aux = aux[['CD_EMPRESA', 'CD_FILIAL', 'ID_USUARIO', 'CD_ITEM', 'DT_PRODUCAO', 'QT_PESO_ALR', 'DS_ITEM']]

    plano_producao = pd.concat([plano_producao, aux], ignore_index=True)

plano_producao = plano_producao[['CD_EMPRESA', 'CD_FILIAL', 'ID_USUARIO', 'CD_ITEM', 'DS_ITEM', 'QT_PESO_ALR',
                                 'DT_PRODUCAO']]

plano_producao.columns = [unidecode.unidecode(x.replace(" ", "_")) for x in plano_producao.columns]

# carrega_tabela_bq("mrp-ia.painel_mrp", "plano_producao", plano_producao, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "plano_producao", plano_producao, 'WRITE_TRUNCATE')

# COMMAND ----------

# #### tabela_ofs

tabela_ofs = OFS_ANDAMENTO_UNID.copy()

# carrega_tabela_bq("mrp-ia.painel_mrp", "tabela_ofs", tabela_ofs, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "tabela_ofs", tabela_ofs, 'WRITE_TRUNCATE')

# COMMAND ----------

# ##### tabela_mrp

tabela_mrp = df_sistema_final.copy()

tabela_mrp = tabela_mrp[~((tabela_mrp['DATA'].isna()) | (tabela_mrp['CD_FILIAL'].isna()))]

tabela_mrp.columns = [unidecode.unidecode(x.replace(" ", "_")) for x in tabela_mrp.columns]

tabela_mrp.drop(columns=['DS_ITEM_COMPONENTE', 'DS_ITEM', 'DS_TIPO_REGISTRO'], inplace=True)

# carrega_tabela_bq("mrp-ia.painel_mrp", "tabela_mrp", tabela_mrp, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "tabela_mrp", tabela_mrp, 'WRITE_TRUNCATE')

# COMMAND ----------

# df_consumo_30_dias[
#   df_consumo_30_dias['DT_MOVIMENTO'] == hoje
# ]

# COMMAND ----------

# #### alertas

# carrega_tabela_bq("mrp-ia.painel_mrp", "alertas", df_alertas, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "alertas", df_alertas, 'WRITE_TRUNCATE')

# COMMAND ----------

print(datetime.datetime.now().replace(tzinfo=pytz.utc).astimezone(tz))

# COMMAND ----------

print('Atualizando estoque suprimentos.')

# COMMAND ----------

query = f"""
WITH ITEM AS (
    SELECT
        ITEM.CD_ITEM,
        ITEM.NM_ITEM,
        ITEM.DS_ITEM,
        ITEM.CD_CLASSE_ITEM,
        CLASSE_ITEM.NM_CLASSE,
        ITEM.CD_GRUPO_ITEM,
        GRUPO_ITEM.NM_GRUPO,
        ITEM.CD_SUBGRUPO_ITEM,
        SUBGRUPO_ITEM.NM_SUBGRUPO,
        --ITEM.CD_FAMILIA_ITEM,
        ITEM.CD_FAMILIA_PRODUTO,
        FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
        FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
        FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
        FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
        FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO

    FROM
        `seara-analytics-prod.stg_erp_seara.item` AS ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
    ON
        ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
        AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
        AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
    ON
        ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
    ON
        SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
    ON
        FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
        --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
    ON
        FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
        AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
        AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
    LEFT JOIN 
        `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
    ON
        FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
        AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
        AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
        AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
        AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
),

FILIAL AS (

  WITH A AS (
      SELECT
          CD_EMPRESA,
          CD_FILIAL,
          CD_FILIAL_CENTRALIZADORA,
          NM_FILIAL,
          DT_ATUALIZACAO,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
      FROM
          `seara-analytics-prod.stg_erp_seara.filial_cgc`
      )

  SELECT * FROM A WHERE A.rn = 1
)


SELECT
  ESTOQUE.CD_EMPRESA,
  FILIAL.CD_FILIAL_CENTRALIZADORA AS CD_FILIAL,
  CONCAT(FILIAL.CD_FILIAL_CENTRALIZADORA, ' - ', FILIAL.NM_FILIAL) AS FILTRO_FILIAL,
  PARSE_DATE('%d/%m/%Y', LEFT(ESTOQUE.DT_MESANO_REFERENCIA,10)) AS DT_MESANO_REFERENCIA,
  ESTOQUE.CD_ITEM,
  CONCAT(ITEM.CD_ITEM, ' - ', ITEM.NM_ITEM) AS FILTRO_ITEM,
  ESTOQUE.CD_LOCAL_ESTOQUE,
  CONCAT(FILIAL.CD_EMPRESA,' - ',FILIAL.CD_FILIAL_CENTRALIZADORA, ' - ', ESTOQUE.CD_LOCAL_ESTOQUE) AS CHAVE_EMPRESA_FILIAL_LOCAL,
  LOCAL_ESTOQUE.DS_LOCAL_ESTOQUE,
  CONCAT(ESTOQUE.CD_LOCAL_ESTOQUE, ' - ', LOCAL_ESTOQUE.DS_LOCAL_ESTOQUE) AS FILTRO_LOCAL_ESTOQUE,
  ESTOQUE.CD_TIPO_CUSTO_SUPRIMENTO,
  CAST(ESTOQUE.QT_SALDO_ATUAL AS FLOAT64) AS QT_SALDO_ATUAL,
  CAST(ESTOQUE.VL_CUSTO_UNITARIO AS FLOAT64) AS VL_CUSTO_UNITARIO,
  CAST(ESTOQUE.VL_SALDO_ATUAL AS FLOAT64) AS VL_SALDO_ATUAL,
  ITEM.NM_CLASSE,
  ITEM.CD_GRUPO_ITEM,
  ITEM.NM_GRUPO,
  -- CONCAT(ITEM.CD_GRUPO_ITEM, ' - ', ITEM.NM_GRUPO) AS FILTRO_GRUPO,
  ITEM.CD_SUBGRUPO_ITEM,
  ITEM.NM_SUBGRUPO,
  -- CONCAT(ITEM.CD_SUBGRUPO_ITEM, ' - ', ITEM.NM_SUBGRUPO) AS FILTRO_SUBGRUPO
FROM
  `seara-analytics-prod.stg_erp_seara.saldo_estoque_spm_mes` ESTOQUE
LEFT JOIN
  ITEM
ON
  ITEM.CD_ITEM = ESTOQUE.CD_ITEM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.local_estoque_suprimento` LOCAL_ESTOQUE
ON
  ESTOQUE.CD_EMPRESA = LOCAL_ESTOQUE.CD_EMPRESA
  AND ESTOQUE.CD_FILIAL = LOCAL_ESTOQUE.CD_FILIAL
  AND ESTOQUE.CD_LOCAL_ESTOQUE = LOCAL_ESTOQUE.CD_LOCAL_ESTOQUE
LEFT JOIN
  FILIAL
ON
  ESTOQUE.CD_FILIAL = FILIAL.CD_FILIAL
  
WHERE
  ESTOQUE.CD_EMPRESA IN (30, 36)
  AND FILIAL.CD_FILIAL_CENTRALIZADORA IN  ({filiais})
  AND QT_SALDO_ATUAL > 0
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ESTOQUE.DT_MESANO_REFERENCIA) >= '2021-01-01'
"""

df_saldo_estoque_mes = client.query(query).to_dataframe()

fatos_saldo_estoque_mes = df_saldo_estoque_mes[[
 'CD_EMPRESA',
 'CD_FILIAL',
#  'FILTRO_FILIAL',
 'DT_MESANO_REFERENCIA',
 'CD_ITEM',
#  'FILTRO_ITEM',
#  'CD_LOCAL_ESTOQUE',
  'CHAVE_EMPRESA_FILIAL_LOCAL',
#  'DS_LOCAL_ESTOQUE',
 'FILTRO_LOCAL_ESTOQUE',
#  'CD_TIPO_CUSTO_SUPRIMENTO',
 'QT_SALDO_ATUAL',
 'VL_CUSTO_UNITARIO',
 'VL_SALDO_ATUAL',
#  'NM_CLASSE',
#  'NM_GRUPO',
#  'NM_SUBGRUPO'
]].copy()

for coluna in ['QT_SALDO_ATUAL','VL_CUSTO_UNITARIO','VL_SALDO_ATUAL']:

    fatos_saldo_estoque_mes[coluna] = round(fatos_saldo_estoque_mes[coluna],2)


# carrega_tabela_bq("mrp-ia.mrp_estoque_modelado", "fatos_saldo_estoque_mes", fatos_saldo_estoque_mes, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "mrp_estoque_modelado", "fatos_saldo_estoque_mes", fatos_saldo_estoque_mes, 'WRITE_TRUNCATE')

# COMMAND ----------

lista_itens_estoque = list(fatos_saldo_estoque_mes['CD_ITEM'].unique())

ITEM_filtrado = ITEM_FULL[
  ITEM_FULL['CD_ITEM'].isin(lista_itens_estoque)
].copy()

ITEM_filtrado['FILTRO_ITEM'] = ITEM_filtrado['CD_ITEM'].astype(str) + ' - ' + ITEM_filtrado['NM_ITEM']

# carrega_tabela_bq("mrp-ia.mrp_estoque_modelado", "dim_item", ITEM_filtrado, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "mrp_estoque_modelado", "dim_item", ITEM_filtrado, 'WRITE_TRUNCATE')

# COMMAND ----------

query = """
  SELECT
    CD_EMPRESA,
    CD_FILIAL,
    CD_LOCAL_ESTOQUE,
    DS_LOCAL_ESTOQUE,
    CONCAT(CD_EMPRESA,' - ',CD_FILIAL,' - ','CD_LOCAL_ESTOQUE') AS CHAVE_EMPRESA_FILIAL_LOCAL,
    CONCAT(CD_LOCAL_ESTOQUE,' - ',DS_LOCAL_ESTOQUE) AS FILTRO_LOCAL_ESTOQUE
  FROM
    `seara-analytics-prod.stg_erp_seara.local_estoque_suprimento`
"""

df_local_estoque = client.query(query).to_dataframe()

# carrega_tabela_bq("mrp-ia.mrp_estoque_modelado", "dim_local_estoque", df_local_estoque, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "mrp_estoque_modelado", "dim_local_estoque", df_local_estoque, 'WRITE_TRUNCATE')

# COMMAND ----------

SALDO_ESTOQUE_SUPRIMENTO = SALDO_ESTOQUE_SUPRIMENTO.merge(
  df_local_estoque[['CD_EMPRESA', 'CD_FILIAL','CD_LOCAL_ESTOQUE','FILTRO_LOCAL_ESTOQUE']],
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL','CD_LOCAL_ESTOQUE']
)

# COMMAND ----------

# #### saldo_estoque

# carrega_tabela_bq("mrp-ia.painel_mrp", "saldo_estoque", SALDO_ESTOQUE_SUPRIMENTO, "WRITE_TRUNCATE")

carrega_tabela_bq("mrp-ia", "painel_mrp", "saldo_estoque", SALDO_ESTOQUE_SUPRIMENTO, 'WRITE_TRUNCATE')

# COMMAND ----------

# SOLICITACOES DE COMPRA

query = f"""

SELECT
  SOLICITACAO_COMPRA.CD_EMPRESA,
  SOLICITACAO_COMPRA.CD_FILIAL,
  SOLICITACAO_COMPRA.CD_SOLICITACAO_COMPRA,
  SOLICITACAO_COMPRA.CD_SITUACAO_SOLICITACAO,
  SITUACAO_SOLICITACAO.DS_SITUACAO_SOLICITACAO,
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', SOLICITACAO_COMPRA.DT_SOLICITACAO_COMPRA)) DT_SOLICITACAO_COMPRA,
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', SOLICITACAO_COMPRA.DT_ULTIMA_ALTERACAO)) DT_ULTIMA_ALTERACAO,
  SOLICITACAO_COMPRA.CD_USUARIO_SOLICITANTE,
  SOLICITACAO_COMPRA.CD_COMPRADOR,
  SOLICITACAO_COMPRA.CD_CENTRO_CUSTO,
  SOLICITACAO_COMPRA.DS_MSG_ENVIO_MDC_ELE,
  ITEM_SOLICITACAO_COMPRA.CD_ITEM,
  ITEM_SOLICITACAO_COMPRA.NUM_SEQ,
  ITEM_SOLICITACAO_COMPRA.CD_UNIDADE_MEDIDA,
  ITEM_SOLICITACAO_COMPRA.DS_MATERIAL,
  ITEM_SOLICITACAO_COMPRA.QT_SOLICITADA,
  ITEM_SOLICITACAO_COMPRA.VL_ESTIMADO,
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', ITEM_SOLICITACAO_COMPRA.DT_ENTREGA)) DT_ENTREGA,
  ITEM_SOLICITACAO_COMPRA.ID_CANCELADO
FROM
  `seara-analytics-prod.stg_erp_seara.solicitacao_compra` SOLICITACAO_COMPRA
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.item_solicitacao_compra` ITEM_SOLICITACAO_COMPRA
ON
  SOLICITACAO_COMPRA.CD_EMPRESA = ITEM_SOLICITACAO_COMPRA.CD_EMPRESA_ETQ
  AND SOLICITACAO_COMPRA.CD_FILIAL = ITEM_SOLICITACAO_COMPRA.CD_FILIAL_ETQ
  AND SOLICITACAO_COMPRA.CD_SOLICITACAO_COMPRA = ITEM_SOLICITACAO_COMPRA.CD_SOLICITACAO_COMPRA
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.situacao_solicitacao` SITUACAO_SOLICITACAO 
ON
  SITUACAO_SOLICITACAO.CD_SITUACAO_SOLICITACAO = SOLICITACAO_COMPRA.CD_SITUACAO_SOLICITACAO
WHERE
  ITEM_SOLICITACAO_COMPRA.ID_CANCELADO IS NULL
  AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S', SOLICITACAO_COMPRA.DT_SOLICITACAO_COMPRA) >= '{trinta_dias_atras}'
  AND SOLICITACAO_COMPRA.CD_SITUACAO_SOLICITACAO NOT IN (7,8)
--   AND SOLICITACAO_COMPRA.CD_EMPRESA = 30
--   AND SOLICITACAO_COMPRA.CD_FILIAL = 909
--   AND ITEM_SOLICITACAO_COMPRA.CD_ITEM = 200506
ORDER BY 
  CD_EMPRESA,
  CD_FILIAL,
  CD_SOLICITACAO_COMPRA,
  CD_ITEM,
  NUM_SEQ

"""

df_solicitacao_compra = client.query(query).to_dataframe()

carrega_tabela_bq("mrp-ia", "painel_mrp", "mrp_solicitacoes_compra", df_solicitacao_compra, 'WRITE_TRUNCATE')

print(datetime.datetime.now().replace(tzinfo=pytz.utc).astimezone(tz))
