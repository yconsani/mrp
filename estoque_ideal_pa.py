# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ### Importando biliotecas e definindo variáveis

# COMMAND ----------

# !pip install fsspec
# !pip install gcsfs
# !pip install openpyxl
# !pip install google-cloud-bigquery
# !pip install google-api-core==1.31.1

# COMMAND ----------

# import pandas as pd
import pyspark.sql.functions as F
from pyspark.sql.functions import col, coalesce, when, lit, datediff, asc, desc, concat_ws, to_timestamp, to_date, concat
from pyspark.sql.types import DoubleType, IntegerType, StringType, DateType, StructType, StructField
from pyspark.sql import Window
import datetime
from datetime import timedelta
import numpy as np
import pandas as pd
import scipy.stats as stats
import pytz

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

spark.conf.set("viewsEnabled","true")

from google.cloud import bigquery as bq
client = bq.Client()

# COMMAND ----------

# função que para facilitar o carregamento de tabelas no bigquery

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.
  
#   df.to_gbq(
#     f'{nome_dataset}.{nome_tabela}',
#     project_id=id_projeto,
#     chunksize=None,
#     if_exists=modo
#   )
  
#   return print(f'tabela {nome_tabela} carregada no local {id_projeto}.{nome_dataset}.{nome_tabela}')  #print(f"Loaded dataframe to {table_ref.path}")

# COMMAND ----------

def retorna_inicio_mes_anterior(data, num_meses):
  
  for i in range(num_meses):
  
    inicio_do_mes = data.replace(day = 1)

    final_do_mes_anterior = inicio_do_mes - datetime.timedelta(days=1)

    inicio_do_mes_anterior = final_do_mes_anterior.replace(day = 1)

    data = inicio_do_mes_anterior
    
  return data

# COMMAND ----------

def retorna_inicio_do_mes(x):
  
  return x.replace(day = 1)

retorna_inicio_do_mes_UDF = F.udf(lambda x: retorna_inicio_do_mes(x), DateType())

# COMMAND ----------

def adiciona_zero_esquerda(valor):
  
  if int(valor) < 10:
    
    return '0'+str(valor)
  
  else:
  
    return str(valor)
  
adiciona_zero_esquerda_UDF = F.udf(lambda x: adiciona_zero_esquerda(x), StringType())

# COMMAND ----------

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

inicio_do_mes = hoje.replace(day = 1)

final_do_mes_anterior = inicio_do_mes - datetime.timedelta(days=1)

trinta_dias_atras = hoje - datetime.timedelta(days=30)

inicio_do_mes_d_menos_3 = retorna_inicio_mes_anterior(hoje, 3)

inicio_do_mes_d_menos_5 = retorna_inicio_mes_anterior(hoje, 5)

inicio_do_mes_d_menos_6 = retorna_inicio_mes_anterior(hoje, 6)

data_ref = str(hoje)[:10]

# COMMAND ----------

num_dias_diarizacao = 21

# COMMAND ----------

lista_motivos_cancelamento_por_falta_estoque = [
    4,
    6,
    19,
    27,
    28,
    40,
    45,
    51,
    52,
    53,
    59,
    69,
    787
  ]

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Dimensão das filiais

# COMMAND ----------

query = """
WITH FILIAIS AS (

    WITH A AS (
        SELECT
            FILIAL_CGC.CD_FILIAL,
            FILIAL_CGC.NM_FILIAL,
            FILIAL_CGC.DT_ATUALIZACAO,
            FILIAL_CGC.ID_LOGISTICA,
            REF_CODES.DS_LOGISTICA,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',FILIAL_CGC.DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_CGC         
        LEFT JOIN (
          SELECT
            RV_LOW_VALUE AS ID_LOGISTICA,
            RV_MEANING AS DS_LOGISTICA
          FROM
            `seara-analytics-prod.stg_erp_seara.bceg_ref_codes`
          WHERE
            RV_DOMAIN = "FILIAL_CGC.ID_LOGISTICA"
             ) REF_CODES
          ON
            REF_CODES.ID_LOGISTICA = FILIAL_CGC.ID_LOGISTICA
          )

SELECT * FROM A WHERE A.rn = 1
)

SELECT 
    CD_FILIAL, 
    NM_FILIAL,
    CONCAT(CD_FILIAL,' - ',NM_FILIAL) AS FILTRO_FILIAL,
    ID_LOGISTICA,
    DS_LOGISTICA
FROM 
    FILIAIS
ORDER BY 
    CD_FILIAL
"""

df_filiais = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_filiais.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.dim_filial').save()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Dimensão dos itens

# COMMAND ----------

query_tabela_item = """
   SELECT
    CONCAT(ITEM.CD_ITEM, ' - ', ITEM.NM_ITEM) AS FILTRO_ITEM,
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ORIGEM.ID_ORIGEM,
    ORIGEM.DS_ORIGEM,
    MARCA_PRODUTO.NM_MARCA
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
LEFT JOIN
(
SELECT
  RV_LOW_VALUE AS ID_ORIGEM,
  -- RV_DOMAIN,
  RV_MEANING AS DS_ORIGEM
FROM
  `seara-analytics-prod.erp_seara.bcmg_ref_codes`
WHERE
  RV_DOMAIN = 'ITEM.ID_ORIGEM'
) AS ORIGEM
ON
  CAST(ORIGEM.ID_ORIGEM AS INT) = ITEM.ID_ORIGEM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.marca_produto` MARCA_PRODUTO
ON
  MARCA_PRODUTO.CD_MARCA_PRODUTO = ITEM.CD_MARCA_PRODUTO
WHERE
  ITEM.ID_SITUACAO = 1 -- Somente itens ativos
  AND ITEM.CD_NEGOCIO IN (35, 66)-- 66 = MARGARINAS E INDUSTRIALIZADOS
  AND ITEM.CD_GRUPO_ITEM IN (3) -- 3 = PRODUTOS
  AND ITEM.CD_SUBGRUPO_ITEM IN (4,30,40,45,58) -- 4 = INDUSTRIALIZADOS, 30 = PRODUTOS REVENDA, 40 = REVENDA, 45  = PRODUTOS REVENDA
  AND ITEM.CD_DESTINO_FINAL IN (1) -- 1 = Mercado Interno
  -- Resta filtro de marca
"""

ITEM = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query_tabela_item).load()

# COMMAND ----------

lista_itens_ind = [x['CD_ITEM'] for x in ITEM.select('CD_ITEM').distinct().collect()]

# COMMAND ----------

str_lista_itens_ind = str(lista_itens_ind).replace('[','').replace(']','')

# COMMAND ----------

ITEM.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.dim_item').save()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Itens Heroes

# COMMAND ----------

lista_itens_heroes = [
  5487,
  6300,
  9741,
  82708,
  326046,
  500245,
  571954,
  574783,
  5550,
  9865,
  572462,
  44030
]

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### De/para filiais newco e tsps

# COMMAND ----------

query = """
WITH 

ESTOQUE_FILIAIS AS ( 
  SELECT
    CD_EMPRESA,
    CD_FILIAL,
    SUM(QT_VOLUME) AS QT_VOLUME
  FROM
    `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
  WHERE
    PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_REFERENCIA) = (
      SELECT
        MAX(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_REFERENCIA))
      FROM
        `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
      )
  GROUP BY
    CD_EMPRESA,
    CD_FILIAL
),

FILIAIS_NEWCO_SEM_ESTOQUE AS (

SELECT
  CD_EMPRESA,
  CD_FILIAL,
  NM_FILIAL,
  NM_CURTO,
  CD_CEP_LOGRADOURO
FROM
  `seara-analytics-prod.stg_erp_seara.filial_cgc`
WHERE
  DT_FIM_ATIVIDADE IS NULL
  AND CD_EMPRESA = 178
  AND CONCAT(CD_EMPRESA, ' - ', CD_FILIAL) NOT IN ( 
    SELECT
      DISTINCT CONCAT(CD_EMPRESA, ' - ', CD_FILIAL) AS CHAVE_EMPRESA_FILIAL
    FROM
      `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
    WHERE
      PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_REFERENCIA) = (
        SELECT
          MAX(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_REFERENCIA))
        FROM
          `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
        )
      AND QT_VOLUME > 0
   )
ORDER BY
  CD_CEP_LOGRADOURO
  
),

FILIAIS_SEARA AS (
  SELECT
    FILIAL_CGC.CD_EMPRESA,
    FILIAL_CGC.CD_FILIAL,
    FILIAL_CGC.CD_FILIAL_CENTRALIZADORA,
    FILIAL_CGC.NM_FILIAL,
    FILIAL_CGC.NM_CURTO,
    FILIAL_CGC.CD_CEP_LOGRADOURO,
    REF_CODES.DS_LOGISTICA,
    ESTOQUE_FILIAIS.QT_VOLUME
  FROM
    `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_CGC
  LEFT JOIN (
      SELECT
        RV_LOW_VALUE AS ID_LOGISTICA,
        RV_MEANING AS DS_LOGISTICA
      FROM
        `seara-analytics-prod.stg_erp_seara.bceg_ref_codes`
      WHERE
        RV_DOMAIN = "FILIAL_CGC.ID_LOGISTICA"
   ) REF_CODES
  ON
    REF_CODES.ID_LOGISTICA = FILIAL_CGC.ID_LOGISTICA
  LEFT JOIN
    ESTOQUE_FILIAIS
  ON
    ESTOQUE_FILIAIS.CD_EMPRESA = FILIAL_CGC.CD_EMPRESA
    AND ESTOQUE_FILIAIS.CD_FILIAL = FILIAL_CGC.CD_FILIAL
  WHERE
    FILIAL_CGC.DT_FIM_ATIVIDADE IS NULL
    AND FILIAL_CGC.CD_EMPRESA != 178
),

BASE AS (

SELECT
  FILIAIS_NEWCO_SEM_ESTOQUE.CD_EMPRESA AS CD_EMPRESA_NEWCO,
  FILIAIS_NEWCO_SEM_ESTOQUE.CD_FILIAL AS CD_FILIAL_NEWCO,
  FILIAIS_NEWCO_SEM_ESTOQUE.NM_FILIAL AS NM_FILIAL_NEWCO,
  FILIAIS_SEARA.CD_EMPRESA,
  FILIAIS_SEARA.CD_FILIAL,
  FILIAIS_SEARA.NM_FILIAL,
  FILIAIS_SEARA.DS_LOGISTICA,
  FILIAIS_SEARA.QT_VOLUME,
  ROW_NUMBER() OVER (PARTITION BY
                        FILIAIS_NEWCO_SEM_ESTOQUE.CD_EMPRESA,
                        FILIAIS_NEWCO_SEM_ESTOQUE.CD_FILIAL
                      ORDER BY QT_VOLUME DESC
                         ) as rn
FROM
  FILIAIS_NEWCO_SEM_ESTOQUE
LEFT JOIN
  FILIAIS_SEARA
ON
  FILIAIS_SEARA.CD_CEP_LOGRADOURO = FILIAIS_NEWCO_SEM_ESTOQUE.CD_CEP_LOGRADOURO
ORDER BY
  FILIAIS_NEWCO_SEM_ESTOQUE.CD_FILIAL

)

SELECT
  BASE.CD_EMPRESA_NEWCO AS CD_EMPRESA,
  BASE.CD_FILIAL_NEWCO AS CD_FILIAL,
  BASE.CD_EMPRESA AS NOVO_CD_EMPRESA,
  BASE.CD_FILIAL AS NOVO_CD_FILIAL
FROM
  BASE
WHERE
  rn = 1
  -- AND CD_FILIAL_NEWCO = 697
ORDER BY
  CD_FILIAL_NEWCO
"""

df_newco_fisico = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_de_para_filiais = df_newco_fisico.alias('df_de_para_filiais')

# COMMAND ----------

df_tsps_fisico = pd.DataFrame(columns=['CD_EMPRESA','CD_FILIAL', 'NOVO_CD_EMPRESA', 'NOVO_CD_FILIAL'])

lista_linhas = [
  [178, 677, 178, 676],
  [178, 38, 178, 676],
  [30, 932, 30, 892],
  [178, 699, 178, 675]
]

for linha in lista_linhas:
  
  df_tsps_fisico.loc[df_tsps_fisico.shape[0]] = linha

lista_tsps = list(df_tsps_fisico['CD_FILIAL'].unique())

df_tsps_fisico = spark.createDataFrame(df_tsps_fisico)

# df_tsps_fisico.display()

# COMMAND ----------

df_de_para_filiais = df_de_para_filiais.filter(
  ~(col('CD_FILIAL').isin(lista_tsps))
)

df_de_para_filiais = df_de_para_filiais.union(df_tsps_fisico)

df_de_para_filiais = df_de_para_filiais.filter(
    (~col('NOVO_CD_EMPRESA').isNull())
    &(~col('NOVO_CD_FILIAL').isNull())
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Troca de notas

# COMMAND ----------

query = f"""

SELECT
  *
FROM
  `mrp-ia.estoque_ideal_produto_acabado.view_troca_nota_2_pernas`
"""

df_troca_nota_2_pernas = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

query = f"""

SELECT
  *
FROM
  `mrp-ia.estoque_ideal_produto_acabado.view_troca_nota_3_pernas`
"""

df_troca_nota_3_pernas = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_troca_nota = df_troca_nota_3_pernas.select(
#  'dt_processo',
#  'CD_PEDIDO_TRANSF',
#  'CD_PEDIDO_TROCA',
#  'CD_PEDIDO_VENDA',
#  'Cd_Seq_Item_Ped_Vda',
 'NOTA_TRANSF',
 'NF_TROCA',
#  'NF_TROCA_TROCA',
#  'cd_viagem_transf',
#  'cd_viagem_troca',
#  'CD_VIAGEM_TROCA_TROCA',
#  'ITEM_TRANSF',
#  'TROCA_TRANSF',
#  'ITEM_PEDIDO_TROCA',
#  'ITEM_TROCA',
#  'TROCA',
#  'qt_entrega_mtz_troca',
#  'qt_entrega_fil_troca',
#  'qt_entrega_mtz_transf',
#  'qt_entrega_fil_transf',
#  'cd_cep_logradouro',
#  'localidade_transf',
#  'uf_transf',
#  'cep_troca',
#  'nm_localidade_troca',
#  'uf_troca',
#  'cep_final',
#  'nm_localidade_final',
#  'uf_final',
 'cd_empresa_transf',
 'cd_filial_transf',
#  'nm_filial_origem',
 'cd_empresa_venda',
 'cd_filial_venda',
#  'nm_filial_destino',
 'CD_EMP_TROCA_TROCA',
 'CD_FIL_TROCA_TROCA',
#  'NM_FILIAL_TROCA_TROCA',
#  'nm_item_transf',
#  'nm_item_troca',
#  'ITEM_TROCA_TROCA',
#  'CD_OPE_FISCAL_TPT_TRANSF',
#  'DS_OPE_fiscal_TPT_TRANSF',
#  'CD_OPE_FISCAL_TPT_TROCA',
#  'DS_OPE_FISCAL_TPT_TROCA',
#  'CD_OPE_FISCAL_TPT_TROCA_TROCA',
#  'DS_OPE_FISCAL_TPT_TROCA_TROCA',
#  'ope_fiscal_venda_venda',
#  'natureza_operacao_venda',
#  'ope_fiscal_venda_FINAL',
#  'natureza_operacao_venda_FINAL',
#  'ope_fiscal_venda_transf',
#  'natureza_operacao_transf',
#  'ope_venda_troca',
#  'descricao_ope_venda_troca',
#  'ope_venda_FINAL',
#  'descricao_ope_venda_FINAL',
#  'ope_venda_transf',
#  'descricao_ope_venda_transf',
#  'cd_base_cliente_transf',
#  'cd_loja_cliente_transf',
#  'nm_cliente_transf',
#  'cd_base_cli_troca',
#  'lojacli_destino',
#  'nm_cliente_destino',
#  'nm_curto_cli_destino',
#  'NM_CLIENTE_TROCA_TROCA',
#  'NM_CURTO_CLI_TROCA_TROCA',
#  'cd_classe_item',
#  'nm_classe',
#  'cd_negocio',
#  'nm_negocio',
#  'cd_destino_final',
#  'volume_transf_nf',
#  'volume_troca_nf',
#  'VOLUME_TROCA_TROCA',
#  'VOLUME_DEST_FINAL',
#  'id_filtro_troca_nota',
#  'dat_fat_nf_troca',
#  'dat_fat_nf_transf',
#  'DAT_FAT_TROCA_TROCA',
#  'id_troca_nota',
#  'ID_TROCA_TROCA',
#  'motivo_ccl_transf',
#  'motivo_ccl_troca',
#  'vl_frete_transf',
#  'vl_mercadoria_transf',
#  'km_transf',
#  'vl_frete_troca',
#  'vl_mercadoria_troca',
#  'km_troca',
#  'vl_frete_TROCA_TROCA',
#  'vl_mercadoria_TROCA_TROCA',
#  'km_TROCA_TROCA',
#  'cd_agr_tributario',
#  'cd_entrada_saida',
#  'cd_tipo_operacao_fiscal_troca',
#  'ds_tipo_operacao_fiscal_troca',
#  'nm_municipio_final',
#  'cd_opetransp_transf',
#  'ds_ope_transp_transf',
#  'cd_ope_transp_troca',
#  'nm_ope_transp_troca',
#  'cd_ope_transp_VENDA_FINAL',
#  'nm_ope_transp_VENDA_FINAL',
#  'uf_final_cliente',
#  'cd_tipo_operacao_fiscal_transf',
#  'ds_tipo_operacao_fiscal_transf',
#  'cd_tipo_ope_fiscal_venda_final',
#  'ds_tipo_ope_fiscal_venda_final',
#  'cd_motivo_ccl_troca_troca',
#  'cd_divisao_empresarial',
#  'cd_emp_nf_1perna',
#  'cd_fil_nf_1perna'
).distinct()

# COMMAND ----------

# df_troca_nota.filter(
#   (col('NOTA_TRANSF') == 24988)
# ).display()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Lead time de transferência

# COMMAND ----------

query = f"""
WITH 

LIVRO_ENTRADA AS ( 
  SELECT
    LIVRO_FISCAL_ENTRADA.CD_BASE_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_ESTAB_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_NOTA_FISCAL,
    LIVRO_FISCAL_ENTRADA.CD_SERIE,
    LIVRO_FISCAL_ENTRADA.CD_EMPRESA AS CD_EMPRESA_DESTINO,
    LIVRO_FISCAL_ENTRADA.CD_FILIAL AS CD_FILIAL_DESTINO,
    EMPRESA.NR_BASE_CGC,
    FILIAL_CGC.NR_ESTAB_FILIAL_CGC,
    MAX(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', LIVRO_FISCAL_ENTRADA.DT_ENTRADA_SAIDA_NFI)) AS DT_ENTRADA
  FROM
    `seara-analytics-prod.stg_erp_seara.livro_fiscal_entrada` LIVRO_FISCAL_ENTRADA
  LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
  ON
    EMPRESA.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
  LEFT JOIN 
    (
      SELECT
        *
      FROM
        `seara-analytics-prod.stg_erp_seara.filial_cgc` 
      WHERE
        DT_FIM_ATIVIDADE IS NULL
    ) FILIAL_CGC 
  ON
    FILIAL_CGC.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
    AND FILIAL_CGC.CD_FILIAL = LIVRO_FISCAL_ENTRADA.CD_FILIAL
  GROUP BY 
    LIVRO_FISCAL_ENTRADA.CD_BASE_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_ESTAB_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_NOTA_FISCAL,
    LIVRO_FISCAL_ENTRADA.CD_SERIE,
    LIVRO_FISCAL_ENTRADA.CD_EMPRESA,
    LIVRO_FISCAL_ENTRADA.CD_FILIAL,
    EMPRESA.NR_BASE_CGC,
    FILIAL_CGC.NR_ESTAB_FILIAL_CGC
),

NOTA_FISCAL_SAIDA_ACM AS (
  SELECT
    NOTA_FISCAL.CD_EMPRESA,
    NOTA_FISCAL.CD_FILIAL,
    NOTA_FISCAL.CD_NOTA_FISCAL,
    NOTA_FISCAL.CD_SERIE,
    NOTA_FISCAL.CD_SEQUENCIA_HISTORICO,
    NOTA_FISCAL.CD_BASE_CLIENTE,
    NOTA_FISCAL.CD_LOJA_CLIENTE,
    NOTA_FISCAL.ID_ACAO,
    NOTA_FISCAL.CD_PLACA_CAVALO,
    NOTA_FISCAL.QT_PESO_LIQUIDO,
    NOTA_FISCAL.QT_PESO_BRUTO,
    NOTA_FISCAL.DT_CANCELAMENTO_NOTA,
    NOTA_FISCAL.CD_MOTIVO_CANCELAMENTO,
    NOTA_FISCAL.CD_NFS_INTERMEDIARIO,
    NOTA_FISCAL.CD_NOTA_FISCAL_REFATURADA,
    NOTA_FISCAL.CD_TIPO_LOJA_CLIENTE,
    NOTA_FISCAL.CD_NATUREZA_OPERACAO,
    NOTA_FISCAL.ID_TIPO_NOTA_FISCAL,
    NOTA_FISCAL.CD_OPERACAO_FISCAL,
    NOTA_FISCAL.CD_VIAGEM,
    NOTA_FISCAL.CD_MEIO_TRANSPORTE,
    NOTA_FISCAL.CD_PLACA,
    NOTA_FISCAL.DT_EMISSAO,
    NOTA_FISCAL.DT_PROCESSO,
    NOTA_FISCAL.CD_PEDIDO,
    NOTA_FISCAL.VL_TOTAL_NOTA_FISCAL,
    NOTA_FISCAL.DT_VENCIMENTO,
    NOTA_FISCAL.DT_PREVISAO_CHEGADA,
    NOTA_FISCAL.DT_ATUALIZACAO,
    NOTA_FISCAL.CD_ENTRADA_SAIDA,
    NOTA_FISCAL.NR_SEQUENCIA_VIAGEM,
    MAX(DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', NOTA_FISCAL.DT_ENTRADA_SAIDA))) AS DT_SAIDA
  FROM
    `seara-analytics-prod.stg_erp_seara.nota_fiscal_saida_acm` NOTA_FISCAL
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item_nota_fiscal_saida_acm` ITEM_NOTA_FISCAL
  ON
    NOTA_FISCAL.CD_SEQUENCIA_HISTORICO = ITEM_NOTA_FISCAL.CD_SEQUENCIA_HISTORICO AND
    NOTA_FISCAL.CD_NOTA_FISCAL = ITEM_NOTA_FISCAL.CD_NOTA_FISCAL AND
    NOTA_FISCAL.CD_EMPRESA = ITEM_NOTA_FISCAL.CD_EMPRESA AND
    NOTA_FISCAL.CD_ENTRADA_SAIDA = ITEM_NOTA_FISCAL.CD_ENTRADA_SAIDA AND
    NOTA_FISCAL.CD_FILIAL = ITEM_NOTA_FISCAL.CD_FILIAL AND
    NOTA_FISCAL.CD_SERIE = ITEM_NOTA_FISCAL.CD_SERIE    
  WHERE
    ITEM_NOTA_FISCAL.CD_ITEM IN ({str_lista_itens_ind})
  GROUP BY
    NOTA_FISCAL.CD_EMPRESA,
    NOTA_FISCAL.CD_FILIAL,
    NOTA_FISCAL.CD_NOTA_FISCAL,
    NOTA_FISCAL.CD_SERIE,
    NOTA_FISCAL.CD_SEQUENCIA_HISTORICO,
    NOTA_FISCAL.CD_BASE_CLIENTE,
    NOTA_FISCAL.CD_LOJA_CLIENTE,
    NOTA_FISCAL.ID_ACAO,
    NOTA_FISCAL.CD_PLACA_CAVALO,
    NOTA_FISCAL.QT_PESO_LIQUIDO,
    NOTA_FISCAL.QT_PESO_BRUTO,
    NOTA_FISCAL.DT_CANCELAMENTO_NOTA,
    NOTA_FISCAL.CD_MOTIVO_CANCELAMENTO,
    NOTA_FISCAL.CD_NFS_INTERMEDIARIO,
    NOTA_FISCAL.CD_NOTA_FISCAL_REFATURADA,
    NOTA_FISCAL.CD_TIPO_LOJA_CLIENTE,
    NOTA_FISCAL.CD_NATUREZA_OPERACAO,
    NOTA_FISCAL.ID_TIPO_NOTA_FISCAL,
    NOTA_FISCAL.CD_OPERACAO_FISCAL,
    NOTA_FISCAL.CD_VIAGEM,
    NOTA_FISCAL.CD_MEIO_TRANSPORTE,
    NOTA_FISCAL.CD_PLACA,
    NOTA_FISCAL.DT_EMISSAO,
    NOTA_FISCAL.DT_PROCESSO,
    NOTA_FISCAL.CD_PEDIDO,
    NOTA_FISCAL.VL_TOTAL_NOTA_FISCAL,
    NOTA_FISCAL.DT_VENCIMENTO,
    NOTA_FISCAL.DT_PREVISAO_CHEGADA,
    NOTA_FISCAL.DT_ATUALIZACAO,
    NOTA_FISCAL.CD_ENTRADA_SAIDA,
    NOTA_FISCAL.NR_SEQUENCIA_VIAGEM
)

SELECT
  NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA AS CD_EMPRESA_ORIGEM,
  NOTA_FISCAL_SAIDA_ACM.CD_FILIAL AS CD_FILIAL_ORIGEM,
  EMPRESA.NR_BASE_CGC,
  FILIAL_CGC.NR_ESTAB_FILIAL_CGC,
  NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_SERIE,
  NOTA_FISCAL_SAIDA_ACM.CD_SEQUENCIA_HISTORICO,
  LIVRO_ENTRADA.CD_EMPRESA_DESTINO,
  LIVRO_ENTRADA.CD_FILIAL_DESTINO,
  NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE,
  NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE,
  NOTA_FISCAL_SAIDA_ACM.ID_ACAO,
  NOTA_FISCAL_SAIDA_ACM.CD_PLACA_CAVALO,
  NOTA_FISCAL_SAIDA_ACM.QT_PESO_LIQUIDO,
  NOTA_FISCAL_SAIDA_ACM.QT_PESO_BRUTO,
  NOTA_FISCAL_SAIDA_ACM.DT_CANCELAMENTO_NOTA,
  NOTA_FISCAL_SAIDA_ACM.CD_MOTIVO_CANCELAMENTO,
  NOTA_FISCAL_SAIDA_ACM.CD_NFS_INTERMEDIARIO,
  NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL_REFATURADA,
  NOTA_FISCAL_SAIDA_ACM.CD_TIPO_LOJA_CLIENTE,
  NOTA_FISCAL_SAIDA_ACM.CD_NATUREZA_OPERACAO,
  NOTA_FISCAL_SAIDA_ACM.ID_TIPO_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_OPERACAO_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_VIAGEM,
  NOTA_FISCAL_SAIDA_ACM.CD_MEIO_TRANSPORTE,
  MEIO_TRANSPORTE.NM_MEIO,
  VIAGEM_TRANSPORTE.CD_OPERACAO_TRANSPORTE,
  OPERACAO_TRANSPORTE.NM_OPERACAO_TRANSPORTE,
  OPERACAO_TRANSPORTE.ID_CABOTAGEM,
  NOTA_FISCAL_SAIDA_ACM.CD_PLACA,
  NOTA_FISCAL_SAIDA_ACM.DT_EMISSAO,
  NOTA_FISCAL_SAIDA_ACM.DT_PROCESSO,
  NOTA_FISCAL_SAIDA_ACM.CD_PEDIDO,
  NOTA_FISCAL_SAIDA_ACM.VL_TOTAL_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.DT_VENCIMENTO,
  NOTA_FISCAL_SAIDA_ACM.DT_PREVISAO_CHEGADA,
  NOTA_FISCAL_SAIDA_ACM.DT_ATUALIZACAO,
  NOTA_FISCAL_SAIDA_ACM.CD_ENTRADA_SAIDA,
  NOTA_FISCAL_SAIDA_ACM.NR_SEQUENCIA_VIAGEM,
  NOTA_FISCAL_SAIDA_ACM.DT_SAIDA,
  DATE(LIVRO_ENTRADA.DT_ENTRADA) AS DT_ENTRADA,
  DATE_DIFF(DATE(LIVRO_ENTRADA.DT_ENTRADA), NOTA_FISCAL_SAIDA_ACM.DT_SAIDA, DAY) AS LEAD_TIME
FROM 
  NOTA_FISCAL_SAIDA_ACM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
ON
  EMPRESA.CD_EMPRESA = NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA
LEFT JOIN 
  (
    SELECT
      *
    FROM
      `seara-analytics-prod.stg_erp_seara.filial_cgc` 
    WHERE
      DT_FIM_ATIVIDADE IS NULL
  ) FILIAL_CGC 
ON
  FILIAL_CGC.CD_EMPRESA = NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA
  AND FILIAL_CGC.CD_FILIAL = NOTA_FISCAL_SAIDA_ACM.CD_FILIAL
LEFT JOIN 
  LIVRO_ENTRADA 
ON
  LIVRO_ENTRADA.NR_BASE_CGC = NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE
  AND LIVRO_ENTRADA.NR_ESTAB_FILIAL_CGC = NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE
  AND LIVRO_ENTRADA.CD_BASE_PESSOA_JURIDICA = EMPRESA.NR_BASE_CGC
  AND LIVRO_ENTRADA.CD_ESTAB_PESSOA_JURIDICA = FILIAL_CGC.NR_ESTAB_FILIAL_CGC
  AND LIVRO_ENTRADA.CD_NOTA_FISCAL = NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL
  AND LIVRO_ENTRADA.CD_SERIE = NOTA_FISCAL_SAIDA_ACM.CD_SERIE
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.meio_transporte` MEIO_TRANSPORTE 
ON
  NOTA_FISCAL_SAIDA_ACM.CD_MEIO_TRANSPORTE = MEIO_TRANSPORTE.CD_MEIO_TRANSPORTE
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.viagem_transporte` VIAGEM_TRANSPORTE 
ON
  VIAGEM_TRANSPORTE.CD_VIAGEM_TRANSPORTE = NOTA_FISCAL_SAIDA_ACM.CD_VIAGEM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.operacao_transporte` OPERACAO_TRANSPORTE 
ON
  OPERACAO_TRANSPORTE.CD_OPERACAO_TRANSPORTE = VIAGEM_TRANSPORTE.CD_OPERACAO_TRANSPORTE
WHERE
  NOTA_FISCAL_SAIDA_ACM.DT_SAIDA between DATE_SUB(CURRENT_DATE('America/Sao_Paulo'), INTERVAL 90 DAY) and CURRENT_DATE('America/Sao_Paulo')
--   DATE(LIVRO_ENTRADA.DT_ENTRADA) IS NOT NULL
--   AND NOTA_FISCAL_SAIDA_ACM.CD_FILIAL = 4001
--   AND CD_FILIAL_DESTINO = 910
--   NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL = 450843
  -- AND NOTA_FISCAL_SAIDA_ACM.CD_FILIAL = 704
  -- AND NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE = 2914460
  -- AND NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE = 191
"""

df_lead_time_analitico = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_lead_time_analitico = df_lead_time_analitico.join(
  df_troca_nota.withColumnRenamed('NF_TROCA', 'CD_NOTA_FISCAL') \
                .withColumnRenamed('cd_empresa_venda', 'CD_EMPRESA_ORIGEM') \
                .withColumnRenamed('cd_filial_venda', 'CD_FILIAL_ORIGEM') \
                .withColumnRenamed('CD_EMP_TROCA_TROCA', 'CD_EMPRESA_DESTINO') \
                .withColumnRenamed('CD_FIL_TROCA_TROCA', 'CD_FILIAL_DESTINO') 
                ,
  how = 'left',
  on = ['CD_NOTA_FISCAL', 'CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM', 'CD_EMPRESA_DESTINO', 'CD_FILIAL_DESTINO']
)

# COMMAND ----------

df_lead_time_analitico = df_lead_time_analitico.join(
  df_lead_time_analitico.select('CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM', 'CD_EMPRESA_DESTINO', 'CD_FILIAL_DESTINO', 'CD_NOTA_FISCAL', 'DT_SAIDA') \
                      .withColumnRenamed('CD_EMPRESA_ORIGEM', 'cd_empresa_transf')\
                      .withColumnRenamed('CD_FILIAL_ORIGEM', 'cd_filial_transf')\
                      .withColumnRenamed('CD_NOTA_FISCAL', 'NOTA_TRANSF')\
                      .withColumnRenamed('CD_EMPRESA_DESTINO', 'CD_EMPRESA_ORIGEM')\
                      .withColumnRenamed('CD_FILIAL_DESTINO', 'CD_FILIAL_ORIGEM')\
                      .withColumnRenamed('DT_SAIDA', 'NOVO_DT_SAIDA')
  ,
  how = 'left',
  on = ['cd_empresa_transf', 'cd_filial_transf', 'NOTA_TRANSF', 'CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM']
)

# COMMAND ----------

df_lead_time_analitico = df_lead_time_analitico.withColumn(
  'CD_EMPRESA_ORIGEM', coalesce(col('cd_empresa_transf'), col('CD_EMPRESA_ORIGEM'))
).withColumn(
  'CD_FILIAL_ORIGEM', coalesce(col('cd_filial_transf'), col('CD_FILIAL_ORIGEM'))
).withColumn(
  'CD_NOTA_FISCAL', coalesce(col('NOTA_TRANSF'), col('CD_NOTA_FISCAL'))
).withColumn(
  'DT_SAIDA', coalesce(col('NOVO_DT_SAIDA'), col('DT_SAIDA'))
)

df_lead_time_analitico = df_lead_time_analitico.withColumn(
  'DS_TROCA_NOTA', when(col('NOTA_TRANSF').isNull(), lit('0 a 1 troca de nota')).otherwise(lit('2 trocas de nota'))
)


df_lead_time_analitico = df_lead_time_analitico.drop(
  'cd_empresa_transf',
  'cd_filial_transf',
  'NOTA_TRANSF',
  'NOVO_DT_SAIDA'
)

df_lead_time_analitico = df_lead_time_analitico.withColumn(
  'LEAD_TIME', datediff(col('DT_ENTRADA'),col('DT_SAIDA'))
)

# COMMAND ----------

# df_lead_time_analitico.filter(
#   (col('CD_NOTA_FISCAL') == 24988)
#   &(col('CD_EMPRESA_DESTINO') == 178)
#   &(col('CD_FILIAL_DESTINO') == 676)
# ).display()

# COMMAND ----------

df_lead_time_analitico.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.lead_time_transferencia').save()

# COMMAND ----------

query = """
WITH BASE_LEAD_TIMES AS (
SELECT
  CD_EMPRESA_ORIGEM,
  CD_FILIAL_ORIGEM,
  CD_EMPRESA_DESTINO,
  CD_FILIAL_DESTINO,
  -- NM_MEIO,
  NM_OPERACAO_TRANSPORTE,
  DS_TROCA_NOTA,
  LEAD_TIME,
  PERCENTILE_DISC(LEAD_TIME, 0.5) OVER (
      PARTITION BY CD_EMPRESA_ORIGEM,
                    CD_FILIAL_ORIGEM,
                    CD_EMPRESA_DESTINO,
                    CD_FILIAL_DESTINO,
                    NM_OPERACAO_TRANSPORTE,
                    DS_TROCA_NOTA) AS LEAD_TIME_MEDIANO,
  STDDEV(LEAD_TIME) OVER (
      PARTITION BY CD_EMPRESA_ORIGEM,
                    CD_FILIAL_ORIGEM,
                    CD_EMPRESA_DESTINO,
                    CD_FILIAL_DESTINO,
                    NM_OPERACAO_TRANSPORTE,
                    DS_TROCA_NOTA) AS DESVPAD_LEAD_TIME
FROM
  `mrp-ia.estoque_ideal_produto_acabado.lead_time_transferencia`
)

SELECT
  CONCAT(CD_FILIAL_ORIGEM,' - ',CD_FILIAL_DESTINO) AS CHAVE_FILIAL_ORIGEM_DESTINO, 
  CD_EMPRESA_ORIGEM,
  CD_FILIAL_ORIGEM,
  CONCAT(CD_FILIAL_ORIGEM,' - ',FILIAL_ORIGEM.NM_FILIAL) AS NM_FILIAL_ORIGEM,
  CD_EMPRESA_DESTINO,
  CD_FILIAL_DESTINO,
  CONCAT(CD_FILIAL_DESTINO,' - ',FILIAL_DESTINO.NM_FILIAL) AS NM_FILIAL_DESTINO,
  -- NM_MEIO,
  NM_OPERACAO_TRANSPORTE,
  DS_TROCA_NOTA,
  AVG(LEAD_TIME_MEDIANO) AS LEAD_TIME_MEDIANO,
  ROUND(AVG(LEAD_TIME),2) AS LEAD_TIME_MEDIO,
  CEIL(AVG(LEAD_TIME)) AS LEAD_TIME_MEDIO_TETO,
  ROUND(AVG(DESVPAD_LEAD_TIME),2) AS DESVPAD_LEAD_TIME,
  COUNT(*) AS NUM_VIAGENS
FROM  
  BASE_LEAD_TIMES
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_ORIGEM 
ON
  FILIAL_ORIGEM.CD_EMPRESA = BASE_LEAD_TIMES.CD_EMPRESA_ORIGEM
  AND FILIAL_ORIGEM.CD_FILIAL = BASE_LEAD_TIMES.CD_FILIAL_ORIGEM
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_DESTINO
ON
  FILIAL_DESTINO.CD_EMPRESA = BASE_LEAD_TIMES.CD_EMPRESA_DESTINO
  AND FILIAL_DESTINO.CD_FILIAL = BASE_LEAD_TIMES.CD_FILIAL_DESTINO
WHERE
  CD_FILIAL_DESTINO IS NOT NULL
GROUP BY 
  CD_EMPRESA_ORIGEM,
  CD_FILIAL_ORIGEM,
  FILIAL_ORIGEM.NM_FILIAL,
  CD_EMPRESA_DESTINO,
  CD_FILIAL_DESTINO,
  FILIAL_DESTINO.NM_FILIAL,
  -- NM_MEIO,
  NM_OPERACAO_TRANSPORTE,
  DS_TROCA_NOTA
"""

df_lead_time = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_lead_time.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.view_lead_time_transferencia').save()

# COMMAND ----------

df_lead_time = df_lead_time.groupBy(
  'CD_FILIAL_ORIGEM',
  'CD_FILIAL_DESTINO',
  'NM_OPERACAO_TRANSPORTE',
  'DS_TROCA_NOTA'
).agg(
  F.avg('LEAD_TIME_MEDIANO').alias('LEAD_TIME_MEDIANO'),
  F.avg('DESVPAD_LEAD_TIME').alias('DESVPAD_LEAD_TIME'),
  F.sum('NUM_VIAGENS').alias('NUM_VIAGENS')
).orderBy(
  col('NUM_VIAGENS').desc()
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_lead_time.createOrReplaceTempView("df")

df_lead_time = spark.sql(
  """
    SELECT 
        *,
       row_number() OVER (PARTITION BY CD_FILIAL_ORIGEM, CD_FILIAL_DESTINO ORDER BY NUM_VIAGENS DESC) AS rn
     FROM 
       df
   """
)

# COMMAND ----------

df_lead_time = df_lead_time.filter(
  col('rn') == 1
).select('CD_FILIAL_ORIGEM', 'CD_FILIAL_DESTINO', 'LEAD_TIME_MEDIANO', 'DESVPAD_LEAD_TIME')

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Obtendo a filial de abastecimento de cada filial

# COMMAND ----------

query = """
WITH LIVRO_ENTRADA AS ( 
  SELECT
    LIVRO_FISCAL_ENTRADA.CD_BASE_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_ESTAB_PESSOA_JURIDICA,
    LIVRO_FISCAL_ENTRADA.CD_NOTA_FISCAL,
    LIVRO_FISCAL_ENTRADA.CD_SERIE,
    LIVRO_FISCAL_ENTRADA.CD_EMPRESA AS CD_EMPRESA_DESTINO,
    LIVRO_FISCAL_ENTRADA.CD_FILIAL AS CD_FILIAL_DESTINO,
    EMPRESA.NR_BASE_CGC,
    FILIAL_CGC.NR_ESTAB_FILIAL_CGC,
    LIVRO_FISCAL_ENTRADA.CD_ITEM,
    LIVRO_FISCAL_ENTRADA.QT_ITEM_NFI,
    PARSE_DATETIME('%d/%m/%Y %H:%M:%S', LIVRO_FISCAL_ENTRADA.DT_ENTRADA_SAIDA_NFI) AS DT_ENTRADA
  FROM
    `seara-analytics-prod.stg_erp_seara.livro_fiscal_entrada` LIVRO_FISCAL_ENTRADA
  LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
  ON
    EMPRESA.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
  LEFT JOIN 
    (
      SELECT
        *
      FROM
        `seara-analytics-prod.stg_erp_seara.filial_cgc` 
      WHERE
        DT_FIM_ATIVIDADE IS NULL
    ) FILIAL_CGC 
  ON
    FILIAL_CGC.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
    AND FILIAL_CGC.CD_FILIAL = LIVRO_FISCAL_ENTRADA.CD_FILIAL
--   GROUP BY 
--     LIVRO_FISCAL_ENTRADA.CD_BASE_PESSOA_JURIDICA,
--     LIVRO_FISCAL_ENTRADA.CD_ESTAB_PESSOA_JURIDICA,
--     LIVRO_FISCAL_ENTRADA.CD_NOTA_FISCAL,
--     LIVRO_FISCAL_ENTRADA.CD_SERIE,
--     LIVRO_FISCAL_ENTRADA.CD_EMPRESA,
--     LIVRO_FISCAL_ENTRADA.CD_FILIAL,
--     EMPRESA.NR_BASE_CGC,
--     FILIAL_CGC.NR_ESTAB_FILIAL_CGC
)


SELECT
  NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA AS CD_EMPRESA_ORIGEM,
  NOTA_FISCAL_SAIDA_ACM.CD_FILIAL AS CD_FILIAL_ORIGEM,
  EMPRESA.NR_BASE_CGC,
  FILIAL_CGC.NR_ESTAB_FILIAL_CGC,
  NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_SERIE,
  NOTA_FISCAL_SAIDA_ACM.CD_SEQUENCIA_HISTORICO,
  LIVRO_ENTRADA.CD_EMPRESA_DESTINO,
  LIVRO_ENTRADA.CD_FILIAL_DESTINO,
  NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE,
  NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE,
  LIVRO_ENTRADA.CD_ITEM,
  LIVRO_ENTRADA.QT_ITEM_NFI,
  NOTA_FISCAL_SAIDA_ACM.ID_ACAO,
  NOTA_FISCAL_SAIDA_ACM.CD_VIAGEM,
  NOTA_FISCAL_SAIDA_ACM.CD_PLACA_CAVALO,
  NOTA_FISCAL_SAIDA_ACM.QT_PESO_LIQUIDO,
  NOTA_FISCAL_SAIDA_ACM.QT_PESO_BRUTO,
  NOTA_FISCAL_SAIDA_ACM.DT_CANCELAMENTO_NOTA,
  NOTA_FISCAL_SAIDA_ACM.CD_MOTIVO_CANCELAMENTO,
  NOTA_FISCAL_SAIDA_ACM.CD_NFS_INTERMEDIARIO,
  NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL_REFATURADA,
  NOTA_FISCAL_SAIDA_ACM.CD_TIPO_LOJA_CLIENTE,
  NOTA_FISCAL_SAIDA_ACM.CD_NATUREZA_OPERACAO,
  NOTA_FISCAL_SAIDA_ACM.ID_TIPO_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_OPERACAO_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.CD_PLACA,
  NOTA_FISCAL_SAIDA_ACM.DT_EMISSAO,
  NOTA_FISCAL_SAIDA_ACM.DT_PROCESSO,
  NOTA_FISCAL_SAIDA_ACM.CD_PEDIDO,
  NOTA_FISCAL_SAIDA_ACM.VL_TOTAL_NOTA_FISCAL,
  NOTA_FISCAL_SAIDA_ACM.DT_VENCIMENTO,
  NOTA_FISCAL_SAIDA_ACM.DT_PREVISAO_CHEGADA,
  NOTA_FISCAL_SAIDA_ACM.DT_ATUALIZACAO,
  NOTA_FISCAL_SAIDA_ACM.CD_ENTRADA_SAIDA,
  NOTA_FISCAL_SAIDA_ACM.NR_SEQUENCIA_VIAGEM,
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', NOTA_FISCAL_SAIDA_ACM.DT_ENTRADA_SAIDA)) AS DT_SAIDA,
  DATE(LIVRO_ENTRADA.DT_ENTRADA) AS DT_ENTRADA,
  DATE_DIFF(DATE(LIVRO_ENTRADA.DT_ENTRADA), DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', NOTA_FISCAL_SAIDA_ACM.DT_ENTRADA_SAIDA)), DAY) AS LEAD_TIME
FROM 
  `seara-analytics-prod.stg_erp_seara.nota_fiscal_saida_acm` NOTA_FISCAL_SAIDA_ACM
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
ON
  EMPRESA.CD_EMPRESA = NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA
LEFT JOIN 
  (
    SELECT
      *
    FROM
      `seara-analytics-prod.stg_erp_seara.filial_cgc` 
    WHERE
      DT_FIM_ATIVIDADE IS NULL
  ) FILIAL_CGC 
ON
  FILIAL_CGC.CD_EMPRESA = NOTA_FISCAL_SAIDA_ACM.CD_EMPRESA
  AND FILIAL_CGC.CD_FILIAL = NOTA_FISCAL_SAIDA_ACM.CD_FILIAL
LEFT JOIN 
  LIVRO_ENTRADA 
ON
  LIVRO_ENTRADA.NR_BASE_CGC = NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE
  AND LIVRO_ENTRADA.NR_ESTAB_FILIAL_CGC = NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE
  AND LIVRO_ENTRADA.CD_BASE_PESSOA_JURIDICA = EMPRESA.NR_BASE_CGC
  AND LIVRO_ENTRADA.CD_ESTAB_PESSOA_JURIDICA = FILIAL_CGC.NR_ESTAB_FILIAL_CGC
  AND LIVRO_ENTRADA.CD_NOTA_FISCAL = NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL
  AND LIVRO_ENTRADA.CD_SERIE = NOTA_FISCAL_SAIDA_ACM.CD_SERIE
WHERE
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', NOTA_FISCAL_SAIDA_ACM.DT_ENTRADA_SAIDA)) between DATE_SUB(CURRENT_DATE('America/Sao_Paulo'), INTERVAL 90 DAY) and CURRENT_DATE('America/Sao_Paulo')
--   AND DATE(LIVRO_ENTRADA.DT_ENTRADA) IS NOT NULL
--   AND NOTA_FISCAL_SAIDA_ACM.CD_FILIAL = 4001
--   AND CD_FILIAL_DESTINO = 910
--   AND NOTA_FISCAL_SAIDA_ACM.CD_NOTA_FISCAL = 450843
--   AND NOTA_FISCAL_SAIDA_ACM.CD_FILIAL = 704
--   AND NOTA_FISCAL_SAIDA_ACM.CD_BASE_CLIENTE = 2914460
--   AND NOTA_FISCAL_SAIDA_ACM.CD_LOJA_CLIENTE = 191
"""

df_filial_abastecimento_original = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_filial_abastecimento_original = df_filial_abastecimento_original.join(
  df_troca_nota.withColumnRenamed('NF_TROCA', 'CD_NOTA_FISCAL') \
                .withColumnRenamed('cd_empresa_venda', 'CD_EMPRESA_ORIGEM') \
                .withColumnRenamed('cd_filial_venda', 'CD_FILIAL_ORIGEM') \
                .withColumnRenamed('CD_EMP_TROCA_TROCA', 'CD_EMPRESA_DESTINO') \
                .withColumnRenamed('CD_FIL_TROCA_TROCA', 'CD_FILIAL_DESTINO') 
                ,
  how = 'left',
  on = ['CD_NOTA_FISCAL', 'CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM', 'CD_EMPRESA_DESTINO', 'CD_FILIAL_DESTINO']
)

df_filial_abastecimento_original = df_filial_abastecimento_original.join(
  df_filial_abastecimento_original.select('CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM', 'CD_EMPRESA_DESTINO', 'CD_FILIAL_DESTINO', 'CD_NOTA_FISCAL', 'DT_SAIDA') \
                      .withColumnRenamed('CD_EMPRESA_ORIGEM', 'cd_empresa_transf')\
                      .withColumnRenamed('CD_FILIAL_ORIGEM', 'cd_filial_transf')\
                      .withColumnRenamed('CD_NOTA_FISCAL', 'NOTA_TRANSF')\
                      .withColumnRenamed('CD_EMPRESA_DESTINO', 'CD_EMPRESA_ORIGEM')\
                      .withColumnRenamed('CD_FILIAL_DESTINO', 'CD_FILIAL_ORIGEM')\
                      .withColumnRenamed('DT_SAIDA', 'NOVO_DT_SAIDA')
  ,
  how = 'left',
  on = ['cd_empresa_transf', 'cd_filial_transf', 'NOTA_TRANSF', 'CD_EMPRESA_ORIGEM', 'CD_FILIAL_ORIGEM']
)

df_filial_abastecimento_original = df_filial_abastecimento_original.withColumn(
  'CD_EMPRESA_ORIGEM', coalesce(col('cd_empresa_transf'), col('CD_EMPRESA_ORIGEM'))
).withColumn(
  'CD_FILIAL_ORIGEM', coalesce(col('cd_filial_transf'), col('CD_FILIAL_ORIGEM'))
).withColumn(
  'CD_NOTA_FISCAL', coalesce(col('NOTA_TRANSF'), col('CD_NOTA_FISCAL'))
).withColumn(
  'DT_SAIDA', coalesce(col('NOVO_DT_SAIDA'), col('DT_SAIDA'))
)

df_filial_abastecimento_original = df_filial_abastecimento_original.withColumn(
  'DS_TROCA_NOTA', when(col('NOTA_TRANSF').isNull(), lit('0 a 1 troca de nota')).otherwise(lit('2 trocas de nota'))
)


df_filial_abastecimento_original = df_filial_abastecimento_original.drop(
  'cd_empresa_transf',
  'cd_filial_transf',
  'NOTA_TRANSF',
  'NOVO_DT_SAIDA'
)

df_filial_abastecimento_original = df_filial_abastecimento_original.withColumn(
  'LEAD_TIME', datediff(col('DT_ENTRADA'),col('DT_SAIDA'))
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Por item em cada filial

# COMMAND ----------

df_filial_abastecimento_item = df_filial_abastecimento_original.groupBy(
 'CD_FILIAL_ORIGEM',
 'CD_FILIAL_DESTINO',
 'DS_TROCA_NOTA',
 'CD_ITEM'
).agg(
  F.sum('QT_ITEM_NFI').alias('QT_ITEM')
).orderBy(
  asc('CD_FILIAL_ORIGEM'),
  asc('CD_FILIAL_DESTINO'),
  asc('CD_ITEM'),
  desc('QT_ITEM')
)

spark.catalog.dropTempView("df")

df_filial_abastecimento_item.createOrReplaceTempView("df")

df_filial_abastecimento_item = spark.sql(
  """
    SELECT 
        *,
        ROW_NUMBER() OVER (PARTITION BY CD_FILIAL_DESTINO , CD_ITEM ORDER BY CD_FILIAL_DESTINO, CD_ITEM, QT_ITEM DESC) AS rn
     FROM 
       df
   """
)

df_itens_com_troca_de_nota_predominante = df_filial_abastecimento_item.filter(
  (col('DS_TROCA_NOTA') == '2 trocas de nota')
  &(col('rn') == 1)
).select('CD_FILIAL_ORIGEM', 'CD_FILIAL_DESTINO', 'CD_ITEM')

df_filial_abastecimento_item = df_filial_abastecimento_item.filter(
  col('rn') == 1
).select(
  'CD_FILIAL_ORIGEM',
  'CD_FILIAL_DESTINO',
  'CD_ITEM'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Por filial

# COMMAND ----------

df_filial_abastecimento_filial = df_filial_abastecimento_original.groupBy(
 'CD_FILIAL_ORIGEM',
 'CD_FILIAL_DESTINO',
 'DS_TROCA_NOTA'
).agg(
  F.sum('QT_ITEM_NFI').alias('QT_ITEM')
).orderBy(
  asc('CD_FILIAL_ORIGEM'),
  asc('CD_FILIAL_DESTINO'),
  desc('QT_ITEM')
)

spark.catalog.dropTempView("df")

df_filial_abastecimento_filial.createOrReplaceTempView("df")

df_filial_abastecimento_filial = spark.sql(
  """
    SELECT 
        *,
        ROW_NUMBER() OVER (PARTITION BY CD_FILIAL_DESTINO ORDER BY CD_FILIAL_DESTINO, QT_ITEM DESC) AS rn
     FROM 
       df
   """
)

# df_filiais_com_troca_de_nota_predominante = df_filial_abastecimento_filial.filter(
#   (col('DS_TROCA_NOTA') == '2 trocas de nota')
#   &(col('rn') == 1)
# ).select('CD_FILIAL_ORIGEM', 'CD_FILIAL_DESTINO')

df_filial_abastecimento_filial = df_filial_abastecimento_filial.filter(
  col('rn') == 1
).select(
  'CD_FILIAL_ORIGEM',
  'CD_FILIAL_DESTINO'
)

# COMMAND ----------

df_filial_abastecimento_filial.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.filial_abastecedora_das_filiais').save()

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Obtendo a filial de abastecimento de cada PDV

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Por pdv e item

# COMMAND ----------

query = f"""
WITH 

TABELA_ORIGINAL AS (

SELECT
  *
FROM
  `pricing-seara.comercial.base_comercial`
WHERE
  LENGTH(CAST(DT_ANOMES AS STRING)) = 6
),

TABELA AS (

SELECT
  CAST(SPLIT(CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(1)] AS INT) AS CD_BASE_CLIENTE,
  CAST(SPLIT(CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(2)] AS INT) AS CD_LOJA_CLIENTE,
  CD_ITEM,
  CD_FILIAL,
  SUM(VOLUME_REAL) AS VOLUME_REAL
FROM
  TABELA_ORIGINAL
WHERE
  CD_BASE_LOJA_CLIENTE IS NOT NULL
  AND CD_FILIAL IS NOT NULL
  AND PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING)) >= DATE_SUB(
                                                           DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), 
                                                           INTERVAL 90 DAY)
GROUP BY 
  CD_BASE_LOJA_CLIENTE,
  CD_FILIAL,
  CD_ITEM
ORDER BY
  CD_BASE_CLIENTE,
  CD_LOJA_CLIENTE,
  CD_ITEM,
  VOLUME_REAL DESC

)

SELECT 
  *,
  ROW_NUMBER()  OVER ( 
      PARTITION BY 
        CD_BASE_CLIENTE, 
        CD_LOJA_CLIENTE, 
        CD_ITEM 
      ORDER BY
        CD_BASE_CLIENTE, 
        CD_LOJA_CLIENTE, 
        CD_ITEM,
        VOLUME_REAL DESC
    ) as rn
FROM 
  TABELA
WHERE
  TABELA.CD_ITEM IN ({str_lista_itens_ind})
ORDER BY
  CD_BASE_CLIENTE, 
  CD_LOJA_CLIENTE, 
  CD_ITEM,
  VOLUME_REAL DESC
"""

df_filial_pdv_item = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_filial_pdv_item = df_filial_pdv_item.withColumnRenamed('CD_FILIAL','CD_FILIAL_PDV_ITEM')

# COMMAND ----------

# df_filial_pdv_item = df_filial_pdv_item.join(
#   df_itens_com_troca_de_nota_predominante.withColumnRenamed('CD_FILIAL_DESTINO', 'CD_FILIAL_PDV_ITEM'),
#   how = 'left',
#   on = ['CD_FILIAL_PDV_ITEM', 'CD_ITEM']
# ).withColumn('CD_FILIAL_PDV_ITEM', coalesce(col('CD_FILIAL_ORIGEM'), col('CD_FILIAL_PDV_ITEM'))).drop('CD_FILIAL_ORIGEM')

# COMMAND ----------

df_filial_pdv_item.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.filial_abastecedora_pdv_item').save()

# COMMAND ----------

# Para cada item, em cada cliente. a filial que abasteceu o cliente com o maior volume nos últimos 90 dias será considerada como a filial que o abastecerá no futuro

df_filial_pdv_item = df_filial_pdv_item.filter(
  col('rn') == 1
).select(
  'CD_BASE_CLIENTE',
  'CD_LOJA_CLIENTE',
  'CD_ITEM',
  'CD_FILIAL_PDV_ITEM'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Por município e item

# COMMAND ----------

query = f"""
WITH 

TABELA_ORIGINAL AS (
 
SELECT
  *
FROM
  `pricing-seara.comercial.base_comercial`
WHERE
  LENGTH(CAST(DT_ANOMES AS STRING)) = 6

),

TABELA AS (

SELECT
  NM_LOCALIDADE,
  CD_ITEM,
  CD_FILIAL,
  SUM(VOLUME_REAL) AS VOLUME_REAL
FROM
  TABELA_ORIGINAL
WHERE
  CD_BASE_LOJA_CLIENTE IS NOT NULL
  AND CD_FILIAL IS NOT NULL
  AND PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING)) >= DATE_SUB(
                                                           DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), 
                                                           INTERVAL 90 DAY)
GROUP BY 
  NM_LOCALIDADE,
  CD_ITEM,
  CD_FILIAL
ORDER BY
  NM_LOCALIDADE,
  CD_ITEM,
  VOLUME_REAL DESC

)

SELECT 
  *,
  ROW_NUMBER()  OVER ( 
      PARTITION BY 
        NM_LOCALIDADE,
        CD_ITEM
      ORDER BY
        NM_LOCALIDADE,
        CD_ITEM, 
        VOLUME_REAL DESC
    ) as rn
FROM 
  TABELA
WHERE
  CD_ITEM IN ({str_lista_itens_ind})
ORDER BY
  NM_LOCALIDADE,
  CD_ITEM, 
  VOLUME_REAL DESC
"""

df_filial_municipio_item = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_filial_municipio_item = df_filial_municipio_item.withColumnRenamed('CD_FILIAL','CD_FILIAL_MUNICIPIO_ITEM')

# COMMAND ----------

# df_filial_municipio_item = df_filial_municipio_item.join(
#   df_itens_com_troca_de_nota_predominante.withColumnRenamed('CD_FILIAL_DESTINO', 'CD_FILIAL_MUNICIPIO_ITEM'),
#   how = 'left',
#   on = ['CD_FILIAL_MUNICIPIO_ITEM', 'CD_ITEM']
# ).withColumn('CD_FILIAL_MUNICIPIO_ITEM', coalesce(col('CD_FILIAL_ORIGEM'), col('CD_FILIAL_MUNICIPIO_ITEM'))).drop('CD_FILIAL_ORIGEM')

# COMMAND ----------

df_filial_municipio_item.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.filial_abastecedora_municipio_item').save()

# COMMAND ----------

# Para cada item, em cada cliente. a filial que abasteceu o municipio com o maior volume nos últimos 90 dias será considerada como a filial que o abastecerá no futuro

df_filial_municipio_item = df_filial_municipio_item.filter(
  col('rn') == 1
).select(
  'NM_LOCALIDADE',
  'CD_ITEM',
  'CD_FILIAL_MUNICIPIO_ITEM'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Por município

# COMMAND ----------

query = f"""
WITH 

TABELA_ORIGINAL AS (

SELECT
  *
FROM
  `pricing-seara.comercial.base_comercial`
WHERE
  LENGTH(CAST(DT_ANOMES AS STRING)) = 6

),

TABELA AS (

SELECT
  NM_LOCALIDADE,
  CD_FILIAL,
  SUM(VOLUME_REAL) AS VOLUME_REAL
FROM
  TABELA_ORIGINAL
WHERE
  CD_BASE_LOJA_CLIENTE IS NOT NULL
  AND CD_FILIAL IS NOT NULL
  AND PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING)) >= DATE_SUB(
                                                           DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), 
                                                           INTERVAL 90 DAY)
GROUP BY 
  NM_LOCALIDADE,
  CD_FILIAL
ORDER BY
  NM_LOCALIDADE,
  VOLUME_REAL DESC

)

SELECT 
  *,
  ROW_NUMBER()  OVER ( 
      PARTITION BY 
        NM_LOCALIDADE
      ORDER BY
        NM_LOCALIDADE,
        VOLUME_REAL DESC
    ) as rn
FROM 
  TABELA
ORDER BY
  NM_LOCALIDADE,
  VOLUME_REAL DESC
"""

df_filial_municipio = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_filial_municipio = df_filial_municipio.withColumnRenamed('CD_FILIAL','CD_FILIAL_MUNICIPIO')

# COMMAND ----------

df_filial_municipio.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.filial_abastecedora_municipio').save()

# COMMAND ----------

# Para cada cliente. a filial que abasteceu o municipio com o maior volume nos últimos 90 dias será considerada como a filial que o abastecerá no futuro

df_filial_municipio = df_filial_municipio.filter(
  col('rn') == 1
).select(
  'NM_LOCALIDADE',
  'CD_FILIAL_MUNICIPIO'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Histórico de demanda

# COMMAND ----------

query = f"""

WITH TABELA AS (

SELECT
  *
FROM
  `pricing-seara.comercial.base_comercial`
WHERE
  LENGTH(CAST(DT_ANOMES AS STRING)) = 6

)

SELECT
  PARSE_DATE('%Y%m', CAST(BASE_COMERCIAL.DT_ANOMES AS STRING)) AS MES_REF,
  CAST(SPLIT(BASE_COMERCIAL.CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(1)] AS INT) AS CD_BASE_CLIENTE,
  CAST(SPLIT(BASE_COMERCIAL.CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(2)] AS INT) AS CD_LOJA_CLIENTE,
  BASE_COMERCIAL.NM_LOCALIDADE,
  BASE_COMERCIAL.CD_FILIAL,
  BASE_COMERCIAL.CD_ITEM,
  SUM(BASE_COMERCIAL.VOLUME_REAL) AS VOLUME_REAL,
  ROUND(SUM(BASE_COMERCIAL.VOLUME_REAL)/{num_dias_diarizacao},2) AS VOLUME_REAL_DIARIZADO
FROM
  TABELA AS BASE_COMERCIAL
LEFT JOIN
  (
     WITH BASE AS (
 
       SELECT
          *,
          ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_ATUALIZACAO) DESC) as rn
        FROM 
          `seara-analytics-prod.stg_erp_seara.filial_cgc` 
        WHERE 
          DT_FIM_ATIVIDADE IS NULL    
      )

      SELECT
        *
      FROM
        BASE
      WHERE
        rn = 1
  ) FILIAL_CGC
ON
  BASE_COMERCIAL.CD_FILIAL = FILIAL_CGC.CD_FILIAL
LEFT JOIN (
      SELECT
        RV_LOW_VALUE AS ID_LOGISTICA,
        RV_MEANING AS DS_LOGISTICA
      FROM
        `seara-analytics-prod.stg_erp_seara.bceg_ref_codes`
      WHERE
        RV_DOMAIN = "FILIAL_CGC.ID_LOGISTICA"
   ) REF_CODES
ON
  REF_CODES.ID_LOGISTICA = FILIAL_CGC.ID_LOGISTICA
WHERE
  PARSE_DATE('%Y%m', CAST(BASE_COMERCIAL.DT_ANOMES AS STRING)) BETWEEN DATE_SUB(DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')),EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), INTERVAL 90 DAY)
                                      AND DATE_SUB(DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')),EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), INTERVAL 1 DAY)
  AND BASE_COMERCIAL.CD_ITEM IS NOT NULL
  AND BASE_COMERCIAL.VOLUME_REAL > 0
  AND BASE_COMERCIAL.CD_ITEM IN ({str_lista_itens_ind})
GROUP BY 
  BASE_COMERCIAL.DT_ANOMES, 
  BASE_COMERCIAL.CD_BASE_LOJA_CLIENTE,
  BASE_COMERCIAL.NM_LOCALIDADE,
  BASE_COMERCIAL.CD_FILIAL,
  BASE_COMERCIAL.CD_ITEM
ORDER BY 
  DT_ANOMES, 
  CD_BASE_CLIENTE,
  CD_LOJA_CLIENTE,
  CD_ITEM
"""

df_volume_realizado = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_volume_realizado = df_volume_realizado.join(
  df_de_para_filiais.select('CD_FILIAL', 'NOVO_CD_FILIAL'),
  how = 'left',
  on = ['CD_FILIAL']
)

df_volume_realizado = df_volume_realizado.withColumn('CD_FILIAL', coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL')))

df_volume_realizado = df_volume_realizado.drop('NOVO_CD_FILIAL')

# COMMAND ----------

df_volume_realizado = df_volume_realizado.join(
  df_filiais.select('CD_FILIAL', 'ID_LOGISTICA', 'DS_LOGISTICA'),
  how = 'left',
  on = ['CD_FILIAL']
)

# COMMAND ----------

# df_volume_realizado = df_volume_realizado.join(
#   df_itens_com_troca_de_nota_predominante.withColumnRenamed('CD_FILIAL_DESTINO', 'CD_FILIAL'),
#   how = 'left',
#   on = ['CD_FILIAL', 'CD_ITEM']
# )

# df_volume_realizado = df_volume_realizado.withColumn(
#   'CD_FILIAL', coalesce(col('CD_FILIAL_ORIGEM'), col('CD_FILIAL'))
# ).drop('CD_FILIAL_ORIGEM')

# COMMAND ----------

df_volume_realizado.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.historico_volume_realizado').save()

# COMMAND ----------

df_volume_realizado = df_volume_realizado.groupBy(
  'MES_REF',
  'CD_FILIAL',
  'ID_LOGISTICA',
  'DS_LOGISTICA',
  'CD_ITEM'
).agg(
  F.sum(col('VOLUME_REAL_DIARIZADO')).alias('VOLUME_REAL_DIARIZADO'),
  F.sum(col('VOLUME_REAL')).alias('VOLUME_REAL')
)

spark.catalog.dropTempView("df")

df_volume_realizado.createOrReplaceTempView("df")

df_volume_realizado = spark.sql(
  """
    SELECT 
        *,
        ROUND(STDDEV(VOLUME_REAL_DIARIZADO) OVER (PARTITION BY CD_FILIAL, CD_ITEM),2) AS DESVPAD_VOLUME_REAL_DIARIZADO
     FROM 
       df
   """
)

df_volume_realizado = df_volume_realizado.groupBy(
  'CD_FILIAL',
  'ID_LOGISTICA',
  'DS_LOGISTICA',
  'CD_ITEM'
).agg(
  F.avg(col('VOLUME_REAL_DIARIZADO')).alias('VOLUME_REAL_DIARIZADO'),
  F.avg(col('DESVPAD_VOLUME_REAL_DIARIZADO')).alias('DESVPAD_VOLUME_REAL_DIARIZADO')
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Ciclo Abastecimento

# COMMAND ----------

query = f"""
SELECT
    LIVRO_FISCAL_ENTRADA.CD_EMPRESA AS CD_EMPRESA_DESTINO,
    LIVRO_FISCAL_ENTRADA.CD_FILIAL AS CD_FILIAL_DESTINO,
    EMPRESA.NR_BASE_CGC,
    FILIAL_CGC.NR_ESTAB_FILIAL_CGC,
    LIVRO_FISCAL_ENTRADA.CD_ITEM,
    DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', LIVRO_FISCAL_ENTRADA.DT_ENTRADA_SAIDA_NFI)) AS DT_ENTRADA,
    LIVRO_FISCAL_ENTRADA.QT_ITEM_NFI,
    SAFE_DIVIDE(LIVRO_FISCAL_ENTRADA.QT_ITEM_NFI, ITEM.QT_MAXIMA) AS QT_VOLUME,
    NATUREZA_OPERACAO.DS_NATUREZA_OPERACAO
FROM
    `seara-analytics-prod.stg_erp_seara.livro_fiscal_entrada` LIVRO_FISCAL_ENTRADA
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.empresa` EMPRESA
ON
    EMPRESA.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
LEFT JOIN 
(
    SELECT
    *
    FROM
    `seara-analytics-prod.stg_erp_seara.filial_cgc` 
    WHERE
    DT_FIM_ATIVIDADE IS NULL
) FILIAL_CGC 
ON
    FILIAL_CGC.CD_EMPRESA = LIVRO_FISCAL_ENTRADA.CD_EMPRESA
    AND FILIAL_CGC.CD_FILIAL = LIVRO_FISCAL_ENTRADA.CD_FILIAL
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.natureza_operacao` NATUREZA_OPERACAO
ON
  NATUREZA_OPERACAO.CD_NATUREZA_OPERACAO = LIVRO_FISCAL_ENTRADA.CD_NATUREZA_OPERACAO
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.item` ITEM
ON
  ITEM.CD_ITEM = LIVRO_FISCAL_ENTRADA.CD_ITEM
WHERE 
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', LIVRO_FISCAL_ENTRADA.DT_ENTRADA_SAIDA_NFI)) between DATE_SUB(CURRENT_DATE('America/Sao_Paulo'), INTERVAL 90 DAY) and CURRENT_DATE('America/Sao_Paulo')
  AND LIVRO_FISCAL_ENTRADA.CD_ITEM IN ({str_lista_itens_ind})
  AND (
      (
        (REGEXP_CONTAINS(LOWER(NATUREZA_OPERACAO.DS_NATUREZA_OPERACAO), r'venda')) 
        OR (REGEXP_CONTAINS(LOWER(NATUREZA_OPERACAO.DS_NATUREZA_OPERACAO), r'transf'))
        OR (REGEXP_CONTAINS(LOWER(NATUREZA_OPERACAO.DS_NATUREZA_OPERACAO), r'compra'))
      )
      AND (NOT REGEXP_CONTAINS(LOWER(NATUREZA_OPERACAO.DS_NATUREZA_OPERACAO), r'dev'))
  )
ORDER BY
  LIVRO_FISCAL_ENTRADA.CD_EMPRESA,
  LIVRO_FISCAL_ENTRADA.CD_FILIAL,
  LIVRO_FISCAL_ENTRADA.CD_ITEM,
  DT_ENTRADA
"""

df_base_abastecimento = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()


# Adiciona o volume historico diarizado do item para que os dias com uma quantidade de abastecimento menor do que 1 dia de volume real sejam desconsiderados.
# Esses dias com baixo volume de abastecimento são removidos na view_base_abastecimento
df_base_abastecimento = df_base_abastecimento.join(
  df_volume_realizado.select('CD_FILIAL', 'CD_ITEM', 'VOLUME_REAL_DIARIZADO').withColumnRenamed('CD_FILIAL', 'CD_FILIAL_DESTINO'),
  how = 'left',
  on = ['CD_FILIAL_DESTINO', 'CD_ITEM']
)

df_base_abastecimento.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.base_abastecimento').save()

query = """
SELECT
  *
FROM
  `mrp-ia.estoque_ideal_produto_acabado.view_base_abastecimento`
"""

df_abastecimento = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_abastecimento = df_abastecimento.groupBy(
  'CD_EMPRESA_DESTINO',
  'CD_FILIAL_DESTINO',
  'CD_ITEM'
).agg(
  F.avg(col('INTERVALO_ABASTECIMENTO_MEDIANO')).alias('INTERVALO_ABASTECIMENTO_MEDIANO'),
  F.round(F.avg(col('DESVPAD_INTERVALO_ABASTECIMENTO')),2).alias('DESVPAD_INTERVALO_ABASTECIMENTO')
).orderBy(
  asc('CD_EMPRESA_DESTINO'),
  asc('CD_FILIAL_DESTINO'),
  asc('INTERVALO_ABASTECIMENTO_MEDIANO')  
)

df_abastecimento = df_abastecimento.drop('CD_EMPRESA_DESTINO').withColumnRenamed('CD_FILIAL_DESTINO','CD_FILIAL')

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Ciclo de Produção

# COMMAND ----------

query = """

WITH 

BASE AS (

SELECT
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_PRODUCAO)) AS DT_PRODUCAO,
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  SUM(QT_PESO_PRODUTO) AS QT_PESO_PRODUTO
FROM
  `seara-analytics-prod.stg_erp_seara.apontamento_producao_cne`
WHERE
  DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_PRODUCAO)) between DATE_SUB(CURRENT_DATE('America/Sao_Paulo'), INTERVAL 90 DAY) and CURRENT_DATE('America/Sao_Paulo')
GROUP BY
  DT_PRODUCAO,
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM
ORDER BY
  DT_PRODUCAO

),

BASE_COM_QUARTIS AS (
SELECT
  *,
  PERCENTILE_CONT(QT_PESO_PRODUTO, 0.25) OVER(PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q1_QT_PESO_PRODUTO,
  PERCENTILE_CONT(QT_PESO_PRODUTO, 0.75) OVER(PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS Q3_QT_PESO_PRODUTO,
FROM
  BASE
),

BASE_SEM_OUTLIERS AS (
  SELECT
    *
  FROM
    BASE_COM_QUARTIS
  WHERE
    QT_PESO_PRODUTO >= CASE 
                          WHEN 
                            Q1_QT_PESO_PRODUTO - 1.5*(Q3_QT_PESO_PRODUTO - Q1_QT_PESO_PRODUTO) >= 0
                          THEN
                            Q1_QT_PESO_PRODUTO - 1.5*(Q3_QT_PESO_PRODUTO - Q1_QT_PESO_PRODUTO)
                          ELSE
                            0
                          END
),

BASE_COM_INTERVALO_PRODUCAO AS (

SELECT
  *,
  DATE_DIFF(
    DT_PRODUCAO, 
    LAG(DT_PRODUCAO) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY DT_PRODUCAO),
    DAY
  ) INTERVALO_PRODUCAO,
FROM
  BASE_SEM_OUTLIERS
),

BASE_COM_INTERVALO_MEDIANO AS (
SELECT
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  PERCENTILE_DISC(INTERVALO_PRODUCAO, 0.5) OVER(PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS INTERVALO_PRODUCAO_MEDIANO,
  STDDEV(INTERVALO_PRODUCAO) OVER(PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM) AS DESVPAD_INTERVALO_PRODUCAO
FROM
  BASE_COM_INTERVALO_PRODUCAO
)

SELECT
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  AVG(INTERVALO_PRODUCAO_MEDIANO) AS INTERVALO_PRODUCAO_MEDIANO,
  ROUND(AVG(DESVPAD_INTERVALO_PRODUCAO),2) AS DESVPAD_INTERVALO_PRODUCAO
FROM
  BASE_COM_INTERVALO_MEDIANO
GROUP BY
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM

"""

df_ciclo_producao = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# df_ciclo_producao.filter(
#   (col('CD_FILIAL') == 228)
#   &(col('CD_ITEM') == 9741)
# ).display()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Faturamento dia a dia

# COMMAND ----------

query = f"""

SELECT 
  A.CD_REGIONAL_VENDA,
  CANAL_VENDA_02.NM_CANAL_VENDA,
  PARSE_DATETIME('%d/%m/%Y %H:%M:%S',B.DT_PROCESSO) AS DT_PROCESSO,
  B.CD_BASE_CLIENTE,
  B.CD_EMPRESA,
  B.CD_FILIAL,
  B.CD_LOJA_CLIENTE,
  G.CD_ITEM,
  G.NM_ITEM,
  TABELA_EMPRESA.NM_CURTO AS EMPRESA,
  H.CD_MARCA_PRODUTO,
  H.NM_MARCA,
  CLASSE_ITEM.NM_CLASSE,
  G.CD_NEGOCIO,
  F.PR_VALIDADE_PRODUTO,
  G.CD_TIPO_ACLIMATACAO,
  J.NM_TIPO_ACLIMATACAO_PRO,
  A.CD_SEQ_ITEM_PED_VDA,
  A.CD_ITEM_PALLET,
  A.CD_LOCAL_ESTOQUE_ITEM,
  A.CD_NOTA_FISCAL,
  B.DT_EMISSAO, 
  B.DT_ENTRADA_SAIDA, 
  B.DT_CANCELAMENTO_NOTA, 
  B.DT_PREVISAO_CHEGADA,
  F.CD_PEDIDO_VENDA,
  F.DT_FABRICACAO_PRODUTO,
  F.DT_APROVACAO_ESTOQUE,
  F.DT_AGENDAMENTO,
  F.DT_EMBARQUE_ORIGINAL,
  F.DT_EMBARQUE_ENTRADA,
  F.DT_EMBARQUE,
  F.DT_PREVISTA_ENTREGA_ORIGINAL,
  F.DT_PREVISTA_ENTREGA,

  CASE 
      WHEN B.ID_TIPO_NOTA_FISCAL = 2 
      THEN (
              (
                  CASE 
                      WHEN B.ID_ACAO = 2 
                      THEN (A.QT_ITEM) * -1
                      ELSE A.QT_ITEM 
                      END
              )
          ) * -1 
      ELSE 
          (
              CASE 
                  WHEN B.ID_ACAO = 2 
                  THEN (A.QT_ITEM) * -1
                  ELSE A.QT_ITEM 
                  END
          )
      END AS QT_FAT,

  ROUND (
      CASE 
          WHEN B.ID_TIPO_NOTA_FISCAL = 2 
          THEN (
              CASE 
                  WHEN B.ID_ACAO = 2 
                  THEN (
                      (COALESCE(A.VL_ITEM, 0) + COALESCE(nf_swift.VL_DIFERENCA_PRECO,0))* -1
                      )
                  ELSE (COALESCE(A.VL_ITEM, 0) + COALESCE(nf_swift.VL_DIFERENCA_PRECO,0))
                  END
              ) * -1
          ELSE (
              CASE 
                  WHEN B.ID_ACAO = 2 
              THEN
                  (COALESCE(A.VL_ITEM, 0) + COALESCE(nf_swift.VL_DIFERENCA_PRECO,0))* -1
              ELSE
                  (COALESCE(A.VL_ITEM, 0) + COALESCE(nf_swift.VL_DIFERENCA_PRECO,0))
              END
          )
          END
  , 2) AS VL_FAT

FROM
  `seara-analytics-prod.stg_erp_seara.item_nota_fiscal_saida_acm` AS A

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.nota_fiscal_saida_acm` AS B
ON
  B.CD_SEQUENCIA_HISTORICO = A.CD_SEQUENCIA_HISTORICO AND
  B.CD_NOTA_FISCAL = A.CD_NOTA_FISCAL AND
  B.CD_EMPRESA = A.CD_EMPRESA AND
  B.CD_ENTRADA_SAIDA = A.CD_ENTRADA_SAIDA AND
  B.CD_FILIAL = A.CD_FILIAL AND
  B.CD_SERIE = A.CD_SERIE     

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.operacao_fiscal_faturamento` AS C
ON
  C.CD_OPERACAO_FISCAL = B.CD_OPERACAO_FISCAL

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.pedido_venda_ctr` AS D
ON
  B.CD_PEDIDO = D.CD_PEDIDO_VENDA

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.item_pedido_venda_ctr` AS F
ON
  A.CD_SEQ_ITEM_PED_VDA = F.CD_SEQ_ITEM_PED_VDA AND
  D.CD_PEDIDO_VENDA = F.CD_PEDIDO_VENDA

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.loja_cliente` AS I
ON
  I.CD_BASE_CLIENTE = B.CD_BASE_CLIENTE AND
  I.CD_LOJA_CLIENTE = B.CD_LOJA_CLIENTE

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.tipo_loja_cliente` AS TIPO_LOJA_CLIENTE_02
ON
  I.CD_TIPO_LOJA_CLIENTE = TIPO_LOJA_CLIENTE_02.CD_TIPO_LOJA_CLIENTE

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.canal_venda` AS CANAL_VENDA_02
ON
  CANAL_VENDA_02.CD_CANAL_VENDA = TIPO_LOJA_CLIENTE_02.CD_CANAL_VENDA

INNER JOIN
   `seara-analytics-prod.stg_erp_seara.item` AS G
ON
  A.CD_ITEM = G.CD_ITEM

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
  G.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM AND
  G.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM AND
  G.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.negocio_comercial` AS NEGOCIO_COMERCIAL
ON
  NEGOCIO_COMERCIAL.CD_NEGOCIO_COMERCIAL = G.CD_NEGOCIO_COMERCIAL

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.empresa` AS TABELA_EMPRESA
ON
  B.CD_EMPRESA = TABELA_EMPRESA.CD_EMPRESA

INNER JOIN
  `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` AS J
ON
  G.CD_TIPO_ACLIMATACAO = J.CD_TIPO_ACLIMATACAO_PRO

INNER JOIN 
  `seara-analytics-prod.stg_erp_seara.marca_produto` AS H
ON
  G.CD_MARCA_PRODUTO = H.CD_MARCA_PRODUTO

LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.item_nota_fiscal_dif_venda` AS nf_swift
ON
  nf_swift.cd_empresa = A.cd_empresa AND
  nf_swift.cd_filial = A.cd_filial AND
  nf_swift.cd_nota_fiscal = A.cd_nota_fiscal AND
  nf_swift.cd_serie = A.cd_serie AND
  nf_swift.cd_sequencia_historico = A.cd_sequencia_historico AND
  nf_swift.cd_entrada_saida = A.cd_entrada_saida AND
  nf_swift.cd_sequencia_item = A.cd_sequencia_item

WHERE
  A.cd_empresa IN (30, 36, 37, 40, 58, 60, 170, 178, 179, 184, 195) AND
  (B.ID_ACAO < 9 AND C.CD_TIPO_OPERACAO_FISCAL IN (1, 3, 11)) AND
  ((CASE WHEN C.CD_TIPO_OPERACAO_FISCAL = 11 THEN 1 ELSE I.CD_TIPO_LOJA_CLIENTE END) NOT IN (88, 89, 300)) AND 
  G.CD_ITEM IN ({str_lista_itens_ind}) AND
  (PARSE_DATETIME('%d/%m/%Y %H:%M:%S',B.DT_PROCESSO) BETWEEN '2022-04-01' AND CURRENT_DATE('America/Sao_Paulo'))

"""

df_faturamento_diario = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_faturamento_diario = df_faturamento_diario.join(
  df_de_para_filiais,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL']
)

df_faturamento_diario = df_faturamento_diario.withColumn(
  'CD_FILIAL', coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL'))
).withColumn(
  'CD_EMPRESA', coalesce(col('NOVO_CD_EMPRESA'), col('CD_EMPRESA'))
).drop('NOVO_CD_FILIAL', 'NOVO_CD_EMPRESA')

# COMMAND ----------

# Considera troca de nota de 2 pernas


df_troca_nota_2_pernas_faturamento = df_troca_nota_2_pernas.select(
    'NF_TROCA',
    'CD_EMPRESA_TRANSF',
    'CD_FILIAL_TRANSF',
    'CD_EMPRESA_VENDA',
    'CD_FILIAL_VENDA',
    'ITEM_TROCA',
    'DS_OPE_FISCAL_TPT_TROCA'
  ).filter(
    (col('CD_OPE_FISCAL_TPT_TROCA') == 33) # Mantém somente as vendas com troca de nota
  ).withColumnRenamed(
    'NF_TROCA', 'CD_NOTA_FISCAL'
  ).withColumnRenamed(
    'CD_EMPRESA_VENDA', 'CD_EMPRESA'
  ).withColumnRenamed(
    'CD_FILIAL_VENDA', 'CD_FILIAL'
  ).withColumnRenamed(
    'ITEM_TROCA', 'CD_ITEM'
  )

df_faturamento_diario = df_faturamento_diario.join(
  df_troca_nota_2_pernas_faturamento,
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'CD_NOTA_FISCAL']
)

df_faturamento_diario = df_faturamento_diario.withColumn(
  'CD_EMPRESA',
  coalesce(col('CD_EMPRESA_TRANSF'), col('CD_EMPRESA'))
).withColumn(
  'CD_FILIAL',
  coalesce(col('CD_FILIAL_TRANSF'), col('CD_FILIAL'))
)

# COMMAND ----------

# Considera troca de nota de 3 pernas

df_troca_nota_3_pernas_faturamento = df_troca_nota_3_pernas.select(
    'cd_empresa_transf',
    'cd_filial_transf',
    'CD_EMP_TROCA_TROCA',
    'cd_fil_troca_troca',
    'item_troca',
    'NF_TROCA_TROCA',
    'DS_OPE_FISCAL_TPT_TROCA'
  ).filter(
    (col('CD_OPE_FISCAL_TPT_TROCA_TROCA') == 33) # Mantém somente as vendas com troca de nota
  ).withColumnRenamed(
    'CD_EMP_TROCA_TROCA', 'CD_EMPRESA'
  ).withColumnRenamed(
    'cd_fil_troca_troca', 'CD_FILIAL'
  ).withColumnRenamed(
    'item_troca', 'CD_ITEM'
  ).withColumnRenamed(
    'NF_TROCA_TROCA', 'CD_NOTA_FISCAL'
  ).withColumnRenamed(
    'cd_empresa_transf', 'CD_EMPRESA_TRANSF_3_PERNAS'
  ).withColumnRenamed(
    'cd_filial_transf', 'CD_FILIAL_TRANSF_3_PERNAS'
  ).withColumnRenamed(
    'DS_OPE_FISCAL_TPT_TROCA', 'DS_OPE_FISCAL_TPT_TROCA_3_PERNAS'
  )

df_faturamento_diario = df_faturamento_diario.join(
  df_troca_nota_3_pernas_faturamento,
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL', 'CD_NOTA_FISCAL', 'CD_ITEM']
)

df_faturamento_diario = df_faturamento_diario.withColumn(
  'CD_EMPRESA',
  coalesce(col('CD_EMPRESA_TRANSF_3_PERNAS'), col('CD_EMPRESA'))
).withColumn(
  'CD_FILIAL',
  coalesce(col('CD_FILIAL_TRANSF_3_PERNAS'), col('CD_FILIAL'))
)

# COMMAND ----------

lista_colunas_data_faturamento_diario = [
 'DT_PROCESSO',
 'DT_EMISSAO',
 'DT_ENTRADA_SAIDA',
 'DT_CANCELAMENTO_NOTA',
 'DT_PREVISAO_CHEGADA',
 'DT_FABRICACAO_PRODUTO',
 'DT_APROVACAO_ESTOQUE',
 'DT_AGENDAMENTO',
 'DT_EMBARQUE_ORIGINAL',
 'DT_EMBARQUE_ENTRADA',
 'DT_EMBARQUE',
 'DT_PREVISTA_ENTREGA_ORIGINAL',
 'DT_PREVISTA_ENTREGA'
]

for coluna in lista_colunas_data_faturamento_diario:
  
  df_faturamento_diario = df_faturamento_diario.withColumn(coluna, col(coluna).cast(StringType()))

# COMMAND ----------

df_faturamento_diario.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.faturamento_diario').save()

# COMMAND ----------

# df_faturamento_resumido = df_faturamento_diario.groupBy(
#   'CD_FILIAL',
#   'CD_ITEM',
#   'DT_PROCESSO'
# ).agg(
#   F.sum('QT_FAT').alias('QT_FAT')
# ).withColumn(
#   'DT_EXECUCAO', to_date(col('DT_PROCESSO'))
# ).drop('DT_PROCESSO')

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Sazonalidade da demanda

# COMMAND ----------

query = f"""

SELECT 
    B.CD_FILIAL,
    G.CD_ITEM,
    PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10)) AS DT_REFERENCIA,
    DATE(
        EXTRACT( YEAR FROM PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10))),
        EXTRACT( MONTH FROM PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10))),
        1) AS DT_ANO_MES,
    EXTRACT( DAY FROM PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10))) AS DIA,
    EXTRACT( MONTH FROM PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10))) AS MES,
    EXTRACT( YEAR FROM PARSE_DATE('%d/%m/%Y',LEFT(B.DT_PROCESSO,10))) AS ANO,
    SUM(CASE 
        WHEN B.ID_TIPO_NOTA_FISCAL = 2 
        THEN (
                (
                    CASE 
                        WHEN B.ID_ACAO = 2 
                        THEN (A.QT_ITEM) * -1
                        ELSE A.QT_ITEM 
                        END
                )
            ) * -1 
        ELSE 
            (
                CASE 
                    WHEN B.ID_ACAO = 2 
                    THEN (A.QT_ITEM) * -1
                    ELSE A.QT_ITEM 
                    END
            )
        END ) AS QT_FAT_S_FEFO
FROM
    `seara-analytics-prod.stg_erp_seara.item_nota_fiscal_saida_acm` AS A
    
INNER JOIN
    `seara-analytics-prod.stg_erp_seara.nota_fiscal_saida_acm` AS B
ON
    B.CD_SEQUENCIA_HISTORICO = A.CD_SEQUENCIA_HISTORICO AND
    B.CD_NOTA_FISCAL = A.CD_NOTA_FISCAL AND
    B.CD_EMPRESA = A.CD_EMPRESA AND
    B.CD_ENTRADA_SAIDA = A.CD_ENTRADA_SAIDA AND
    B.CD_FILIAL = A.CD_FILIAL AND
    B.CD_SERIE = A.CD_SERIE     

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.operacao_fiscal_faturamento` AS C
ON
    C.CD_OPERACAO_FISCAL = B.CD_OPERACAO_FISCAL

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.pedido_venda_ctr` AS D
ON
    B.CD_PEDIDO = D.CD_PEDIDO_VENDA

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item_pedido_venda_ctr` AS F
ON
    A.CD_SEQ_ITEM_PED_VDA = F.CD_SEQ_ITEM_PED_VDA AND
    D.CD_PEDIDO_VENDA = F.CD_PEDIDO_VENDA

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.loja_cliente` AS I
ON
    I.CD_BASE_CLIENTE = B.CD_BASE_CLIENTE AND
    I.CD_LOJA_CLIENTE = B.CD_LOJA_CLIENTE

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.tipo_loja_cliente` AS TIPO_LOJA_CLIENTE_02
ON
    I.CD_TIPO_LOJA_CLIENTE = TIPO_LOJA_CLIENTE_02.CD_TIPO_LOJA_CLIENTE

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.canal_venda` AS CANAL_VENDA_02
ON
    CANAL_VENDA_02.CD_CANAL_VENDA = TIPO_LOJA_CLIENTE_02.CD_CANAL_VENDA

INNER JOIN
        `seara-analytics-prod.stg_erp_seara.item` AS G
ON
    A.CD_ITEM = G.CD_ITEM

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    G.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM AND
    G.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM AND
    G.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.negocio_comercial` AS NEGOCIO_COMERCIAL
ON
    NEGOCIO_COMERCIAL.CD_NEGOCIO_COMERCIAL = G.CD_NEGOCIO_COMERCIAL

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.empresa` AS TABELA_EMPRESA
ON
    B.CD_EMPRESA = TABELA_EMPRESA.CD_EMPRESA

INNER JOIN
    `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` AS J
ON
    G.CD_TIPO_ACLIMATACAO = J.CD_TIPO_ACLIMATACAO_PRO

INNER JOIN 
    `seara-analytics-prod.stg_erp_seara.marca_produto` AS H
ON
    G.CD_MARCA_PRODUTO = H.CD_MARCA_PRODUTO
    
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.item_nota_fiscal_dif_venda` AS nf_swift
ON
    nf_swift.cd_empresa = A.cd_empresa AND
    nf_swift.cd_filial = A.cd_filial AND
    nf_swift.cd_nota_fiscal = A.cd_nota_fiscal AND
    nf_swift.cd_serie = A.cd_serie AND
    nf_swift.cd_sequencia_historico = A.cd_sequencia_historico AND
    nf_swift.cd_entrada_saida = A.cd_entrada_saida AND
    nf_swift.cd_sequencia_item = A.cd_sequencia_item
WHERE
    A.cd_empresa IN (30, 36, 37, 40, 58, 60, 170, 178, 179, 184, 195) AND
    (B.ID_ACAO < 9 AND C.CD_TIPO_OPERACAO_FISCAL IN (1, 3, 11)) AND
    ((CASE WHEN C.CD_TIPO_OPERACAO_FISCAL = 11 THEN 1 ELSE I.CD_TIPO_LOJA_CLIENTE END) NOT IN (88, 89, 300)) AND 
    G.CD_CLASSE_ITEM NOT IN (29, 38) AND
    G.CD_NEGOCIO IN (35, 46, 47, 48, 49, 51, 57, 65, 66) AND
    PARSE_DATETIME('%d/%m/%Y %H:%M:%S',B.DT_PROCESSO) BETWEEN DATE_SUB(
        DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), INTERVAL 4  YEAR
        ) AND DATE_SUB(
        DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), INTERVAL 1 DAY)
    AND A.CD_ITEM IN ({str_lista_itens_ind})
GROUP BY 
  B.CD_FILIAL,
  G.CD_ITEM,
  DT_REFERENCIA,
  DT_ANO_MES,
  ANO,
  MES,
  DIA
ORDER BY
  CD_FILIAL,
  CD_ITEM,
  DT_REFERENCIA
"""

df_sazonalidade = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_sazonalidade = df_sazonalidade.filter(
  col('QT_FAT_S_FEFO') > 0
)

df_sazonalidade = df_sazonalidade.join(
  df_de_para_filiais.select('CD_FILIAL', 'NOVO_CD_FILIAL'),
  how = 'left',
  on = ['CD_FILIAL']
).withColumn(
  'CD_FILIAL',
  coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL'))
).drop('NOVO_CD_FILIAL').orderBy('CD_FILIAL','CD_ITEM','DT_REFERENCIA')

# Remove dia 29 de fevereiro

df_sazonalidade = df_sazonalidade.withColumn(
  'DIA',
  when((col('DIA') == 29)&(col('MES') == 2), 28).otherwise(col('DIA'))
)

df_sazonalidade = df_sazonalidade.groupBy(
  'CD_FILIAL',
  'CD_ITEM',
#   'DT_REFERENCIA',
#   'DT_ANO_MES',
  'DIA',
  'MES',
#   'ANO'
).agg(
  F.sum('QT_FAT_S_FEFO').alias('QT_FATURADA')
).orderBy(
  'CD_FILIAL',
  'CD_ITEM',
  'MES',
  'DIA'
)

spark.catalog.dropTempView("df")

df_sazonalidade.createOrReplaceTempView("df")

df_sazonalidade = spark.sql(
  """
    SELECT 
        *,
       SUM(QT_FATURADA) OVER (PARTITION BY CD_FILIAL, CD_ITEM, MES) AS QT_FATURADA_TOTAL_MES
     FROM 
       df
   """
)

df_sazonalidade = df_sazonalidade.withColumn(
  'PR_FAT_DIA', F.round(col('QT_FATURADA')/col('QT_FATURADA_TOTAL_MES'),3)
)

df_sazonalidade = df_sazonalidade.drop('QT_FATURADA', 'QT_FATURADA_TOTAL_MES').orderBy('CD_FILIAL', 'CD_ITEM', 'MES', 'DIA')

df_sazonalidade.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.sazonalidade_faturamento_itens').save()

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Volume meta diarizado por filial

# COMMAND ----------

# O volume meta é dado por PDV no painel comercial e devemos atribuir o volume meta de cada cliente a uma filial, usando o df_filial_abastecedora

query = f"""

WITH TABELA AS (

SELECT
  *
FROM
  `pricing-seara.comercial.base_comercial`
WHERE
  LENGTH(CAST(DT_ANOMES AS STRING)) = 6

)

SELECT 
  PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING)) AS MES_REF,
  EXTRACT(MONTH FROM PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING))) AS MES,
  EXTRACT(YEAR FROM PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING))) AS ANO,
  CAST(SPLIT(CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(1)] AS INT) AS CD_BASE_CLIENTE,
  CAST(SPLIT(CD_BASE_LOJA_CLIENTE, '|')[safe_ordinal(2)] AS INT) AS CD_LOJA_CLIENTE,
  NM_LOCALIDADE,
  CD_ITEM,
  SUM(VOLUME_META) AS VOLUME_META,
  ROUND(SUM(VOLUME_META)/{num_dias_diarizacao},2) AS VOLUME_META_DIARIZADO_FLAT
FROM
  TABELA
WHERE
  PARSE_DATE('%Y%m', CAST(DT_ANOMES AS STRING)) BETWEEN 
                                      DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')),EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1)
                                      AND LAST_DAY(CURRENT_DATE('America/Sao_Paulo'), MONTH)
  AND CD_BASE_LOJA_CLIENTE IS NOT NULL
  AND CD_ITEM IN ({str_lista_itens_ind})
GROUP BY 
  DT_ANOMES, 
  CD_BASE_LOJA_CLIENTE,
  NM_LOCALIDADE,
  CD_ITEM
ORDER BY 
  DT_ANOMES, 
  CD_BASE_CLIENTE,
  CD_LOJA_CLIENTE,
  CD_ITEM
"""

df_volume_meta_por_pdv = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_volume_meta = df_volume_meta_por_pdv.join(
  df_filial_pdv_item,
  how = 'left',
  on = ['CD_BASE_CLIENTE','CD_LOJA_CLIENTE','CD_ITEM']
).join(
  df_filial_municipio_item,
  how = 'left',
  on = ['NM_LOCALIDADE','CD_ITEM']
).join(
  df_filial_municipio,
  how = 'left',
  on = ['NM_LOCALIDADE']
)

df_volume_meta = df_volume_meta.withColumn('CD_FILIAL', coalesce(col('CD_FILIAL_PDV_ITEM'),col('CD_FILIAL_MUNICIPIO_ITEM'),col('CD_FILIAL_MUNICIPIO')))

# COMMAND ----------

df_volume_meta = df_volume_meta.join(
  df_de_para_filiais.select('CD_FILIAL','NOVO_CD_FILIAL'),
  how = 'left',
  on = ['CD_FILIAL']
)

df_volume_meta = df_volume_meta.withColumn('CD_FILIAL', coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL')))

df_volume_meta = df_volume_meta.drop('NOVO_CD_FILIAL')

# COMMAND ----------

df_volume_meta = df_volume_meta.groupBy('ANO', 'MES','CD_FILIAL', 'CD_ITEM').agg(
  F.sum(col('VOLUME_META')).alias('VOLUME_META'),
  F.sum(col('VOLUME_META_DIARIZADO_FLAT')).alias('VOLUME_META_DIARIZADO_FLAT'),
)

# COMMAND ----------

# O painel comercial possui o volume meta somente para o mês vigente. Replicaremos o mesmo volume para os próximos dois meses.

def encontra_proximo_mes(mes):
  
  if mes == 12:
    
    return 1
  
  else:
    return  mes + 1
  
mes_atual = hoje.month

m_mais_1 = encontra_proximo_mes(mes_atual)

m_mais_2 = encontra_proximo_mes(m_mais_1)

ano_atual = hoje.year

if m_mais_1 < mes_atual:
  
  ano_mais_1 = ano_atual + 1

else:
   
  ano_mais_1 = ano_atual
  
if m_mais_2 < m_mais_1:
  
  ano_mais_2 = ano_mais_1 + 1
  
else:
 
  ano_mais_2 = ano_mais_1

# COMMAND ----------

# Se já existe o volume meta para o mês seguinte, manteremos essa informação

df_volume_meta = df_volume_meta.withColumn('LINHA_ORIGINAL', lit(1))

# COMMAND ----------

df_volume_meta_m_mais_1 = df_volume_meta.alias('df_volume_meta_m_mais_1')

df_volume_meta_m_mais_1 = df_volume_meta_m_mais_1.withColumn('MES', lit(m_mais_1)).withColumn('LINHA_ORIGINAL', lit(0))

df_volume_meta_m_mais_2 = df_volume_meta.alias('df_volume_meta_m_mais_2')

df_volume_meta_m_mais_2 = df_volume_meta_m_mais_2.withColumn('MES', lit(m_mais_2)).withColumn('LINHA_ORIGINAL', lit(0))

df_volume_meta = df_volume_meta.union(df_volume_meta_m_mais_1).union(df_volume_meta_m_mais_2)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_volume_meta.createOrReplaceTempView("df")

df_volume_meta = spark.sql(
  """
    SELECT 
        *,
       row_number() OVER (PARTITION BY CD_FILIAL, CD_ITEM, MES ORDER BY LINHA_ORIGINAL DESC) AS rn
     FROM 
       df
   """
)

df_volume_meta = df_volume_meta.filter(
  col('rn') == 1
).drop('rn', 'LINHA_ORIGINAL')

# COMMAND ----------

df_volume_meta = df_volume_meta.join(
  df_sazonalidade,
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM', 'MES']
).orderBy('CD_FILIAL', 'CD_ITEM', 'MES', 'DIA')

# COMMAND ----------

df_volume_meta = df_volume_meta.withColumn('VOLUME_META_DIARIZADO', col('VOLUME_META')*col('PR_FAT_DIA'))

# COMMAND ----------

df_volume_meta = df_volume_meta.join(
  df_abastecimento.drop('DESVPAD_INTERVALO_ABASTECIMENTO'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_ciclo_producao.drop('CD_EMPRESA', 'DESVPAD_INTERVALO_PRODUCAO'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
)

# COMMAND ----------

df_volume_meta = df_volume_meta.filter(
  ~col('CD_FILIAL').isNull()
)

# COMMAND ----------

df_volume_meta = df_volume_meta.withColumn("DATA",concat_ws("-",col("ANO"),col("MES"),col("DIA")).cast("timestamp"))

# COMMAND ----------

df_volume_meta = df_volume_meta.withColumn('VOLUME_META_DIARIZADO', col('VOLUME_META_DIARIZADO').cast(DoubleType()))

# COMMAND ----------

df_volume_meta = df_volume_meta.filter(
  col('DATA') >= hoje
).orderBy('CD_FILIAL', 'CD_ITEM', 'DATA')

# COMMAND ----------

df_volume_meta.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.volume_meta_projetado').save()

# COMMAND ----------

# Essa parte do código é utilizada para acrescentar as linhas para a data de hoje, para os casos que não teriam linhas na data de hoje

df_calendario_meta = df_volume_meta.groupBy(
  'CD_FILIAL',
  'CD_ITEM',
#   'MES',
#   'ANO',
  'VOLUME_META',
  'VOLUME_META_DIARIZADO_FLAT',
#   'DIA',
#   'PR_FAT_DIA',
#   'VOLUME_META_DIARIZADO',
  'INTERVALO_ABASTECIMENTO_MEDIANO',
  'INTERVALO_PRODUCAO_MEDIANO'
).agg(F.min('DATA').alias('DATA'))

df_calendario_meta = df_calendario_meta.filter(
  col('DATA') != hoje
)

df_calendario_meta = df_calendario_meta.filter(
  col('DATA') != hoje
)

df_calendario_meta = df_calendario_meta.withColumn(
  'DATA', to_timestamp(lit(hoje))
).withColumn(
  'MES', lit(hoje.month).cast('long')
).withColumn(
  'ANO', lit(hoje.year).cast('long')
).withColumn(
  'DIA', lit(hoje.day).cast('long')
).withColumn(
  'PR_FAT_DIA', lit(0).cast('double')
).withColumn(
  'VOLUME_META_DIARIZADO', lit(0).cast('double')
)

df_calendario_meta = df_calendario_meta.select(
  'CD_FILIAL',
  'CD_ITEM',
  'MES',
  'ANO',
  'VOLUME_META',
  'VOLUME_META_DIARIZADO_FLAT',
  'DIA',
  'PR_FAT_DIA',
  'VOLUME_META_DIARIZADO',
  'INTERVALO_ABASTECIMENTO_MEDIANO',
  'INTERVALO_PRODUCAO_MEDIANO',
  'DATA'
)

# COMMAND ----------

df_volume_meta = df_volume_meta.union(df_calendario_meta)

# COMMAND ----------

df_volume_meta = df_volume_meta.withColumn(
  'INTERVALO_ABASTECIMENTO_MEDIANO', 
  coalesce(
    col('INTERVALO_ABASTECIMENTO_MEDIANO'), 
    col('INTERVALO_PRODUCAO_MEDIANO'), 
    lit(0)
  )
).drop('INTERVALO_PRODUCAO_MEDIANO')

# COMMAND ----------

string_cases_sql_volume_intervalo_abastecimento = ""
string_cases_sql_dias_intervalo_abastecimento = ""

inicio = 1
fim = 180
step = 1

for valor in range(inicio, fim + step, step):
  
  string_cases_sql_volume_intervalo_abastecimento += f"WHEN (INTERVALO_ABASTECIMENTO_MEDIANO <= {valor}) THEN SUM(VOLUME_META_DIARIZADO) OVER ( PARTITION BY CD_FILIAL, CD_ITEM ORDER BY DATA RANGE BETWEEN CURRENT ROW AND INTERVAL {valor} DAYS FOLLOWING ) \n"
  
  string_cases_sql_dias_intervalo_abastecimento += f"WHEN (INTERVALO_ABASTECIMENTO_MEDIANO <= {valor}) THEN {valor} \n"

# COMMAND ----------

spark.catalog.dropTempView("df")

df_volume_meta.createOrReplaceTempView("df")

# valor_limite = 180

df_volume_meta = spark.sql(
    f"""
      SELECT 
        *,
        CASE 
          {string_cases_sql_volume_intervalo_abastecimento}
          WHEN (INTERVALO_ABASTECIMENTO_MEDIANO > {fim}) THEN SUM(VOLUME_META_DIARIZADO) OVER ( PARTITION BY CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp) RANGE BETWEEN CURRENT ROW AND INTERVAL {fim} DAYS FOLLOWING )
          ELSE NULL
        END AS CONSUMO_CICLO_ABASTECIMENTO,
        CASE 
          {string_cases_sql_dias_intervalo_abastecimento}
          WHEN (INTERVALO_ABASTECIMENTO_MEDIANO > {fim}) THEN {fim}
          ELSE NULL
        END AS DIAS_CONSIDERADOS_CICLO_ABASTECIMENTO,
        ROW_NUMBER() OVER (PARTITION BY CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp)) as rn
      FROM
       df
      WHERE
       DATA >= '{hoje}'
    """
)

# COMMAND ----------

# Armazena df para ser usado na projecao de estoque e recomendação de reabastecimento

# df_projecao_volume_meta = df_volume_meta.alias('df_projecao_volume_meta')

# COMMAND ----------

df_volume_meta = df_volume_meta.filter(
  col('rn') == 1
)

# COMMAND ----------

df_volume_meta = df_volume_meta.select(
  'CD_FILIAL',
  'CD_ITEM',
  'VOLUME_META',
  'VOLUME_META_DIARIZADO_FLAT',
  'VOLUME_META_DIARIZADO',
  'CONSUMO_CICLO_ABASTECIMENTO',
  'DIAS_CONSIDERADOS_CICLO_ABASTECIMENTO'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Sazonalidade cancelamento de pedidos

# COMMAND ----------

query = f"""

SELECT
    LOJA_CLIENTE8.DS_REDE,
    ITEM_PEDIDO_VENDA_CTR2.CD_ITEM,
    ITEM_PEDIDO_VENDA_CTR2.CD_FILIAL_FIS_ENTREGA,
    ITEM_PEDIDO_VENDA_CTR2.CD_EMPRESA_FIS_ENTREGA,
    COALESCE(ITEM_PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO, PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO) AS CD_MOTIVO_CANCELAMENTO,
    MOTIVO_CCL_PEDIDO_VENDA2.DS_MOTIVO_CCL_PEDIDO,
    DATE(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_LQD_CCL_PEDIDO)) AS DT_LQD_CCL_PEDIDO,
    ITEM_PEDIDO_VENDA_CTR2.CD_PEDIDO_VENDA,
    ITEM_PEDIDO_VENDA_CTR2.CD_REGIONAL_VENDA,
    FILIAL_CGC20.NM_FILIAL,
    REGIONAL_VENDA28.NM_REGIONAL_VENDA,
    CAST(ITEM_PEDIDO_VENDA_CTR2.QT_CONTRATADA AS FLOAT64) QT_CONTRATADA,
    CAST(SUM(ITEM_PEDIDO_VENDA_CTR2.QT_ENTREGA_FILIAL) AS FLOAT64) QT_ENTREGA_FILIAL,
    CAST(SUM(ITEM_PEDIDO_VENDA_CTR2.QT_ENTREGA_MATRIZ) AS FLOAT64) QT_ENTREGA_MATRIZ,
    ITEM_PEDIDO_VENDA_CTR2.VL_UNITARIO_PEDIDO,
    PEDIDO_VENDA_CTR2.ID_TIPO_PEDIDO,
    ITEM.NM_ITEM,
    LOJA_CLIENTE8.NM_CLIENTE,
    PEDIDO_VENDA_CTR2.CD_BASE_CLIENTE,
    VENDEDOR62.NM_VENDEDOR,
    CLASSE_ITEM.NM_CLASSE,
    NEGOCIO.NM_NEGOCIO,
    DATE(ITEM_PEDIDO_VENDA_CTR2.DT_EMBARQUE) DT_EMBARQUE,
    DATE(ITEM_PEDIDO_VENDA_CTR2.DT_EMBARQUE_ORIGINAL) DT_EMBARQUE_ORIGINAL,
    DATE(ITEM_PEDIDO_VENDA_CTR2.DT_PREVISTA_ENTREGA) DT_PREVISTA_ENTREGA,
    PEDIDO_VENDA_CTR2.CD_OPERACAO_VENDA,
    CANAL_VENDA.CD_CANAL_VENDA,
    CANAL_VENDA.NM_CANAL_VENDA,
    MUNICIPIO.nm_municipio,
    loja_cliente8.nm_curto_cliente,
    NM_MARCA,
    EXTRACT(MONTH FROM DT_EMBARQUE_ORIGINAL) AS MES_REF,
    EXTRACT(DAY FROM DT_EMBARQUE_ORIGINAL) AS DIA_REF,
    EXTRACT(YEAR FROM DT_EMBARQUE_ORIGINAL) AS ANO_REF,
FROM
    `seara-analytics-prod.stg_erp_seara.pedido_venda_ctr` PEDIDO_VENDA_CTR2
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item_pedido_venda_ctr` ITEM_PEDIDO_VENDA_CTR2
ON
    PEDIDO_VENDA_CTR2.CD_PEDIDO_VENDA=ITEM_PEDIDO_VENDA_CTR2.CD_PEDIDO_VENDA 
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.motivo_ccl_pedido_venda` MOTIVO_CCL_PEDIDO_VENDA
ON
    MOTIVO_CCL_PEDIDO_VENDA.CD_MOTIVO_CCL_PEDIDO=ITEM_PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO +0
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.motivo_ccl_pedido_venda` MOTIVO_CCL_PEDIDO_VENDA2
ON
    MOTIVO_CCL_PEDIDO_VENDA2.CD_MOTIVO_CCL_PEDIDO=PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO +0
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item` ITEM
ON
    ITEM.CD_ITEM=ITEM_PEDIDO_VENDA_CTR2.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO=NEGOCIO.CD_NEGOCIO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_CGC20
ON
    ITEM_PEDIDO_VENDA_CTR2.CD_EMPRESA_FIS_ENTREGA=FILIAL_CGC20.CD_EMPRESA
    AND ITEM_PEDIDO_VENDA_CTR2.CD_FILIAL_FIS_ENTREGA=FILIAL_CGC20.CD_FILIAL
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.regional_venda` REGIONAL_VENDA28
ON
    ITEM_PEDIDO_VENDA_CTR2.CD_REGIONAL_VENDA=REGIONAL_VENDA28.CD_REGIONAL_VENDA
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.loja_cliente` LOJA_CLIENTE8
ON
    LOJA_CLIENTE8.CD_BASE_CLIENTE=PEDIDO_VENDA_CTR2.CD_BASE_CLIENTE
    AND LOJA_CLIENTE8.CD_LOJA_CLIENTE=PEDIDO_VENDA_CTR2.CD_LOJA_CLIENTE
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.tipo_loja_cliente` TLC
ON
    LOJA_CLIENTE8.CD_TIPO_LOJA_CLIENTE = TLC.CD_TIPO_LOJA_CLIENTE
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.vendedor` VENDEDOR62
ON
    VENDEDOR62.CD_BASE_VENDEDOR=PEDIDO_VENDA_CTR2.CD_BASE_VENDEDOR
    AND VENDEDOR62.CD_ESTAB_VENDEDOR=PEDIDO_VENDA_CTR2.CD_ESTAB_VENDEDOR
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM=CLASSE_ITEM.CD_GRUPO_ITEM 
    and ITEM.CD_SUBGRUPO_ITEM=CLASSE_ITEM.CD_SUBGRUPO_ITEM 
    and ITEM.CD_CLASSE_ITEM=CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN  
    `seara-analytics-prod.stg_erp_seara.canal_venda` CANAL_VENDA
ON
    TLC.CD_CANAL_VENDA = CANAL_VENDA.CD_CANAL_VENDA
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.municipio` MUNICIPIO
ON
    LOJA_CLIENTE8.cd_cep_logradouro = MUNICIPIO.cd_cep_logradouro
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.marca_produto` MARCA_PRODUTO
ON
    MARCA_PRODUTO.CD_MARCA_PRODUTO = ITEM.CD_MARCA_PRODUTO
WHERE
    DATE(ITEM_PEDIDO_VENDA_CTR2.DT_EMBARQUE_ORIGINAL) >= DATE_SUB(DATE(EXTRACT(YEAR FROM CURRENT_DATE('America/Sao_Paulo')), EXTRACT(MONTH FROM CURRENT_DATE('America/Sao_Paulo')),1), INTERVAL 4 YEAR)
    AND ((PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO IS NOT NULL) OR (ITEM_PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO IS NOT NULL))
    AND  ITEM.CD_GRUPO_ITEM+0  =  3
    AND  ITEM.CD_DESTINO_FINAL  =  1
    AND ITEM.CD_ITEM IN ({str_lista_itens_ind})
    AND  LOJA_CLIENTE8.CD_TIPO_LOJA_CLIENTE  NOT IN  (300, 89, 88)
    AND  VENDEDOR62.NM_VENDEDOR  NOT LIKE  'EKF -%'
GROUP BY
    ITEM_PEDIDO_VENDA_CTR2.CD_ITEM, 
    ITEM_PEDIDO_VENDA_CTR2.CD_FILIAL_FIS_ENTREGA, 
    ITEM_PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO, 
    ITEM_PEDIDO_VENDA_CTR2.CD_PEDIDO_VENDA, 
    ITEM_PEDIDO_VENDA_CTR2.CD_REGIONAL_VENDA, 
    MOTIVO_CCL_PEDIDO_VENDA.DS_MOTIVO_CCL_PEDIDO, 
    DT_LQD_CCL_ITEM_PEDIDO, 
    FILIAL_CGC20.NM_FILIAL, 
    REGIONAL_VENDA28.NM_REGIONAL_VENDA, 
    ITEM_PEDIDO_VENDA_CTR2.QT_CONTRATADA, 
    DT_LQD_CCL_PEDIDO,
    MOTIVO_CCL_PEDIDO_VENDA2.DS_MOTIVO_CCL_PEDIDO,
    ITEM_PEDIDO_VENDA_CTR2.VL_UNITARIO_PEDIDO, 
    PEDIDO_VENDA_CTR2.ID_TIPO_PEDIDO, 
    ITEM.NM_ITEM, 
    PEDIDO_VENDA_CTR2.CD_MOTIVO_CCL_PEDIDO, 
    LOJA_CLIENTE8.NM_CLIENTE, 
    PEDIDO_VENDA_CTR2.CD_BASE_CLIENTE, 
    VENDEDOR62.NM_VENDEDOR, 
    CLASSE_ITEM.NM_CLASSE, 
    NEGOCIO.NM_NEGOCIO, 
    ITEM_PEDIDO_VENDA_CTR2.DT_EMBARQUE,
    ITEM_PEDIDO_VENDA_CTR2.DT_EMBARQUE_ORIGINAL,
    ITEM_PEDIDO_VENDA_CTR2.DT_PREVISTA_ENTREGA, 
    PEDIDO_VENDA_CTR2.CD_OPERACAO_VENDA,
    CANAL_VENDA.CD_CANAL_VENDA,
    CANAL_VENDA.NM_CANAL_VENDA,
    MUNICIPIO.nm_municipio,
    ITEM_PEDIDO_VENDA_CTR2.CD_EMPRESA_FIS_ENTREGA,
    loja_cliente8.nm_curto_cliente,
    LOJA_CLIENTE8.DS_REDE,
    NM_MARCA

"""

df_sazonalidade_cancelamento = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.filter(
  col('CD_MOTIVO_CANCELAMENTO').isin(lista_motivos_cancelamento_por_falta_estoque)
)

# COMMAND ----------

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.groupBy(
  'CD_EMPRESA_FIS_ENTREGA',
  'CD_FILIAL_FIS_ENTREGA',
  'CD_ITEM',
  'DIA_REF',
  'MES_REF'
).agg(
  F.sum('QT_CONTRATADA').alias('QT_CONTRATADA')
).withColumnRenamed('CD_EMPRESA_FIS_ENTREGA', 'CD_EMPRESA').withColumnRenamed('CD_FILIAL_FIS_ENTREGA', 'CD_FILIAL')

# COMMAND ----------

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.join(
  df_de_para_filiais,
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL']
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.withColumn(
  'CD_EMPRESA', coalesce(col('NOVO_CD_EMPRESA'), col('CD_EMPRESA'))
).withColumn(
  'CD_FILIAL', coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL'))
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.drop('NOVO_CD_EMPRESA', 'NOVO_CD_FILIAL')

# COMMAND ----------

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.groupBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM',
  'DIA_REF',
  'MES_REF'
).agg(
  F.sum('QT_CONTRATADA').alias('QT_CONTRATADA')
)

# COMMAND ----------

spark.catalog.dropTempView("df")

df_sazonalidade_cancelamento.createOrReplaceTempView("df")

df_sazonalidade_cancelamento = spark.sql(
  """
    SELECT 
        *,
       SUM(QT_CONTRATADA) OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM, MES_REF) AS QT_CONTRATADA_TOTAL_MES
     FROM 
       df
   """
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.withColumn(
  'PR_CANCELAMENTO_DIA', F.round(col('QT_CONTRATADA')/col('QT_CONTRATADA_TOTAL_MES'),3)
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.drop('QT_CONTRATADA', 'QT_CONTRATADA_TOTAL_MES').orderBy('CD_FILIAL', 'CD_ITEM', 'MES_REF', 'DIA_REF')

# COMMAND ----------

# df_sazonalidade_cancelamento.filter(
#   (col('CD_FILIAL') == 892)
#   &(col('CD_ITEM') == 326259)
# ).display()

# COMMAND ----------

df_sazonalidade_cancelamento.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.sazonalidade_cancelamento_itens').save()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Pedidos Cancelados

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Média mensal histórica

# COMMAND ----------

query = f"""

SELECT
  *
FROM
  `seara-auditoria-cfo-nonprod.abastecimento_seara.view_cancelamento_pedidos`
WHERE
  CD_ITEM IN ({str_lista_itens_ind})
"""

df_pedidos_cancelados_original = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

df_pedidos_cancelados_original = df_pedidos_cancelados_original.join(
  df_de_para_filiais,
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL']
)

df_pedidos_cancelados_original = df_pedidos_cancelados_original.withColumn(
  'CD_EMPRESA', coalesce(col('NOVO_CD_EMPRESA'), col('CD_EMPRESA'))
).withColumn(
  'CD_FILIAL', coalesce(col('NOVO_CD_FILIAL'), col('CD_FILIAL'))
)

# Mantém somente os cancelamentos relacionados à falta de estoque

df_pedidos_cancelados_original = df_pedidos_cancelados_original.filter(
  col('CD_MOTIVO_CANCELAMENTO').isin(lista_motivos_cancelamento_por_falta_estoque)
)

df_pedidos_cancelados = df_pedidos_cancelados_original.alias('df_pedidos_cancelados')

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.groupBy(
  'MES_REF',
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM'
).agg(
  F.sum('QT_CONTRATADA').alias('QT_CANCELAMENTO')
)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.withColumn('QT_CANCELAMENTO_DIARIZADA', col('QT_CANCELAMENTO')/num_dias_diarizacao)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.groupBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM'
).agg(
  F.avg(col('QT_CANCELAMENTO_DIARIZADA')).alias('QT_CANCELAMENTO_DIARIZADA'),
  F.avg(col('QT_CANCELAMENTO')).alias('MEDIA_QT_CANCELAMENTO_MES')
)

# COMMAND ----------

df_pedidos_cancelados_original = df_pedidos_cancelados_original.join(
  df_pedidos_cancelados,
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM']
)

# COMMAND ----------

df_pedidos_cancelados_original.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.pedidos_cancelados').save()

# COMMAND ----------

# df_pedidos_cancelados.filter(
#   (col('CD_FILIAL') == 892)
#   &(col('CD_ITEM') == 326259)
# ).display()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Diarização dos cancelamentos

# COMMAND ----------

# Vamos utilizar somente a sazonalidade dos proximos 3 meses

spark.catalog.dropTempView("df")

df_sazonalidade_cancelamento.createOrReplaceTempView("df")

df_sazonalidade_cancelamento = spark.sql(
  f"""
    SELECT 
        *
     FROM 
       df
     WHERE
       MES_REF IN ({mes_atual}, {m_mais_1}, {m_mais_2})
     ORDER BY
        CD_EMPRESA,
        CD_FILIAL,
        CD_ITEM,
        MES_REF,
        DIA_REF
   """
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.withColumn(
  'ANO_REF',
  when(col('MES_REF') == mes_atual, lit(ano_atual)). \
  when(col('MES_REF') == m_mais_1, lit(ano_mais_1)). \
  when(col('MES_REF') == m_mais_2, lit(ano_mais_2))
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.withColumn(
  'DIA_REF',
  adiciona_zero_esquerda_UDF(col('DIA_REF'))
).withColumn(
  'MES_REF',
  adiciona_zero_esquerda_UDF(col('MES_REF'))
)

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.withColumn(
 'DATA',
  to_date(concat(col('ANO_REF'),lit('-'),col('MES_REF'),lit('-'),col('DIA_REF')), "yyyy-MM-dd")
)

# COMMAND ----------

df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.filter(
  col('DATA') >= hoje
)

# COMMAND ----------

# O código a seguir é utilizado para adicionar o dia de hoje nos casos em que não teria uma linha correspondente ao dia de hoje.
# Isso é importante pois iremos calcular a quantidade de cancelamento no próximo ciclo de abastecimento, a partir de hoje.

df_calendario_cancelamento = df_sazonalidade_cancelamento.groupBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM'
).agg(F.min('DATA').alias('DATA'))


df_calendario_cancelamento = df_calendario_cancelamento.filter(
  col('DATA') != hoje
)

# df_calendario_cancelamento.display()

df_calendario_cancelamento = df_calendario_cancelamento.withColumn(
  'DATA', lit(hoje)
).withColumn(
  'ANO_REF', lit(hoje.year)
).withColumn(
  'MES_REF', adiciona_zero_esquerda_UDF(lit(hoje.month))
).withColumn(
  'DIA_REF', adiciona_zero_esquerda_UDF(lit(hoje.day))
).withColumn(
  'PR_CANCELAMENTO_DIA', lit(0).cast('double')
)

df_calendario_cancelamento = df_calendario_cancelamento.select(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM',
  'DIA_REF',
  'MES_REF',
  'PR_CANCELAMENTO_DIA',
  'ANO_REF',
  'DATA'
)


df_sazonalidade_cancelamento = df_sazonalidade_cancelamento.union(df_calendario_cancelamento).orderBy(
  'CD_EMPRESA',
  'CD_FILIAL',
  'CD_ITEM',
  'DATA'
)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.join(
  df_abastecimento.drop('DESVPAD_INTERVALO_ABASTECIMENTO'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.join(
  df_sazonalidade_cancelamento.drop('DIA_REF', 'MES_REF', 'ANO_REF'),
  how = 'left',
  on = ['CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM']
).orderBy('CD_EMPRESA', 'CD_FILIAL', 'CD_ITEM', 'DATA')

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.withColumn(
  'QT_CANCELAMENTO_PROJETADA_DIARIZADA',
  col('MEDIA_QT_CANCELAMENTO_MES')*col('PR_CANCELAMENTO_DIA')
)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.withColumn(
  'DATA',
  col('DATA').cast('timestamp')
)

# COMMAND ----------

string_cases_sql_cancelamento_intervalo_abastecimento = ""

inicio = 1
fim = 180
step = 1

for valor in range(inicio, fim + step, step):
  
  string_cases_sql_cancelamento_intervalo_abastecimento += f"WHEN (INTERVALO_ABASTECIMENTO_MEDIANO <= {valor}) THEN SUM(QT_CANCELAMENTO_PROJETADA_DIARIZADA) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY DATA RANGE BETWEEN CURRENT ROW AND INTERVAL {valor} DAYS FOLLOWING ) \n"

# COMMAND ----------

spark.catalog.dropTempView("df")

df_pedidos_cancelados.createOrReplaceTempView("df")

# valor_limite = 180

df_pedidos_cancelados = spark.sql(
    f"""
      SELECT 
        *,
        CASE 
          {string_cases_sql_cancelamento_intervalo_abastecimento}
          WHEN (INTERVALO_ABASTECIMENTO_MEDIANO > {fim}) THEN SUM(QT_CANCELAMENTO_PROJETADA_DIARIZADA) OVER ( PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp) RANGE BETWEEN CURRENT ROW AND INTERVAL {fim} DAYS FOLLOWING )
          ELSE NULL
        END AS CANCELAMENTO_CICLO_ABASTECIMENTO,
        ROW_NUMBER() OVER (PARTITION BY CD_EMPRESA, CD_FILIAL, CD_ITEM ORDER BY CAST(DATA AS timestamp)) as rn
      FROM
       df
      WHERE
       DATA >= '{hoje}'
    """
)

# COMMAND ----------

# df_pedidos_cancelados.filter(
#   (col('CD_FILIAL') == 910)
#   &(col('CD_ITEM') == 326259)
# ).display()

# COMMAND ----------

df_pedidos_cancelados.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.projecao_diarizada_cancelamentos').save()

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.filter(
  col('DATA') == hoje
).drop(
  'INTERVALO_ABASTECIMENTO_MEDIANO',
  'PR_CANCELAMENTO_DIA',
  'DATA',
  'rn',
  'QT_CANCELAMENTO_PROJETADA_DIARIZADA',
  'MEDIA_QT_CANCELAMENTO_MES'
)

# COMMAND ----------

df_pedidos_cancelados = df_pedidos_cancelados.na.fill(value=0,subset=["CANCELAMENTO_CICLO_ABASTECIMENTO"])

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Desvio da demanda

# COMMAND ----------

df_desvio_demanda = df_volume_realizado.join(
  df_volume_meta,
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_pedidos_cancelados.drop('CD_EMPRESA'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
)

df_desvio_demanda = df_desvio_demanda.withColumn(
  'DESVIO_REAL_MAIS_CANCELADO_MENOS_META', F.abs(col('VOLUME_REAL_DIARIZADO') + col('QT_CANCELAMENTO_DIARIZADA') - col('VOLUME_META_DIARIZADO_FLAT'))
)

df_desvio_demanda = df_desvio_demanda.na.fill(0, subset=['DESVPAD_VOLUME_REAL_DIARIZADO', 'DESVIO_REAL_MAIS_CANCELADO_MENOS_META'])

df_desvio_demanda = df_desvio_demanda.withColumn(
  'DESVIO_DEMANDA_DIARIZADO', when(col('DESVPAD_VOLUME_REAL_DIARIZADO') >= col('DESVIO_REAL_MAIS_CANCELADO_MENOS_META'), col('DESVPAD_VOLUME_REAL_DIARIZADO')) \
  .otherwise(col('DESVIO_REAL_MAIS_CANCELADO_MENOS_META'))
)

df_desvio_demanda = df_desvio_demanda.select(
  'CD_FILIAL',
  'CD_ITEM',
#   'DESVPAD_VOLUME_REAL_DIARIZADO',
#   'DESVIO_REAL_MAIS_CANCELADO_MENOS_META',
  'DESVIO_DEMANDA_DIARIZADO'
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Saldo Atual

# COMMAND ----------

query = f"""

SELECT
  CD_EMPRESA,
  CD_FILIAL,
  -- CD_LOCAL_ESTOQUE,
  CD_ITEM,
  PARSE_DATE('%d/%m/%Y', LEFT(DT_REFERENCIA,10)) AS DATA_REFERENCIA_ESTOQUE,
  -- MIN(PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_ATUALIZACAO)) AS MIN_HORA_ATUALIZACAO,
  SUM(QT_VOLUME) QT_VOLUME_ATUAL,
  SUM(QT_PESO) QT_PESO_ATUAL
FROM
  `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
WHERE
  PARSE_DATE('%d/%m/%Y', LEFT(DT_REFERENCIA, 10)) = (
    SELECT
      MAX(PARSE_DATE('%d/%m/%Y', LEFT(DT_REFERENCIA, 10)))
    FROM
      `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia`
  )
  AND CD_LOCAL_ESTOQUE = 1
  AND CD_ITEM IN ({str_lista_itens_ind})
GROUP BY
  CD_EMPRESA,
  CD_FILIAL,
  CD_LOCAL_ESTOQUE,
  CD_ITEM,
  DATA_REFERENCIA_ESTOQUE
ORDER BY
  CD_EMPRESA,
  CD_FILIAL,
  CD_ITEM,
  CD_LOCAL_ESTOQUE
  
"""

df_saldo_atual = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Consolidando todas as informações e calculando o estoque ideal

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Cálculo do estoque ideal

# COMMAND ----------

df = df_volume_realizado.join(
  df_desvio_demanda,
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_volume_meta.na.fill(value=0,subset=["CONSUMO_CICLO_ABASTECIMENTO"]),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_pedidos_cancelados.drop('CD_EMPRESA'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_abastecimento.drop('DESVPAD_INTERVALO_ABASTECIMENTO'),
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
).join(
  df_saldo_atual.drop('CD_EMPRESA'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
).join(
  df_ciclo_producao.drop('CD_EMPRESA'),
  how = 'left',
  on = ['CD_FILIAL', 'CD_ITEM']
)

df_filial_abastecimento_item = df_filial_abastecimento_item.withColumnRenamed('CD_FILIAL_DESTINO','CD_FILIAL').withColumnRenamed('CD_FILIAL_ORIGEM','CD_FILIAL_ABASTECIMENTO_ITEM')

df = df.join(
  df_filial_abastecimento_item,
  how = 'left',
  on = ['CD_FILIAL','CD_ITEM']
)

df_filial_abastecimento_filial = df_filial_abastecimento_filial.withColumnRenamed('CD_FILIAL_DESTINO','CD_FILIAL').withColumnRenamed('CD_FILIAL_ORIGEM','CD_FILIAL_ABASTECIMENTO_FILIAL')

df = df.join(
  df_filial_abastecimento_filial,
  how = 'left',
  on = ['CD_FILIAL']
)

df = df.withColumn('CD_FILIAL_ORIGEM', coalesce(col('CD_FILIAL_ABASTECIMENTO_ITEM'), col('CD_FILIAL_ABASTECIMENTO_FILIAL')))

df = df.drop('CD_FILIAL_ABASTECIMENTO_ITEM','CD_FILIAL_ABASTECIMENTO_FILIAL')

# As fábricas não possuem filial de abastecimento. Elas mesmas são a origem de seus skus
df = df.withColumn(
  'CD_FILIAL_ORIGEM', 
  when(col('ID_LOGISTICA') == 'F', col('CD_FILIAL')).otherwise(col('CD_FILIAL_ORIGEM'))
)

df_lead_time = df_lead_time.withColumnRenamed('CD_FILIAL_DESTINO','CD_FILIAL')

df = df.join(
  df_lead_time,
  how = 'left',
  on = ['CD_FILIAL', 'CD_FILIAL_ORIGEM']
)

# Inicialmente adicionaremos 3 dias no lead time, 2 para contratação + carregamento e 1 para descarga

df = df.withColumn('LEAD_TIME_CONSIDERADO', col('LEAD_TIME_MEDIANO') + 3)


# Fábricas terão um lead time e devio padrão de lead time iguais ao intervalo de produção e desvio padrao do intervalo de produção, respectivamente
df = df.withColumn(
  'LEAD_TIME_CONSIDERADO',
  when(col('ID_LOGISTICA') == 'F', col('INTERVALO_PRODUCAO_MEDIANO')).otherwise(col('LEAD_TIME_CONSIDERADO'))
).withColumn(
  'DESVPAD_LEAD_TIME',
  when(col('ID_LOGISTICA') == 'F', col('DESVPAD_INTERVALO_PRODUCAO')).otherwise(col('DESVPAD_LEAD_TIME'))
)

# Inicialmente usaremos um score z = 1,64 para todos os itens em todas as filiais

df = df.withColumn(
  'SCORE_Z', 
  when(col('CD_ITEM').isin(lista_itens_heroes), lit(2.05)).otherwise(lit(1.64))
)

df = df.withColumn(
  'ESTOQUE_CICLO',
  F.round(col('CONSUMO_CICLO_ABASTECIMENTO') + col('CANCELAMENTO_CICLO_ABASTECIMENTO'),2)
).withColumn(
  'ESTOQUE_SEGURANCA',
  F.round(col('SCORE_Z')*F.sqrt( col('LEAD_TIME_CONSIDERADO')*col('DESVIO_DEMANDA_DIARIZADO')**2 + (( col('VOLUME_REAL_DIARIZADO') + col('QT_CANCELAMENTO_DIARIZADA') )*col('DESVPAD_LEAD_TIME'))**2 ),2)
)

df = df.withColumn(
  'ESTOQUE_MINIMO',
  col('ESTOQUE_SEGURANCA')
).withColumn(
  'ESTOQUE_MAXIMO',
  col('ESTOQUE_SEGURANCA') + col('ESTOQUE_CICLO')
).withColumn(
  'FLAG_ITEM_CRITICO',
  when(col('ESTOQUE_MINIMO') >= 2*col('QT_PESO_ATUAL'), lit('Item em falta')). \
  when(2*col('ESTOQUE_MAXIMO') <= col('QT_PESO_ATUAL'), lit('Item em excesso')). \
  otherwise(lit('Sem flag'))
)

df = df.withColumn('INTERVALO_PRODUCAO_MEDIANO', col('INTERVALO_PRODUCAO_MEDIANO').cast(IntegerType()))

# COMMAND ----------

# df.filter(
#   (col('CD_FILIAL') == 676)
#   &(col('CD_ITEM') == 9865)
# ).display()

# COMMAND ----------

df.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.resultados').save()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #### Acompanhamento da evolução dos resultados

# COMMAND ----------

df_his = df.alias('df_his')

df_his = df_his.withColumn('DT_EXECUCAO', lit(hoje))

# COMMAND ----------

schema = StructType([ 
  StructField("CD_FILIAL", IntegerType(), True),
  StructField("CD_FILIAL_ORIGEM", IntegerType(), True),
  StructField("CD_ITEM", IntegerType(), True),
  StructField("ID_LOGISTICA", StringType(), True),
  StructField("DS_LOGISTICA", StringType(), True),
  StructField("VOLUME_REAL_DIARIZADO", DoubleType(), True),
  StructField("DESVPAD_VOLUME_REAL_DIARIZADO", DoubleType(), True),
  StructField("DESVIO_DEMANDA_DIARIZADO", DoubleType(), True),
  StructField("VOLUME_META", DoubleType(), True),
  StructField("VOLUME_META_DIARIZADO_FLAT", DoubleType(), True),
  StructField("VOLUME_META_DIARIZADO", DoubleType(), True),
  StructField("CONSUMO_CICLO_ABASTECIMENTO", DoubleType(), True),
  StructField("DIAS_CONSIDERADOS_CICLO_ABASTECIMENTO", IntegerType(), True),
  StructField("QT_CANCELAMENTO_DIARIZADA", DoubleType(), True),
  StructField("CANCELAMENTO_CICLO_ABASTECIMENTO", DoubleType(), True),
  StructField("INTERVALO_ABASTECIMENTO_MEDIANO", DoubleType(), True),
  StructField("DATA_REFERENCIA_ESTOQUE", DateType(), True),
  StructField("QT_VOLUME_ATUAL", DoubleType(), True),
  StructField("QT_PESO_ATUAL", DoubleType(), True),
  StructField("INTERVALO_PRODUCAO_MEDIANO", IntegerType(), True),
  StructField("DESVPAD_INTERVALO_PRODUCAO", DoubleType(), True),
  StructField("LEAD_TIME_MEDIANO", DoubleType(), True),
  StructField("DESVPAD_LEAD_TIME", DoubleType(), True),
  StructField("LEAD_TIME_CONSIDERADO", DoubleType(), True),
  StructField("SCORE_Z", DoubleType(), True),
  StructField("ESTOQUE_CICLO", DoubleType(), True),
  StructField("ESTOQUE_SEGURANCA", DoubleType(), True),
  StructField("ESTOQUE_MINIMO", DoubleType(), True),
  StructField("ESTOQUE_MAXIMO", DoubleType(), True),
  StructField("FLAG_ITEM_CRITICO", StringType(), True),
  StructField("DT_EXECUCAO", DateType(), True)
])

# COMMAND ----------

df_his = spark.createDataFrame(df_his.rdd, schema)

# COMMAND ----------

delete_statement = f"""

DELETE mrp-ia.estoque_ideal_produto_acabado.his_resultados
WHERE
  DT_EXECUCAO = '{hoje}'
"""

query_job = client.query(delete_statement)

query_job.result()

df_his.write.format("bigquery").mode('append').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'mrp-ia.estoque_ideal_produto_acabado.his_resultados').save()
