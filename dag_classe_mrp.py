import pendulum
from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.google.cloud.operators.dataproc import ClusterGenerator
from airflow.contrib.operators.dataproc_operator import DataprocClusterCreateOperator, DataprocClusterDeleteOperator, DataProcPySparkOperator
from airflow.models import Variable
from airflow.utils.trigger_rule import TriggerRule

CLUSTER_NAME = 'cluster-classe-mrp'

regiao = Variable.get('region_us')

#Parametros do Airflow
DEFAULT_DAG_ARGS = {
    'owner': 'airflow',  #O Owner da TASK
    'depends_on_past': False, #Sempre Falso
    'start_date': pendulum.datetime(2022, 6, 8, 0, 0, 0, tz='America/Sao_Paulo'),
    'email_on_failure': Variable.get('email_on_failure'), #Variavel que vai definir o parametro se recebe e-mail ou nÃ£o em caso de falha.
    'email_on_retry': Variable.get('email_on_retry'), #Variavel que vai definir o parametro se recebe e-mail ou nÃ£o em caso de retry.
    'retries': 3,  # Retry once before failing the task.
    'retry_delay': timedelta(minutes=5),  # Time between retries.
    'project_id': Variable.get('gcp_project')  # Variavel que irÃ¡ trazer o ID do Projeto onde fica o Cloud Composer.
}

#Linha para definir as Libraries que devem ser instaladas no Dataproc para rodar os scripts.
str_packages = 'pandas==1.2.4 numpy==1.20.1 openpyxl==3.0.10'

# Create Directed Acyclic Graph for Airflow
with DAG('CLASSE_MRP', #Defina o nome da DAG que irÃ¡ aparecer dentro do Airflow, o espaÃ§o deve ser substituÃ­do por underline "_"
		default_args=DEFAULT_DAG_ARGS, #Passa os parametros que foram definidos de forma padrÃ£o ao Airflow dentro de DEFAULT_DAG_ARGS na linha 26.
		schedule_interval='0 13 * * *',
        max_active_runs=1,
        tags=["ds_team", "planejamento"]) as dag: 
		#Definir o intervalor de schedule. Este deve ser inserido em CRON, neste link vocÃª tem um CRON generator para auxilia-lo: https://www.freeformatter.com/cron-expression-generator-quartz.html

        CLUSTER_GENERATOR_CONFIG  =  ClusterGenerator(
            cluster_name=CLUSTER_NAME, #O Nome do Cluster deve ser separado por hifen "-" e em minusculo
            region=regiao,
            project_id=Variable.get('gcp_project'),
            init_actions_uris = [
                'gs://seara-airflow-prod/init/us-west1-connectors/connectors.sh',
                'gs://seara-airflow-prod/init/us-west1-pip-install/pip-install.sh'

            ],
            metadata={
                # 'gcs-connector-version' : '2.1.1' , 
                # 'bigquery-connector-version': '1.1.1',
                'spark-bigquery-connector-version': '0.25.0',
                'PIP_PACKAGES': str_packages
            },
            image_version = '2.0-centos8',
            num_workers = 0,
            master_machine_type = "n2-standard-64",
            master_disk_type = 'pd-ssd',
            master_disk_size = 50,
            # worker_machine_type = "e2-standard-4",
            # worker_disk_type = 'pd-ssd',
            # worker_disk_size = 50,
            # optional_components=['JUPYTER', 'ANACONDA'],
            subnetwork_uri = Variable.get('subnetwork_uri_us'),
            service_account_scopes = [Variable.get('account_scope')],
        ).make()

	    #Operador para criar um cluster spark no Dataproc para processar os dados necessários.
        create_cluster = DataprocClusterCreateOperator(
            task_id='CREATE_CLUSTER', #O ID deve ser separado por underline "_"
            cluster_name=CLUSTER_NAME, #O Nome do Cluster deve ser separado por hifen "-" e em minusculo
            region=regiao,
            project_id=Variable.get('gcp_project'),
            cluster_config=CLUSTER_GENERATOR_CONFIG
        )

        #Operador para mandar um JOB ao cluster do Dataproc criado na primeira etapa.
        cluster_task = DataProcPySparkOperator(
            task_id='LOAD_JOB', 
            main='gs://seara-airflow-prod/scripts/MRP/classe_mrp.py',
            cluster_name=CLUSTER_NAME,
            # pyfiles=[
            #     'gs://seara-airflow-dev/scripts/FORECAST_DEMANDA_IND/output.py'
            # ],
            region=regiao
        )
        
        #Deleta o cluster criado no Dataproc ao final do processamento.
        delete_cluster = DataprocClusterDeleteOperator(
        				task_id='DELETE_CLUSTER', #O ID deve ser separado por underline "_"
        				region=regiao,
        				cluster_name=CLUSTER_NAME,
        				trigger_rule=TriggerRule.ALL_DONE) 

#Abaixo vocÃª deve definir como serÃ¡ o pipeline das tarefas, colocando pelo nomes das tarefas que foram criadas, por exemplo. delete_cluster Ã© a Ãºltima tarefa a ser executada. 
create_cluster >> cluster_task >> delete_cluster